#ifndef DEF_NOTIFICATIONTYPES_H
#define DEF_NOTIFICATIONTYPES_H

#define NNT_CAPTCHA_REQUEST           "CaptchaRequest"
#define NNT_CHAT_MESSAGE              "ChatMessage"
#define NNT_NORMAL_MESSAGE            "NormalMessage"
#define NNT_FILETRANSFER              "FileTransfer"
#define NNT_MUC_MESSAGE_INVITE        "MucMessageInvite"
#define NNT_MUC_MESSAGE_GROUPCHAT     "MucMessageGroupchat"
#define NNT_MUC_MESSAGE_PRIVATE       "MucMessagePrivate"
#define NNT_MUC_MESSAGE_MENTION       "MucMessageMention"
#define NNT_SUBSCRIPTION_REQUEST      "SubscriptionRequest"
#define NNT_SESSION_NEGOTIATION       "SessionNegotiation"
#define NNT_CONNECTION_ERROR          "ConnectionError"
#define NNT_BIRTHDAY                  "Birthday"
#define NNT_CHATSTATE_TYPING          "ChatStateTyping"
#define NNT_ROSTEREXCHANGE_REQUEST    "RosterExchangeRequest"

// *** <<< eyeCU <<< ***
#define NNT_ATTENTION                 "Attention"
#define NNT_RECEIPTS                  "Receipts"
#define NNT_ACTIVITY                  "Activity"
#define NNT_CONTACTPROXIMITY          "ContactProximity"
#define NNT_MOOD                      "Mood"
#define NNT_TUNE                      "Tune"
#define NNT_JINGLE_RTP_CALL           "JingleRtpCall"
#define NNT_JINGLE_RTP_MISSED         "JingleRtpMissed"
// *** >>> eyeCU >>> ***
#endif // DEF_NOTIFICATIONTYPES_H
