<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru" sourcelanguage="en">
<context>
    <name>AboutBox</name>
    <message>
        <source>Version: %1.%2 %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Revision: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AboutBoxClass</name>
    <message>
        <source>About the program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>© 2010-2020 Konstantin Kozlov, Vyatcheslav Tselykh, Sergey Potapov. This software is released under the terms of the GNU General Public License version 3.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PasswordDialog</name>
    <message>
        <source>Enter password:</source>
        <translation type="vanished">Введите пароль:</translation>
    </message>
    <message>
        <source>Save password</source>
        <translation type="vanished">Сохранить пароль</translation>
    </message>
</context>
<context>
    <name>PluginManager</name>
    <message>
        <source>Saving settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Loading plugin: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Duplicate plugin uuid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wrong plugin interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dependencies not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Conflict with plugin %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Initializing plugin connections: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Initialization failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Initializing plugin objects: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Initializing plugin settings: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Starting plugin: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Application started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About the program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Setup plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Global shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Application shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchLineEdit</name>
    <message>
        <source>Search options</source>
        <translation type="vanished">Настройки поиска</translation>
    </message>
    <message>
        <source>Clear text</source>
        <translation type="vanished">Очистить текст</translation>
    </message>
</context>
<context>
    <name>SetupPluginsDialog</name>
    <message>
        <source>Disabled (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>With errors (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <source>This plugin depends on %n other plugin(s).</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <source>This plugin does not depend on other plugins.</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <source>Other %n plugin(s) depend on this plugin.</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <source>Another plugin depends on this plugin.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Other plugins don&apos;t depend on this plugin.</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <source>%n dependency(ies) not found.</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <source>Restart Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings saved. Do you want to restart application?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SetupPluginsDialogClass</name>
    <message>
        <source>Setup Plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Plugin Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search plugin</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>XmppSaslError</name>
    <message>
        <source>Authorization aborted</source>
        <translation type="vanished">Авторизация отменена</translation>
    </message>
    <message>
        <source>Account disabled</source>
        <translation type="vanished">Аккаунт отключен</translation>
    </message>
    <message>
        <source>Credentials expired</source>
        <translation type="vanished">Истек срок полномочий</translation>
    </message>
    <message>
        <source>Encryption required</source>
        <translation type="vanished">Требуется шифрование</translation>
    </message>
    <message>
        <source>Incorrect encoding</source>
        <translation type="vanished">Неверная кодировка</translation>
    </message>
    <message>
        <source>Invalid authorization id</source>
        <translation type="vanished">Неверный id авторизации</translation>
    </message>
    <message>
        <source>Invalid mechanism</source>
        <translation type="vanished">Неверный механизм</translation>
    </message>
    <message>
        <source>Malformed request</source>
        <translation type="vanished">Некорректный запрос</translation>
    </message>
    <message>
        <source>Mechanism is too weak</source>
        <translation type="vanished">Механиз слишком слаб</translation>
    </message>
    <message>
        <source>Not authorized</source>
        <translation type="vanished">Не пройдена авторизация</translation>
    </message>
    <message>
        <source>Temporary authentication failure</source>
        <translation type="vanished">Временный сбой авторизации</translation>
    </message>
</context>
<context>
    <name>XmppStreamError</name>
    <message>
        <source>Undefined error condition</source>
        <translation type="vanished">Неизвестная ошибка</translation>
    </message>
    <message>
        <source>Bad request format</source>
        <translation type="vanished">Неверный формат запроса</translation>
    </message>
    <message>
        <source>Bad namespace prefix</source>
        <translation type="vanished">Неверный префикс пространства имён</translation>
    </message>
    <message>
        <source>Conflict</source>
        <translation type="vanished">Конфликт</translation>
    </message>
    <message>
        <source>Connection timeout</source>
        <translation type="vanished">Превышено время подключения</translation>
    </message>
    <message>
        <source>Host is not serviced</source>
        <translation type="vanished">Хост не обслуживается</translation>
    </message>
    <message>
        <source>Unknown host</source>
        <translation type="vanished">Неизвестный хост</translation>
    </message>
    <message>
        <source>Improper addressing</source>
        <translation type="vanished">Неправильная адресация</translation>
    </message>
    <message>
        <source>Internal server error</source>
        <translation type="vanished">Внутренняя ошибка сервера</translation>
    </message>
    <message>
        <source>Invalid from address</source>
        <translation type="vanished">Неверный адрес отправителя</translation>
    </message>
    <message>
        <source>Invalid namespace</source>
        <translation type="vanished">Неверное пространство имен</translation>
    </message>
    <message>
        <source>Invalid XML</source>
        <translation type="vanished">Недопустимый XML</translation>
    </message>
    <message>
        <source>Not authorized</source>
        <translation type="vanished">Не пройдена авторизация</translation>
    </message>
    <message>
        <source>XML not well formed</source>
        <translation type="vanished">XML сформирован неправильно</translation>
    </message>
    <message>
        <source>Policy violation</source>
        <translation type="vanished">Нарушены ограничения</translation>
    </message>
    <message>
        <source>Remote connection failed</source>
        <translation type="vanished">Сбой удалённого подключения</translation>
    </message>
    <message>
        <source>Stream need to be reseted</source>
        <translation type="vanished">Поток необходимо переустановить</translation>
    </message>
    <message>
        <source>Resource constraint</source>
        <translation type="vanished">Недостаточно ресурсов</translation>
    </message>
    <message>
        <source>Restricted XML</source>
        <translation type="vanished">Запрещенный XML</translation>
    </message>
    <message>
        <source>See other host</source>
        <translation type="vanished">Используйте другой хост</translation>
    </message>
    <message>
        <source>System shutdown</source>
        <translation type="vanished">Система отключена</translation>
    </message>
    <message>
        <source>Unsupported encoding</source>
        <translation type="vanished">Неподдерживаемая кодировка</translation>
    </message>
    <message>
        <source>Unsupported feature</source>
        <translation type="vanished">Неподдерживаемая функция</translation>
    </message>
    <message>
        <source>Unsupported stanza type</source>
        <translation type="vanished">Неподдерживаемый тип станзы</translation>
    </message>
    <message>
        <source>Unsupported version</source>
        <translation type="vanished">Неподдерживаемая версия</translation>
    </message>
    <message>
        <source>Feature not implemented</source>
        <translation type="vanished">Возможность не реализована</translation>
    </message>
    <message>
        <source>Insufficient permissions</source>
        <translation type="vanished">Недостаточно прав</translation>
    </message>
    <message>
        <source>Recipient changed address</source>
        <translation type="vanished">Адресат изменил адрес</translation>
    </message>
    <message>
        <source>Requested item not found</source>
        <translation type="vanished">Запрашиваемый элемент не найден</translation>
    </message>
    <message>
        <source>Malformed XMPP address</source>
        <translation type="vanished">Недопустимый XMPP адрес</translation>
    </message>
    <message>
        <source>Not accepted by the recipient</source>
        <translation type="vanished">Не принято адресатом</translation>
    </message>
    <message>
        <source>Not allowed by the recipient</source>
        <translation type="vanished">Не разрешено адресатом</translation>
    </message>
    <message>
        <source>Recipient unavailable</source>
        <translation type="vanished">Адресат недоступен</translation>
    </message>
    <message>
        <source>Redirect to another address</source>
        <translation type="vanished">Перенаправление на другой адрес</translation>
    </message>
    <message>
        <source>Registration required</source>
        <translation type="vanished">Требуется регистрация</translation>
    </message>
    <message>
        <source>Remote server not found</source>
        <translation type="vanished">Удаленный сервер не найден</translation>
    </message>
    <message>
        <source>Remote server timeout</source>
        <translation type="vanished">Превышено время ожидания удалённого сервера</translation>
    </message>
    <message>
        <source>Service unavailable</source>
        <translation type="vanished">Сервис недоступен</translation>
    </message>
    <message>
        <source>Subscription required</source>
        <translation type="vanished">Требуется подписка</translation>
    </message>
    <message>
        <source>Unexpected request</source>
        <translation type="vanished">Неожиданный запрос</translation>
    </message>
    <message>
        <source>Resource limit exceeded</source>
        <translation type="vanished">Превышен лимит ресурсов</translation>
    </message>
    <message>
        <source>Stanza is too big</source>
        <translation type="vanished">Станза слишком большая</translation>
    </message>
    <message>
        <source>Too many stanzas</source>
        <translation type="vanished">Слишком много станз</translation>
    </message>
</context>
</TS>
