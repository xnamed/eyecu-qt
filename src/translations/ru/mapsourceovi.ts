<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>MapSourceOvi</name>
    <message>
        <source>HERE map source</source>
        <translation>Источник карт HERE</translation>
    </message>
    <message>
        <source>Allows Map plugin to use Nokia&apos;s HERE service as map source</source>
        <translation>Позволяет плагину Карта использовать сервис HERE от Nokia в качестве источника карт</translation>
    </message>
</context>
</TS>
