<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>Receipts</name>
    <message>
        <source>Message Delivery Receipts</source>
        <translation>Уведомления о Доставке Сообщений</translation>
    </message>
    <message>
        <source>Sends/receives Message Delivery Receipts</source>
        <translation>Отправляет/принимает Уведомления о Доставке Сообщений</translation>
    </message>
    <message>
        <source>Show delivery notifications</source>
        <translation>Отображать уведомления о доставке</translation>
    </message>
    <message>
        <source>Send delivery notifications</source>
        <translation>Посылать уведомления о достваке</translation>
    </message>
    <message>
        <source>When message delivery notification recieved</source>
        <translation>При получении уведомления о доставке сообщения</translation>
    </message>
    <message>
        <source>Message delivered</source>
        <translation>Сообщение доставлено</translation>
    </message>
    <message>
        <source>Sends, receives and displays message delivery receipts</source>
        <translation>Отправляет, получает и отображает уведомления о доставке сообщений</translation>
    </message>
</context>
</TS>
