<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>JingleTransportRawUdp</name>
    <message>
        <source>Jingle RAW-UDP Transport</source>
        <translation>Транспорт Jingle RAW-UDP</translation>
    </message>
    <message>
        <source>Implements XEP-0177: Jingle RAW-UDP transport method</source>
        <translation>Реализует XEP-0177: Транспорт-Метод Jingle RAW-UDP</translation>
    </message>
    <message>
        <source>Allows using RAW-UDP transport in Jingle sesions</source>
        <translation>Позволяет использовать в сеансах Jingle транспорт RAW-UDIP</translation>
    </message>
</context>
</TS>
