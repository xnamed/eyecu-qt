<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>MapSearchProviderHere</name>
    <message>
        <source>Map Search Provider Here</source>
        <translation>Поставщик услуг поиска на карте Here</translation>
    </message>
    <message>
        <source>Allows to use Nokia&apos;s Here service as a map search provider</source>
        <translation>Позволяет осуществлять поиск на карте с помощью службы Here от Нокиа</translation>
    </message>
    <message>
        <source>Here</source>
        <translation>Here</translation>
    </message>
</context>
</TS>
