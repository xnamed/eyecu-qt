<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>PositioningMethodIpProviderFreegeoip</name>
    <message>
        <source>Positining Method IP Provider freegeoip.net</source>
        <translation>Провайдер метода определения местоположения на основе IP-адреса freegeoip.net</translation>
    </message>
    <message>
        <source>Allows to use freegeoip.net as an IP positioning provider</source>
        <translation>Позволяет использовать freegeoip.net в качестве провайдера определения местоположения, основываясь на IP</translation>
    </message>
    <message>
        <source>freegeoip.net</source>
        <translation>freegeoip.net</translation>
    </message>
</context>
</TS>
