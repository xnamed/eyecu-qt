<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>JingleTransportIceUdp</name>
    <message>
        <source>Jingle ICE-UDP Transport</source>
        <translation>Транспорт Jingle ICE-UDP</translation>
    </message>
    <message>
        <source>Implements XEP-0176: Jingle ICE-UDP transport Method</source>
        <translation>Реализует XEP-0176: Транспорт-Метод Jingle ICE-UDP</translation>
    </message>
    <message>
        <source>Allows using ICE-UDP transport in Jingle sesions</source>
        <translation>Позволяет использовать в сеансах Jingle транспорт ICE-UDIP</translation>
    </message>
</context>
</TS>
