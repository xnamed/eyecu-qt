<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
  <context>
    <name>Emoji</name>
    <message>
      <location line="51" filename="../../plugins/emoji/emoji.cpp"/>
      <source>Emoji</source>
      <translation>Эмодзи</translation>
    </message>
    <message>
      <location line="52" filename="../../plugins/emoji/emoji.cpp"/>
      <source>Allows to use emoji in messages</source>
      <translation>Позволяет использовать эмодзи в сообщениях</translation>
    </message>
    <message>
      <location line="87" filename="../../plugins/emoji/emoji.cpp"/>
      <source>Smileys &amp; People</source>
      <translation>Смайлики и Люди</translation>
    </message>
    <message>
      <location line="88" filename="../../plugins/emoji/emoji.cpp"/>
      <source>Animals &amp; Nature</source>
      <translation>Животные и Природа</translation>
    </message>
    <message>
      <location line="89" filename="../../plugins/emoji/emoji.cpp"/>
      <source>Food &amp; Drink</source>
      <translation>Еда и Напитки</translation>
    </message>
    <message>
      <location line="90" filename="../../plugins/emoji/emoji.cpp"/>
      <source>Travel &amp; Places</source>
      <translation>Путешествие и Места</translation>
    </message>
    <message>
      <location line="91" filename="../../plugins/emoji/emoji.cpp"/>
      <source>Symbols</source>
      <translation>Символы</translation>
    </message>
    <message>
      <location line="92" filename="../../plugins/emoji/emoji.cpp"/>
      <source>Activities</source>
      <translation>Виды деятельности</translation>
    </message>
    <message>
      <location line="94" filename="../../plugins/emoji/emoji.cpp"/>
      <source>Flags</source>
      <translation>Флаги</translation>
    </message>
    <message>
      <location line="93" filename="../../plugins/emoji/emoji.cpp"/>
      <source>Objects</source>
      <translation>Предметы</translation>
    </message>
    <message>
      <location line="166" filename="../../plugins/emoji/emoji.cpp"/>
      <source>Message windows</source>
      <translation>Окна сообщений</translation>
    </message>
  </context>
  <context>
    <name>EmojiOptions</name>
    <message>
      <location line="14" filename="../../plugins/emoji/emojioptions.cpp"/>
      <source>Do not use emoji</source>
      <translation>Не использовать эмодзи</translation>
    </message>
  </context>
  <context>
    <name>EmojiOptionsClass</name>
    <message>
      <location line="29" filename="../../plugins/emoji/emojioptions.ui"/>
      <source>Emoji set:</source>
      <translation>Набор эмодзи:</translation>
    </message>
    <message>
      <location line="39" filename="../../plugins/emoji/emojioptions.ui"/>
      <source>Menu icon size:</source>
      <translation>Размер значков в меню:</translation>
    </message>
    <message>
      <location line="49" filename="../../plugins/emoji/emojioptions.ui"/>
      <source>Chat icon size:</source>
      <translation>Размер значков в чате:</translation>
    </message>
  </context>
  <context>
    <name>IconSizeSpinBox</name>
    <message numerus="yes">
      <location line="62" filename="../../plugins/emoji/iconsizespinbox.cpp"/>
      <source>pixel(s)</source>
      <translation>
        <numerusform>пиксель</numerusform>
        <numerusform>пикселя</numerusform>
        <numerusform>пикселей</numerusform>
        <numerusform>пикселей</numerusform>
      </translation>
    </message>
  </context>
  <context>
    <name>SelectIconMenu</name>
    <message>
      <location line="129" filename="../../plugins/emoji/selecticonmenu.cpp"/>
      <source>Fitzpatrick type %1</source>
      <comment>https://en.wikipedia.org/wiki/Fitzpatrick_scale</comment>
      <translation>Тип %1 по шкале Фицпатрика</translation>
    </message>
    <message>
      <location line="143" filename="../../plugins/emoji/selecticonmenu.cpp"/>
      <source>Skin color</source>
      <translation>Цвет кожи</translation>
    </message>
    <message>
      <location line="112" filename="../../plugins/emoji/selecticonmenu.cpp"/>
      <source>Default</source>
      <translation>По умолчанию</translation>
    </message>
    <message>
      <location line="129" filename="../../plugins/emoji/selecticonmenu.cpp"/>
      <source>1 or 2</source>
      <translation>1 или 2</translation>
    </message>
  </context>
</TS>
