<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>Audio</name>
    <message>
        <source>Audio/Video chat</source>
        <translation>Аудио/Видео чат</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation>Аудио</translation>
    </message>
</context>
<context>
    <name>JingleRtp</name>
    <message>
        <source>Implements XEP-0167: Jingle RTP Sessions</source>
        <translation>Реализует XEP-0167: Сеансы RTP Джингл</translation>
    </message>
    <message>
        <source>Missed call!</source>
        <translation>Пропущенный звонок!</translation>
    </message>
    <message>
        <source>Incoming call!</source>
        <translation>Входящий звонок!</translation>
    </message>
    <message>
        <source>Jingle RTP Sessions</source>
        <translation>Сеансы RTP Джингл</translation>
    </message>
    <message>
        <source>Audio/Video chat via Jingle RTP</source>
        <translation>Аудио/Видео чат через Джингл</translation>
    </message>
    <message>
        <source>Voice call</source>
        <translation>Голосовой звонок</translation>
    </message>
    <message>
        <source>Video call</source>
        <translation>Видеозвонок</translation>
    </message>
    <message>
        <source>When incoming voice or video call received</source>
        <translation>При входящем голосовом или видео звонке</translation>
    </message>
    <message>
        <source>When incoming voice or video call missed</source>
        <translation>При пропущенном голосовом или видеозвонке</translation>
    </message>
    <message>
        <source>Incoming %1 call from %2</source>
        <translation>Входящий %1 звонок от %2</translation>
    </message>
    <message>
        <source>Missed %1 call from %2</source>
        <translation>Пропущенный %1 звонок от %2</translation>
    </message>
    <message>
        <source>video</source>
        <translation>видео</translation>
    </message>
    <message>
        <source>voice</source>
        <translation>голосовой</translation>
    </message>
    <message>
        <source>%1 - Chat</source>
        <translation>%1 - Чат</translation>
    </message>
    <message>
        <source>Call cancelled</source>
        <translation>Звонок отменён</translation>
    </message>
    <message>
        <source>Call rejected</source>
        <translation>Звонок отклонён</translation>
    </message>
    <message>
        <source>Call finished</source>
        <translation>Звонок завершён</translation>
    </message>
    <message>
        <source>Call error</source>
        <translation>Ошибка звонка</translation>
    </message>
    <message>
        <source>Hangup</source>
        <translation>Отбой</translation>
    </message>
    <message>
        <source>Jingle RTP</source>
        <translation></translation>
    </message>
    <message>
        <source>Call error (%1)</source>
        <translation>Ошибка звонка (%1)</translation>
    </message>
    <message>
        <source>Microphone OFF</source>
        <translation>Микрофон ВЫКЛ</translation>
    </message>
    <message>
        <source>Microphone ON</source>
        <translation>Микрофон ВКЛ</translation>
    </message>
</context>
<context>
    <name>JingleRtpOptions</name>
    <message>
        <source>Jingle RTP options</source>
        <translation>Опции Jingle RTP</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation>Аудио</translation>
    </message>
    <message>
        <source>Codec</source>
        <translation>Кодек</translation>
    </message>
    <message>
        <source>Notify Interval, ms</source>
        <translation>Интервал уведомлений, мс</translation>
    </message>
    <message>
        <source>Video</source>
        <translation>Видео</translation>
    </message>
    <message>
        <source>Use RTCP</source>
        <translation>Использовать RTCP</translation>
    </message>
    <message>
        <source>Sample rate, Hz</source>
        <translation>Частота дискретизации, Гц</translation>
    </message>
</context>
</TS>
