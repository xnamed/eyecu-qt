<?xml version="1.0" ?><!DOCTYPE TS><TS language="uk" version="2.0">
 <context>
  <name>MapSearchProviderHere</name>
  <message>
   <location filename="../../plugins/mapsearchproviderhere/mapsearchproviderhere.cpp" line="15"/>
   <source>Map Search Provider Here</source>
   <translation>Провайдер пошуку на мапі Here</translation>
  </message>
  <message>
   <location filename="../../plugins/mapsearchproviderhere/mapsearchproviderhere.cpp" line="16"/>
   <source>Allows to use Nokia&apos;s Here service as a map search provider</source>
   <translation>Дозволяє використовувати Here від Nokia у ролі провайдеру пошуку</translation>
  </message>
  <message>
   <location filename="../../plugins/mapsearchproviderhere/mapsearchproviderhere.cpp" line="55"/>
   <source>Here</source>
   <translation>Here</translation>
  </message>
 </context>
</TS>