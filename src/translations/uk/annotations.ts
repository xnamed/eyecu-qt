<?xml version="1.0" ?><!DOCTYPE TS><TS language="uk" version="2.0">
 <context>
  <name>Annotations</name>
  <message>
   <location filename="../../plugins/annotations/annotations.cpp" line="47"/>
   <source>Annotations</source>
   <translation>Коментарі</translation>
  </message>
  <message>
   <location filename="../../plugins/annotations/annotations.cpp" line="48"/>
   <source>Allows to add comments to the contacts in the roster</source>
   <translation>Дозволяє додавати короткі записи до контактів у ростері</translation>
  </message>
  <message>
   <location filename="../../plugins/annotations/annotations.cpp" line="124"/>
   <location filename="../../plugins/annotations/annotations.cpp" line="388"/>
   <source>Annotation</source>
   <translation>Коментар</translation>
  </message>
  <message>
   <location filename="../../plugins/annotations/annotations.cpp" line="423"/>
   <source>&lt;b&gt;Annotation:&lt;/b&gt;</source>
   <translation>&lt;b&gt;Комментар:&lt;/b&gt;</translation>
  </message>
 </context>
 <context>
  <name>EditNoteDialog</name>
  <message>
   <location filename="../../plugins/annotations/editnotedialog.cpp" line="13"/>
   <source>Annotation - %1</source>
   <translation>Коментар - %1</translation>
  </message>
 </context>
 <context>
  <name>EditNoteDialogClass</name>
  <message>
   <location filename="../../plugins/annotations/editnotedialog.ui" line="22"/>
   <source>Created:</source>
   <translation>Створено:</translation>
  </message>
  <message>
   <location filename="../../plugins/annotations/editnotedialog.ui" line="54"/>
   <source>Modified:</source>
   <translation>Змінено:</translation>
  </message>
 </context>
</TS>