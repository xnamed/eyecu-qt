<?xml version="1.0" ?><!DOCTYPE TS><TS language="uk" version="2.0">
 <context>
  <name>MapSourceOvi</name>
  <message>
   <location filename="../../plugins/mapsourceovi/mapsourceovi.cpp" line="12"/>
   <source>HERE map source</source>
   <translation>Джерело мапи HERE</translation>
  </message>
  <message>
   <location filename="../../plugins/mapsourceovi/mapsourceovi.cpp" line="13"/>
   <source>Allows Map plugin to use Nokia&apos;s HERE service as map source</source>
   <translation>Дозвляє плагіну мапи використовувати сервіс HERE від Nokia як джерело мапи</translation>
  </message>
 </context>
</TS>