<?xml version="1.0" ?><!DOCTYPE TS><TS language="uk" version="2.0">
 <context>
  <name>Jingle</name>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="34"/>
   <location filename="../../plugins/jingle/jingle.cpp" line="134"/>
   <source>Jingle</source>
   <translation>Jingle</translation>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="35"/>
   <source>Implements XEP-0166: Jingle</source>
   <translation>XEP-0166: Jingle</translation>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="135"/>
   <source>Supports XEP-0166: Jingle</source>
   <translation>Підтрика XEP-0166: Jingle</translation>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="477"/>
   <source>Success</source>
   <translation>Успіх</translation>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="479"/>
   <source>Busy</source>
   <translation>Зайнято</translation>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="481"/>
   <source>Cancelled</source>
   <translation>Відміна</translation>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="483"/>
   <source>Alternative session</source>
   <translation>Альтернативна сесія</translation>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="485"/>
   <source>Connectivity error</source>
   <translation>Помилка з’єднання</translation>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="487"/>
   <source>Declined</source>
   <translation>Відмова</translation>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="489"/>
   <source>Session expired</source>
   <translation>Час сесії вичерпано</translation>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="491"/>
   <source>Application failure</source>
   <translation>Помилка застосунку</translation>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="493"/>
   <source>Transport failure</source>
   <translation>Помилка транспорту</translation>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="495"/>
   <source>General error</source>
   <translation>Загальна помилка</translation>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="497"/>
   <source>Session gone</source>
   <translation>Сесію втрачено</translation>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="499"/>
   <source>Incompatible parameters</source>
   <translation>Несумісні параметри</translation>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="501"/>
   <source>Media error</source>
   <translation>Помилка медіа</translation>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="503"/>
   <source>Security error</source>
   <translation>Помилка безпеки</translation>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="505"/>
   <source>Timeout</source>
   <translation>Таймаут</translation>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="507"/>
   <source>Application not supported</source>
   <translation>Застосунок не підтримується</translation>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="509"/>
   <source>Transport not supported</source>
   <translation>Транспорт не підтримується</translation>
  </message>
 </context>
</TS>