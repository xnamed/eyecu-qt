<?xml version="1.0" ?><!DOCTYPE TS><TS language="uk" version="2.0">
 <context>
  <name>AboutBox</name>
  <message>
   <location filename="../../loader/aboutbox.cpp" line="18"/>
   <source>Version: %1.%2 %3</source>
   <translation>Версія: %1.%2 %3</translation>
  </message>
  <message>
   <location filename="../../loader/aboutbox.cpp" line="19"/>
   <source>Revision: %1</source>
   <translation>Ревізія: %1</translation>
  </message>
 </context>
 <context>
  <name>AboutBoxClass</name>
  <message>
   <location filename="../../loader/aboutbox.ui" line="26"/>
   <source>About the program</source>
   <translation>Про Програму</translation>
  </message>
  <message utf8="true">
   <location filename="../../loader/aboutbox.ui" line="188"/>
   <source>© 2010-2020 Konstantin Kozlov, Vyatcheslav Tselykh, Sergey Potapov. This software is released under the terms of the GNU General Public License version 3.</source>
   <translation>© 2010-2020 Константин Козлов, В&apos;ячеслав Целих, Сергій Потапов. Це програмне забезпечення випущено під GNU General Public License версія 3.</translation>
  </message>
 </context>
 <context>
  <name>PluginManager</name>
  <message>
   <location filename="../../loader/pluginmanager.cpp" line="366"/>
   <source>Saving settings</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../loader/pluginmanager.cpp" line="409"/>
   <source>Loading plugin: %1</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../loader/pluginmanager.cpp" line="450"/>
   <source>Duplicate plugin uuid</source>
   <translation>Дублікат  uuid плагіну</translation>
  </message>
  <message>
   <location filename="../../loader/pluginmanager.cpp" line="457"/>
   <source>Wrong plugin interface</source>
   <translation>Помилковий інтерфейс плагіну</translation>
  </message>
  <message>
   <location filename="../../loader/pluginmanager.cpp" line="477"/>
   <source>Dependencies not found</source>
   <translation>Залежності не знайдені</translation>
  </message>
  <message>
   <location filename="../../loader/pluginmanager.cpp" line="484"/>
   <source>Conflict with plugin %1</source>
   <translation>Конфлікт з плагіном %1</translation>
  </message>
  <message>
   <location filename="../../loader/pluginmanager.cpp" line="514"/>
   <source>Initializing plugin connections: %1</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../loader/pluginmanager.cpp" line="526"/>
   <source>Initialization failed</source>
   <translation>Ініціалізація завершилась з помилкою</translation>
  </message>
  <message>
   <location filename="../../loader/pluginmanager.cpp" line="536"/>
   <source>Initializing plugin objects: %1</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../loader/pluginmanager.cpp" line="545"/>
   <source>Initializing plugin settings: %1</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../loader/pluginmanager.cpp" line="561"/>
   <source>Starting plugin: %1</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../loader/pluginmanager.cpp" line="656"/>
   <source>Application started</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../loader/pluginmanager.cpp" line="946"/>
   <source>About Qt</source>
   <translation>Про Qt</translation>
  </message>
  <message>
   <location filename="../../loader/pluginmanager.cpp" line="952"/>
   <source>About the program</source>
   <translation>Про Програму</translation>
  </message>
  <message>
   <location filename="../../loader/pluginmanager.cpp" line="958"/>
   <source>Setup plugins</source>
   <translation>Налаштувати плагіни</translation>
  </message>
  <message>
   <location filename="../../loader/pluginmanager.cpp" line="967"/>
   <source>Global shortcuts</source>
   <translation>Глобальні гарячі клавіші</translation>
  </message>
  <message>
   <location filename="../../loader/pluginmanager.cpp" line="968"/>
   <source>Application shortcuts</source>
   <translation>Гарячі клавіші застосунку</translation>
  </message>
 </context>
 <context>
  <name>SetupPluginsDialog</name>
  <message>
   <location filename="../../loader/setuppluginsdialog.cpp" line="204"/>
   <source>Disabled (%1)</source>
   <translation>Вмикнено (%1)</translation>
  </message>
  <message>
   <location filename="../../loader/setuppluginsdialog.cpp" line="207"/>
   <source>With errors (%1)</source>
   <translation>З помилками (%1)</translation>
  </message>
  <message>
   <location filename="../../loader/setuppluginsdialog.cpp" line="244"/>
   <source>This plugin does not depend on other plugins.</source>
   <translation>Цей плагін не залежить від інших.</translation>
  </message>
  <message numerus="yes">
   <location filename="../../loader/setuppluginsdialog.cpp" line="242"/>
   <source>This plugin depends on %n other plugin(s).</source>
   <translation><numerusform>Цей плагін залежить від %n іншого плагіну.</numerusform><numerusform>Цей плагін залежить від %n інших плагінів.</numerusform><numerusform>Цей плагін залежить від %n інших плагінів.</numerusform></translation>
  </message>
  <message numerus="yes">
   <location filename="../../loader/setuppluginsdialog.cpp" line="249"/>
   <source>Other %n plugin(s) depend on this plugin.</source>
   <translation><numerusform>%n інший плагін залежить від цього плагіну.</numerusform><numerusform>%n інших плагінів залежать від цього плагіну.</numerusform><numerusform>%n інших плагінів залежать від цього плагіну.</numerusform></translation>
  </message>
  <message>
   <location filename="../../loader/setuppluginsdialog.cpp" line="249"/>
   <source>Another plugin depends on this plugin.</source>
   <translation>Інший плагін залежить від цього плагіну.</translation>
  </message>
  <message>
   <location filename="../../loader/setuppluginsdialog.cpp" line="252"/>
   <source>Other plugins don&apos;t depend on this plugin.</source>
   <translation>Інші плагіни не залежать від цього плагіну.</translation>
  </message>
  <message numerus="yes">
   <location filename="../../loader/setuppluginsdialog.cpp" line="271"/>
   <source>%n dependency(ies) not found.</source>
   <translation><numerusform>%n залежність не знайдена.</numerusform><numerusform>%n залежностей не знайдено.</numerusform><numerusform>%n залежностей не знайдено.</numerusform></translation>
  </message>
  <message>
   <location filename="../../loader/setuppluginsdialog.cpp" line="285"/>
   <source>Restart Application</source>
   <translation>Перезапуск програми</translation>
  </message>
  <message>
   <location filename="../../loader/setuppluginsdialog.cpp" line="285"/>
   <source>Settings saved. Do you want to restart application?</source>
   <translation>Налаштування збережені. Ви хочете перезапустити програму?</translation>
  </message>
 </context>
 <context>
  <name>SetupPluginsDialogClass</name>
  <message>
   <location filename="../../loader/setuppluginsdialog.ui" line="14"/>
   <source>Setup Plugins</source>
   <translation>Налаштувати плагіни</translation>
  </message>
  <message>
   <location filename="../../loader/setuppluginsdialog.ui" line="76"/>
   <source>Plugin Information</source>
   <translation>Інформація про плагін</translation>
  </message>
  <message>
   <location filename="../../loader/setuppluginsdialog.cpp" line="89"/>
   <source>Search plugin</source>
   <translation>Шукати плагін</translation>
  </message>
 </context>
</TS>