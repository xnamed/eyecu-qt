<?xml version="1.0" ?><!DOCTYPE TS><TS language="uk" version="2.0">
 <context>
  <name>CombineContactsDialog</name>
  <message>
   <location filename="../../plugins/metacontacts/combinecontactsdialog.ui" line="14"/>
   <source>Combine Contacts</source>
   <translation>Об&apos;єднання контактів</translation>
  </message>
  <message numerus="yes">
   <location filename="../../plugins/metacontacts/combinecontactsdialog.cpp" line="110"/>
   <source>The following &lt;b&gt;%n contact(s)&lt;/b&gt; will be merged into metacontact:</source>
   <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform></translation>
  </message>
 </context>
 <context>
  <name>MetaContacts</name>
  <message>
   <location filename="../../plugins/metacontacts/metacontacts.cpp" line="80"/>
   <source>Metacontacts</source>
   <translation>Метаконтакти</translation>
  </message>
  <message>
   <location filename="../../plugins/metacontacts/metacontacts.cpp" line="81"/>
   <source>Allows to combine single contacts to metacontacts</source>
   <translation>Дозволяє об&apos;єднувати окремі контакти в метаконтакти</translation>
  </message>
  <message>
   <location filename="../../plugins/metacontacts/metacontacts.cpp" line="202"/>
   <source>Combine contacts</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/metacontacts/metacontacts.cpp" line="202"/>
   <source>Ctrl+M</source>
   <comment>Combine contacts</comment>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/metacontacts/metacontacts.cpp" line="203"/>
   <source>Destroy metacontact</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/metacontacts/metacontacts.cpp" line="204"/>
   <source>Detach from metacontact</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/metacontacts/metacontacts.cpp" line="519"/>
   <source>Copy Metacontact to Group</source>
   <translation>Копіювати метаконтакт до групи</translation>
  </message>
  <message>
   <location filename="../../plugins/metacontacts/metacontacts.cpp" line="525"/>
   <source>Move Metacontact to Group</source>
   <translation>Перемістити метаконтакт до групи</translation>
  </message>
  <message>
   <location filename="../../plugins/metacontacts/metacontacts.cpp" line="565"/>
   <location filename="../../plugins/metacontacts/metacontacts.cpp" line="1936"/>
   <source>Combine Contacts...</source>
   <translation>Об&apos;єднати контакти...</translation>
  </message>
  <message>
   <location filename="../../plugins/metacontacts/metacontacts.cpp" line="1149"/>
   <source>%1 - Chat</source>
   <translation>%1 - Чат</translation>
  </message>
  <message>
   <location filename="../../plugins/metacontacts/metacontacts.cpp" line="1461"/>
   <source>Rename Metacontact</source>
   <translation>Перейменувати метаконтакт</translation>
  </message>
  <message>
   <location filename="../../plugins/metacontacts/metacontacts.cpp" line="1461"/>
   <source>Enter name:</source>
   <translation>Введіть ім&apos;я:</translation>
  </message>
  <message>
   <location filename="../../plugins/metacontacts/metacontacts.cpp" line="1949"/>
   <source>Detach from Metacontact</source>
   <translation>Відділити від метаконтакту</translation>
  </message>
  <message>
   <location filename="../../plugins/metacontacts/metacontacts.cpp" line="1961"/>
   <source>Destroy Metacontact</source>
   <translation>Розділити метаконтакти</translation>
  </message>
  <message>
   <location filename="../../plugins/metacontacts/metacontacts.cpp" line="1973"/>
   <source>Rename</source>
   <translation>Перейменувати</translation>
  </message>
 </context>
</TS>