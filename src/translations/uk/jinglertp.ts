<?xml version="1.0" ?><!DOCTYPE TS><TS language="uk" version="2.0">
 <context>
  <name>Audio</name>
  <message>
   <location filename="../../plugins/jinglertp/audio.ui" line="20"/>
   <source>Audio/Video chat</source>
   <translation>Аудіо/Відео чат</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/audio.ui" line="41"/>
   <source>Audio</source>
   <translation>Аудіо</translation>
  </message>
 </context>
 <context>
  <name>JingleRtp</name>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="82"/>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="184"/>
   <source>Jingle RTP</source>
   <translation>Jingle RTP</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="83"/>
   <source>Implements XEP-0167: Jingle RTP Sessions</source>
   <translation>XEP-0167: Сесії Jingle RTP</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="162"/>
   <source>When incoming voice or video call received</source>
   <translation>Коли отримано вхидний голосовий або відео виклик</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="170"/>
   <source>When incoming voice or video call missed</source>
   <translation>Коли пропущено вхідний голосовий або відео виклик</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="407"/>
   <source>Incoming %1 call from %2</source>
   <translation>%1 вхідний(их) виклик(ів) від %2</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="408"/>
   <source>Missed %1 call from %2</source>
   <translation>%1 пропущений(их) виклик(ів) від %2</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="409"/>
   <source>video</source>
   <translation>відео</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="409"/>
   <source>voice</source>
   <translation>голосовий</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="420"/>
   <source>Missed call!</source>
   <translation>Пропущений виклик!</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="420"/>
   <source>Incoming call!</source>
   <translation>Вхідний виклик!</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="456"/>
   <source>%1 - Chat</source>
   <translation>%1 - Чат</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="500"/>
   <source>Jingle RTP Sessions</source>
   <translation>Сесії Jingle RTP</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="501"/>
   <source>Audio/Video chat via Jingle RTP</source>
   <translation>Аудіо/Відео чат за допомогою Jingle RTP</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="667"/>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="775"/>
   <source>Video call</source>
   <translation>Відео виклик</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="671"/>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="764"/>
   <source>Voice call</source>
   <translation>Голосовий виклик</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="674"/>
   <source>Call cancelled</source>
   <translation>Виклик скасовано</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="677"/>
   <source>Call rejected</source>
   <translation>Виклик відхилено</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="680"/>
   <source>Call finished</source>
   <translation>Виклик завершено</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="684"/>
   <source>Call error</source>
   <translation>Помилка виклика</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="685"/>
   <source>Call error (%1)</source>
   <translation>Помилка виклику (%1)</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="786"/>
   <source>Hangup</source>
   <translation>Завершити виклик</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="798"/>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="1034"/>
   <source>Microphone OFF</source>
   <translation>Вимкнути мікрофон</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="1013"/>
   <source>Microphone ON</source>
   <translation>Увімкнути мікрофон</translation>
  </message>
 </context>
 <context>
  <name>JingleRtpOptions</name>
  <message>
   <location filename="../../plugins/jinglertp/jinglertpoptions.ui" line="14"/>
   <source>Jingle RTP options</source>
   <translation>Налаштування Jingle RTP</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertpoptions.ui" line="46"/>
   <source>Audio</source>
   <translation>Аудіо</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertpoptions.ui" line="147"/>
   <location filename="../../plugins/jinglertp/jinglertpoptions.ui" line="221"/>
   <source>Codec</source>
   <translation>Кодек</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertpoptions.ui" line="160"/>
   <source>Notify Interval, ms</source>
   <translation>Інтервал сповіщень, мс</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertpoptions.ui" line="170"/>
   <source>Sample rate, Hz</source>
   <translation>Частота, Гц</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertpoptions.ui" line="191"/>
   <source>Video</source>
   <translation>Відео</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertpoptions.ui" line="241"/>
   <source>Use RTCP</source>
   <translation>Використовувати RTCP</translation>
  </message>
 </context>
</TS>