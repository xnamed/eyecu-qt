<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="uk">
<context>
    <name>IqAuthFeatureFactory</name>
    <message>
        <source>Query Authentication</source>
        <translation>Автентифікація запиту</translation>
    </message>
    <message>
        <source>Allow you to log on the Jabber server without support SASL authentication</source>
        <translation>Дозволяє вам залогінитися на сервер Jabber&apos;а без автентифікації SASL</translation>
    </message>
</context>
</TS>
