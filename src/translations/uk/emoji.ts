<?xml version="1.0" ?><!DOCTYPE TS><TS language="uk" version="2.0">
 <context>
  <name>Emoji</name>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="51"/>
   <source>Emoji</source>
   <translation>Емодзі</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="52"/>
   <source>Allows to use emoji in messages</source>
   <translation>Дозволити використовувати емодзі в повідомленнях</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="87"/>
   <source>Smileys &amp; People</source>
   <translation>Смайлики і Люди</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="88"/>
   <source>Animals &amp; Nature</source>
   <translation>Тварини і Природа</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="89"/>
   <source>Food &amp; Drink</source>
   <translation>Їжа і Напої</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="90"/>
   <source>Travel &amp; Places</source>
   <translation>Подорожі і Місця</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="91"/>
   <source>Symbols</source>
   <translation>Символи</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="92"/>
   <source>Activities</source>
   <translation>Заняття</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="94"/>
   <source>Flags</source>
   <translation>Прапори</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="93"/>
   <source>Objects</source>
   <translation>Об&apos;єкти</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="166"/>
   <source>Message windows</source>
   <translation>Вікна повідомлення</translation>
  </message>
 </context>
 <context>
  <name>EmojiOptions</name>
  <message>
   <location filename="../../plugins/emoji/emojioptions.cpp" line="14"/>
   <source>Do not use emoji</source>
   <translation>Не використовувати емодзі</translation>
  </message>
 </context>
 <context>
  <name>EmojiOptionsClass</name>
  <message>
   <location filename="../../plugins/emoji/emojioptions.ui" line="29"/>
   <source>Emoji set:</source>
   <translation>Набір емодзі:</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/emojioptions.ui" line="39"/>
   <source>Menu icon size:</source>
   <translation>Розмір іконки меню:</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/emojioptions.ui" line="49"/>
   <source>Chat icon size:</source>
   <translation>Розмір іконки чату:</translation>
  </message>
 </context>
 <context>
  <name>IconSizeSpinBox</name>
  <message numerus="yes">
   <location filename="../../plugins/emoji/iconsizespinbox.cpp" line="62"/>
   <source>pixel(s)</source>
   <translation><numerusform>піксель</numerusform><numerusform>пікселі</numerusform><numerusform>пікселів</numerusform></translation>
  </message>
 </context>
 <context>
  <name>SelectIconMenu</name>
  <message>
   <location filename="../../plugins/emoji/selecticonmenu.cpp" line="129"/>
   <source>Fitzpatrick type %1</source>
   <comment>https://en.wikipedia.org/wiki/Fitzpatrick_scale</comment>
   <translation>Fitzpatrick тип %1</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/selecticonmenu.cpp" line="143"/>
   <source>Skin color</source>
   <translation>Колір скіну</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/selecticonmenu.cpp" line="112"/>
   <source>Default</source>
   <translation>За замовчанням</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/selecticonmenu.cpp" line="129"/>
   <source>1 or 2</source>
   <translation>1 або 2</translation>
  </message>
 </context>
</TS>