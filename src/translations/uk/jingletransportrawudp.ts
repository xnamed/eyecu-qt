<?xml version="1.0" ?><!DOCTYPE TS><TS language="uk" version="2.0">
 <context>
  <name>JingleTransportRawUdp</name>
  <message>
   <location filename="../../plugins/jingletransportrawudp/jingletransportrawudp.cpp" line="16"/>
   <location filename="../../plugins/jingletransportrawudp/jingletransportrawudp.cpp" line="286"/>
   <source>Jingle RAW-UDP Transport</source>
   <translation>Транспорт Jingle RAW-UDP</translation>
  </message>
  <message>
   <location filename="../../plugins/jingletransportrawudp/jingletransportrawudp.cpp" line="17"/>
   <source>Implements XEP-0177: Jingle RAW-UDP transport method</source>
   <translation>XEP-0177: Метод передачі Jingle RAW-UDP</translation>
  </message>
  <message>
   <location filename="../../plugins/jingletransportrawudp/jingletransportrawudp.cpp" line="287"/>
   <source>Allows using RAW-UDP transport in Jingle sesions</source>
   <translation>Дозволяє використовувати транспорт RAW-UDP у сесіях Jingle</translation>
  </message>
 </context>
</TS>