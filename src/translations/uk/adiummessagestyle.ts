<?xml version="1.0" ?><!DOCTYPE TS><TS language="uk" version="2.0">
 <context>
  <name>AdiumMessageStyle</name>
  <message>
   <location filename="../../plugins/adiummessagestyle/adiummessagestyle.cpp" line="632"/>
   <source>hh:mm</source>
   <translation>hh:mm</translation>
  </message>
  <message>
   <location filename="../../plugins/adiummessagestyle/adiummessagestyle.cpp" line="643"/>
   <source>hh:mm:ss</source>
   <translation>hh:mm:ss</translation>
  </message>
  <message>
   <location filename="../../plugins/adiummessagestyle/adiummessagestyle.cpp" line="840"/>
   <source>Failed to load message style. Press clear window button to retry.</source>
   <translation>Не вдалося завантажити стиль чату. Натисніть кнопку &quot;Очистити вікно&quot; щоб спробувати знову.</translation>
  </message>
 </context>
 <context>
  <name>AdiumMessageStyleEngine</name>
  <message>
   <location filename="../../plugins/adiummessagestyle/adiummessagestyleengine.cpp" line="27"/>
   <source>Adium Message Style</source>
   <translation>Стиль повідомлень Adium</translation>
  </message>
  <message>
   <location filename="../../plugins/adiummessagestyle/adiummessagestyleengine.cpp" line="28"/>
   <source>Allows to use a Adium style in message design</source>
   <translation>Дозволяє використовувати стиль Adium в дизайні чату</translation>
  </message>
  <message>
   <location filename="../../plugins/adiummessagestyle/adiummessagestyleengine.cpp" line="74"/>
   <source>Adium</source>
   <translation>Adium</translation>
  </message>
 </context>
 <context>
  <name>AdiumOptionsWidget</name>
  <message>
   <location filename="../../plugins/adiummessagestyle/adiumoptionswidget.cpp" line="12"/>
   <source>Parameters</source>
   <translation>Параметри</translation>
  </message>
  <message>
   <location filename="../../plugins/adiummessagestyle/adiumoptionswidget.cpp" line="13"/>
   <source>Background</source>
   <translation>Тло</translation>
  </message>
  <message>
   <location filename="../../plugins/adiummessagestyle/adiumoptionswidget.cpp" line="21"/>
   <location filename="../../plugins/adiummessagestyle/adiumoptionswidget.cpp" line="23"/>
   <source>Default</source>
   <translation>За замовчанням</translation>
  </message>
  <message>
   <location filename="../../plugins/adiummessagestyle/adiumoptionswidget.cpp" line="36"/>
   <source>Normal</source>
   <translation>Звичайний</translation>
  </message>
  <message>
   <location filename="../../plugins/adiummessagestyle/adiumoptionswidget.cpp" line="37"/>
   <source>Center</source>
   <translation>По центру</translation>
  </message>
  <message>
   <location filename="../../plugins/adiummessagestyle/adiumoptionswidget.cpp" line="38"/>
   <source>Title</source>
   <translation>Нагорі</translation>
  </message>
  <message>
   <location filename="../../plugins/adiummessagestyle/adiumoptionswidget.cpp" line="39"/>
   <source>Title center</source>
   <translation>Нагорі по центру</translation>
  </message>
  <message>
   <location filename="../../plugins/adiummessagestyle/adiumoptionswidget.cpp" line="40"/>
   <source>Scale</source>
   <translation>Розтягнути</translation>
  </message>
  <message>
   <location filename="../../plugins/adiummessagestyle/adiumoptionswidget.cpp" line="125"/>
   <source>Select font family and size</source>
   <translation>Виберіть фришт</translation>
  </message>
  <message>
   <location filename="../../plugins/adiummessagestyle/adiumoptionswidget.cpp" line="158"/>
   <source>Select background image</source>
   <translation>Виберіть фонове зображення</translation>
  </message>
  <message>
   <location filename="../../plugins/adiummessagestyle/adiumoptionswidget.cpp" line="158"/>
   <source>Image Files (*.png *.jpg *.bmp *.gif)</source>
   <translation>Файли зображень</translation>
  </message>
 </context>
 <context>
  <name>AdiumOptionsWidgetClass</name>
  <message>
   <location filename="../../plugins/adiummessagestyle/adiumoptionswidget.ui" line="39"/>
   <source>Variant:</source>
   <translation>Варіант:</translation>
  </message>
  <message>
   <location filename="../../plugins/adiummessagestyle/adiumoptionswidget.ui" line="65"/>
   <source>Font:</source>
   <translation>Шрифт:</translation>
  </message>
  <message>
   <location filename="../../plugins/adiummessagestyle/adiumoptionswidget.ui" line="85"/>
   <location filename="../../plugins/adiummessagestyle/adiumoptionswidget.ui" line="158"/>
   <location filename="../../plugins/adiummessagestyle/adiumoptionswidget.cpp" line="33"/>
   <source>Change...</source>
   <translation>Змінити...</translation>
  </message>
  <message>
   <location filename="../../plugins/adiummessagestyle/adiumoptionswidget.ui" line="95"/>
   <location filename="../../plugins/adiummessagestyle/adiumoptionswidget.ui" line="165"/>
   <source>Reset</source>
   <translation>Скинути</translation>
  </message>
  <message>
   <location filename="../../plugins/adiummessagestyle/adiumoptionswidget.ui" line="112"/>
   <source>Color:</source>
   <translation>Колір:</translation>
  </message>
  <message>
   <location filename="../../plugins/adiummessagestyle/adiumoptionswidget.ui" line="138"/>
   <source>Image:</source>
   <translation>Зображення:</translation>
  </message>
  <message>
   <location filename="../../plugins/adiummessagestyle/adiumoptionswidget.ui" line="172"/>
   <source>Layout:</source>
   <translation>Положення:</translation>
  </message>
 </context>
</TS>