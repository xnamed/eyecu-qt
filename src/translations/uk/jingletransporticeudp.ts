<?xml version="1.0" ?><!DOCTYPE TS><TS language="uk" version="2.0">
 <context>
  <name>JingleTransportIceUdp</name>
  <message>
   <location filename="../../plugins/jingletransporticeudp/jingletransporticeudp.cpp" line="13"/>
   <location filename="../../plugins/jingletransporticeudp/jingletransporticeudp.cpp" line="113"/>
   <source>Jingle ICE-UDP Transport</source>
   <translation>Транспорт Jingle ICE-UDP</translation>
  </message>
  <message>
   <location filename="../../plugins/jingletransporticeudp/jingletransporticeudp.cpp" line="14"/>
   <source>Implements XEP-0176: Jingle ICE-UDP transport Method</source>
   <translation>XEP-0176: Метод передачі Jingle ICE-UDP</translation>
  </message>
  <message>
   <location filename="../../plugins/jingletransporticeudp/jingletransporticeudp.cpp" line="114"/>
   <source>Allows using ICE-UDP transport in Jingle sesions</source>
   <translation>Дозволяє використовувати транспорт ICE-UDP у сесіях Jingle</translation>
  </message>
 </context>
</TS>