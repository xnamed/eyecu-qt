<?xml version="1.0" ?><!DOCTYPE TS><TS language="uk" version="2.0">
 <context>
  <name>Activity</name>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="82"/>
   <source>Doing chores</source>
   <translation>Працює по дому</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="82"/>
   <source>Drinking</source>
   <translation>Випиває</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="82"/>
   <source>Eating</source>
   <translation>Їсть</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="82"/>
   <source>Exercising</source>
   <translation>Займається</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="83"/>
   <source>Grooming</source>
   <translation>Доглядає за тваринами</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="83"/>
   <source>Having appointment</source>
   <translation>На зустрічі</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="83"/>
   <source>Inactive</source>
   <translation>Неактивний</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="83"/>
   <source>Relaxing</source>
   <translation>Розслабляється</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="84"/>
   <source>Talking</source>
   <translation>Розмовляє</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="84"/>
   <source>Traveling</source>
   <translation>Подорожує</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="84"/>
   <source>Working</source>
   <translation>Працює</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="84"/>
   <source>Buying groceries</source>
   <translation>Купує продукти</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="85"/>
   <source>Cleaning</source>
   <translation>Прибирає</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="85"/>
   <source>Cooking</source>
   <translation>Готує</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="85"/>
   <source>Doing maintenance</source>
   <translation>Обслуговує техніку</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="85"/>
   <source>Writing</source>
   <translation>Пише</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="86"/>
   <source>Doing the dishes</source>
   <translation>Миє посуд</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="86"/>
   <source>Doing the laundry</source>
   <translation>Займається пранням</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="86"/>
   <source>Gardening</source>
   <translation>Займається садівництвом</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="86"/>
   <source>Running an errand</source>
   <translation>Пішов у справах</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="87"/>
   <source>Walking the dog</source>
   <translation>Вигулює собаку</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="87"/>
   <source>Having a beer</source>
   <translation>П&apos;є пиво</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="87"/>
   <source>Having coffee</source>
   <translation>П&apos;є каву</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="87"/>
   <source>Having tea</source>
   <translation>П&apos;є чай</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="88"/>
   <source>Having a snack</source>
   <translation>Перекусує</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="88"/>
   <source>Having breakfast</source>
   <translation>Снідає</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="88"/>
   <source>Having dinner</source>
   <translation>Вечеряє</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="88"/>
   <source>Having lunch</source>
   <translation>Обідає</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="89"/>
   <source>Cycling</source>
   <translation>Катається на велосипеді</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="89"/>
   <source>Dancing</source>
   <translation>Танцює</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="89"/>
   <source>Hiking</source>
   <translation>На пішій прогулянці</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="89"/>
   <source>Jogging</source>
   <translation>Бігає підтюпцем</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="90"/>
   <source>Playing sports</source>
   <translation>Займається спортом</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="90"/>
   <source>Running</source>
   <translation>Бігає</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="90"/>
   <source>Skiing</source>
   <translation>Катаюється на лижах</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="90"/>
   <source>Swimming</source>
   <translation>Плаває</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="91"/>
   <source>Working out</source>
   <translation>Займається в тренажерці</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="91"/>
   <source>At the spa</source>
   <translation>У спа</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="91"/>
   <source>Brushing teeth</source>
   <translation>Чистить зуби</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="91"/>
   <source>Getting a haircut</source>
   <translation>Підстригається</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="92"/>
   <source>Shaving</source>
   <translation>Бриється</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="92"/>
   <source>Taking a bath</source>
   <translation>Приймає ванну</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="92"/>
   <source>Taking a shower</source>
   <translation>Приймає душ</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="92"/>
   <source>Day off</source>
   <translation>У відгулі</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="93"/>
   <source>Hanging out</source>
   <translation>Зависає</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="93"/>
   <source>Hiding</source>
   <translation>Ховається</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="93"/>
   <source>On vacation</source>
   <translation>У відпустці</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="93"/>
   <source>Praying</source>
   <translation>Молиться</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="94"/>
   <source>Scheduled holiday</source>
   <translation>Запланував свята</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="94"/>
   <source>Sleeping</source>
   <translation>Спить</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="94"/>
   <source>Thinking</source>
   <translation>Думає</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="94"/>
   <source>Fishing</source>
   <translation>Рибалить</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="95"/>
   <source>Gaming</source>
   <translation>Грає</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="95"/>
   <source>Going out</source>
   <translation>На побаченні</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="95"/>
   <source>Partying</source>
   <translation>На гулянці</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="95"/>
   <source>Reading</source>
   <translation>Читає</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="96"/>
   <source>Rehearsing</source>
   <translation>Повторює</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="96"/>
   <source>Shopping</source>
   <translation>Пішов за покупками</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="96"/>
   <source>Smoking</source>
   <translation>Палить</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="96"/>
   <source>Socializing</source>
   <translation>Соціалізується</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="97"/>
   <source>Sunbathing</source>
   <translation>Загорає</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="97"/>
   <source>Watching TV</source>
   <translation>Дивиться ТБ</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="97"/>
   <source>Watching a movie</source>
   <translation>Дивиться фільм</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="97"/>
   <source>In real life</source>
   <translation>Пішов у реал</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="98"/>
   <source>On the phone</source>
   <translation>Розмовляє телефоном</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="98"/>
   <source>On video phone</source>
   <translation>На відеодзвінку</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="98"/>
   <source>Commuting</source>
   <translation>В дорозі</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="98"/>
   <source>Travelling by bicycle</source>
   <translation>Подорожує велосипедом</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="99"/>
   <source>Driving</source>
   <translation>Веде автомобіль</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="99"/>
   <source>In a car</source>
   <translation>У автомобілі</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="99"/>
   <source>On a bus</source>
   <translation>У автобусі</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="99"/>
   <source>On a plane</source>
   <translation>У літаку</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="100"/>
   <source>On a train</source>
   <translation>У потязі</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="100"/>
   <source>On a trip</source>
   <translation>У подорожі</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="100"/>
   <source>Walking</source>
   <translation>Прогулюється</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="100"/>
   <source>Coding</source>
   <translation>Займається кодингом</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="101"/>
   <source>In a meeting</source>
   <translation>На зустрічі</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="101"/>
   <source>Studying</source>
   <translation>Навчається</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="101"/>
   <source>No activity</source>
   <translation>Нічим не займається</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="110"/>
   <location filename="../../plugins/activity/activity.cpp" line="399"/>
   <source>User Activity</source>
   <translation>Активність користувача</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="111"/>
   <source>Implements XEP-0108: User Activity</source>
   <translation>XEP-0108: Активність користувача</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="213"/>
   <location filename="../../plugins/activity/activity.cpp" line="214"/>
   <source>Set activity</source>
   <translation>Встановити активність</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="213"/>
   <source>Ctrl+F6</source>
   <comment>Set activity (for all accounts)</comment>
   <translation>Ctrl+F6</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="214"/>
   <source>F6</source>
   <comment>Set activity (for an account)</comment>
   <translation>F6</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="250"/>
   <source>When user activity changed</source>
   <translation>Коли було змінено активність користувача</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="257"/>
   <location filename="../../plugins/activity/activity.cpp" line="549"/>
   <location filename="../../plugins/activity/activity.cpp" line="839"/>
   <source>Activity</source>
   <translation>Активність</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="400"/>
   <source>Supports publishing of current user activity</source>
   <translation>Підтримує публікацію активності користувача</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="404"/>
   <source>User Activity Notification</source>
   <translation>Сповіщення про Активність Користувача</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="405"/>
   <source>Receives notification of current user activity</source>
   <translation>Отримує сповіщення про активність корустувача</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="502"/>
   <source>Show user activity icons</source>
   <translation>Показати іконки активності користувача</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="506"/>
   <source>Display user activity icon</source>
   <translation>Показувати іконки іконки активності користувача</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="507"/>
   <source>Activity events in chat</source>
   <translation>Події, що пов&apos;язані з активностями у чаті</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="576"/>
   <source>%1: %2</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="717"/>
   <source>%1 changed activity to %2</source>
   <translation>%1 змінив поточну активність на %2</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="876"/>
   <source>Changed activity to: %1</source>
   <translation>Змінив поточну активність на %1</translation>
  </message>
 </context>
 <context>
  <name>ActivitySelect</name>
  <message>
   <location filename="../../plugins/activity/activityselect.ui" line="26"/>
   <source>Activity select</source>
   <translation>Вибрати активність</translation>
  </message>
 </context>
</TS>