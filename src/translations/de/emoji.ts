<?xml version="1.0" ?><!DOCTYPE TS><TS language="de" version="2.0">
 <context>
  <name>Emoji</name>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="51"/>
   <source>Emoji</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="52"/>
   <source>Allows to use emoji in messages</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="87"/>
   <source>Smileys &amp; People</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="88"/>
   <source>Animals &amp; Nature</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="89"/>
   <source>Food &amp; Drink</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="90"/>
   <source>Travel &amp; Places</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="91"/>
   <source>Symbols</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="92"/>
   <source>Activities</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="94"/>
   <source>Flags</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="93"/>
   <source>Objects</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="166"/>
   <source>Message windows</source>
   <translation type="unfinished"/>
  </message>
 </context>
 <context>
  <name>EmojiOptions</name>
  <message>
   <location filename="../../plugins/emoji/emojioptions.cpp" line="14"/>
   <source>Do not use emoji</source>
   <translation type="unfinished"/>
  </message>
 </context>
 <context>
  <name>EmojiOptionsClass</name>
  <message>
   <location filename="../../plugins/emoji/emojioptions.ui" line="29"/>
   <source>Emoji set:</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/emoji/emojioptions.ui" line="39"/>
   <source>Menu icon size:</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/emoji/emojioptions.ui" line="49"/>
   <source>Chat icon size:</source>
   <translation type="unfinished"/>
  </message>
 </context>
 <context>
  <name>IconSizeSpinBox</name>
  <message numerus="yes">
   <location filename="../../plugins/emoji/iconsizespinbox.cpp" line="62"/>
   <source>pixel(s)</source>
   <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
  </message>
 </context>
 <context>
  <name>SelectIconMenu</name>
  <message>
   <location filename="../../plugins/emoji/selecticonmenu.cpp" line="129"/>
   <source>Fitzpatrick type %1</source>
   <comment>https://en.wikipedia.org/wiki/Fitzpatrick_scale</comment>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/emoji/selecticonmenu.cpp" line="143"/>
   <source>Skin color</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/emoji/selecticonmenu.cpp" line="112"/>
   <source>Default</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/emoji/selecticonmenu.cpp" line="129"/>
   <source>1 or 2</source>
   <translation type="unfinished"/>
  </message>
 </context>
</TS>