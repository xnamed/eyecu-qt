<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
 <context>
  <name>JingleTransportIceUdp</name>
  <message>
   <location line="13" filename="../../plugins/jingletransporticeudp/jingletransporticeudp.cpp"/>
   <location line="113" filename="../../plugins/jingletransporticeudp/jingletransporticeudp.cpp"/>
   <source>Jingle ICE-UDP Transport</source>
   <translation>Jingle ICE-UDP Transport</translation>
  </message>
  <message>
   <location line="14" filename="../../plugins/jingletransporticeudp/jingletransporticeudp.cpp"/>
   <source>Implements XEP-0176: Jingle ICE-UDP transport Method</source>
   <translation>Implements XEP-0176: Jingle ICE-UDP transport Method</translation>
  </message>
  <message>
   <location line="114" filename="../../plugins/jingletransporticeudp/jingletransporticeudp.cpp"/>
   <source>Allows using ICE-UDP transport in Jingle sesions</source>
   <translation>Allows using ICE-UDP transport in Jingle sesions</translation>
  </message>
 </context>
</TS>
