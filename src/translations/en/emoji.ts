<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
 <context>
  <name>Emoji</name>
  <message>
   <location line="51" filename="../../plugins/emoji/emoji.cpp"/>
   <source>Emoji</source>
   <translation>Emoji</translation>
  </message>
  <message>
   <location line="52" filename="../../plugins/emoji/emoji.cpp"/>
   <source>Allows to use emoji in messages</source>
   <translation>Allows to use emoji in messages</translation>
  </message>
  <message>
   <location line="87" filename="../../plugins/emoji/emoji.cpp"/>
   <source>Smileys &amp; People</source>
   <translation>Smileys &amp; People</translation>
  </message>
  <message>
   <location line="88" filename="../../plugins/emoji/emoji.cpp"/>
   <source>Animals &amp; Nature</source>
   <translation>Animals &amp; Nature</translation>
  </message>
  <message>
   <location line="89" filename="../../plugins/emoji/emoji.cpp"/>
   <source>Food &amp; Drink</source>
   <translation>Food &amp; Drink</translation>
  </message>
  <message>
   <location line="90" filename="../../plugins/emoji/emoji.cpp"/>
   <source>Travel &amp; Places</source>
   <translation>Travel &amp; Places</translation>
  </message>
  <message>
   <location line="91" filename="../../plugins/emoji/emoji.cpp"/>
   <source>Symbols</source>
   <translation>Symbols</translation>
  </message>
  <message>
   <location line="92" filename="../../plugins/emoji/emoji.cpp"/>
   <source>Activities</source>
   <translation>Activities</translation>
  </message>
  <message>
   <location line="94" filename="../../plugins/emoji/emoji.cpp"/>
   <source>Flags</source>
   <translation>Flags</translation>
  </message>
  <message>
   <location line="93" filename="../../plugins/emoji/emoji.cpp"/>
   <source>Objects</source>
   <translation>Objects</translation>
  </message>
  <message>
   <location line="166" filename="../../plugins/emoji/emoji.cpp"/>
   <source>Message windows</source>
   <translation>Message windows</translation>
  </message>
 </context>
 <context>
  <name>EmojiOptions</name>
  <message>
   <location line="14" filename="../../plugins/emoji/emojioptions.cpp"/>
   <source>Do not use emoji</source>
   <translation>Do not use emoji</translation>
  </message>
 </context>
 <context>
  <name>EmojiOptionsClass</name>
  <message>
   <location line="29" filename="../../plugins/emoji/emojioptions.ui"/>
   <source>Emoji set:</source>
   <translation>Emoji set:</translation>
  </message>
  <message>
   <location line="39" filename="../../plugins/emoji/emojioptions.ui"/>
   <source>Menu icon size:</source>
   <translation>Menu icon size:</translation>
  </message>
  <message>
   <location line="49" filename="../../plugins/emoji/emojioptions.ui"/>
   <source>Chat icon size:</source>
   <translation>Chat icon size:</translation>
  </message>
 </context>
 <context>
  <name>IconSizeSpinBox</name>
  <message numerus="yes">
   <location line="62" filename="../../plugins/emoji/iconsizespinbox.cpp"/>
   <source>pixel(s)</source>
   <translation>
    <numerusform>pixel</numerusform>
    <numerusform>pixels</numerusform>
   </translation>
  </message>
 </context>
 <context>
  <name>SelectIconMenu</name>
  <message>
   <location line="129" filename="../../plugins/emoji/selecticonmenu.cpp"/>
   <source>Fitzpatrick type %1</source>
   <comment>https://en.wikipedia.org/wiki/Fitzpatrick_scale</comment>
   <translation>Fitzpatrick type %1</translation>
  </message>
  <message>
   <location line="143" filename="../../plugins/emoji/selecticonmenu.cpp"/>
   <source>Skin color</source>
   <translation>Skin color</translation>
  </message>
  <message>
   <location line="112" filename="../../plugins/emoji/selecticonmenu.cpp"/>
   <source>Default</source>
   <translation>Default</translation>
  </message>
  <message>
   <location line="129" filename="../../plugins/emoji/selecticonmenu.cpp"/>
   <source>1 or 2</source>
   <translation>1 or 2</translation>
  </message>
 </context>
</TS>
