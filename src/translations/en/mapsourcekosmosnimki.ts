<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
 <context>
  <name>MapSourceKosmosnimki</name>
  <message>
   <location line="12" filename="../../plugins/mapsourcekosmosnimki/mapsourcekosmosnimki.cpp"/>
   <source>Kosmosnimki map source</source>
   <translation>Kosmosnimki map source</translation>
  </message>
  <message>
   <location line="13" filename="../../plugins/mapsourcekosmosnimki/mapsourcekosmosnimki.cpp"/>
   <source>Allows Map plugin to use Kosmosnimki as map source</source>
   <translation>Allows Map plugin to use Kosmosnimki as map source</translation>
  </message>
 </context>
</TS>
