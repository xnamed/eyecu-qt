<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
 <context>
  <name>Audio</name>
  <message>
   <location line="20" filename="../../plugins/jinglertp/audio.ui"/>
   <source>Audio/Video chat</source>
   <translation>Audio/Video chat</translation>
  </message>
  <message>
   <location line="41" filename="../../plugins/jinglertp/audio.ui"/>
   <source>Audio</source>
   <translation>Audio</translation>
  </message>
 </context>
 <context>
  <name>JingleRtp</name>
  <message>
   <location line="82" filename="../../plugins/jinglertp/jinglertp.cpp"/>
   <location line="184" filename="../../plugins/jinglertp/jinglertp.cpp"/>
   <source>Jingle RTP</source>
   <translation>Jingle RTP</translation>
  </message>
  <message>
   <location line="83" filename="../../plugins/jinglertp/jinglertp.cpp"/>
   <source>Implements XEP-0167: Jingle RTP Sessions</source>
   <translation>Implements XEP-0167: Jingle RTP Sessions</translation>
  </message>
  <message>
   <location line="162" filename="../../plugins/jinglertp/jinglertp.cpp"/>
   <source>When incoming voice or video call received</source>
   <translation>When incoming voice or video call received</translation>
  </message>
  <message>
   <location line="170" filename="../../plugins/jinglertp/jinglertp.cpp"/>
   <source>When incoming voice or video call missed</source>
   <translation>When incoming voice or video call missed</translation>
  </message>
  <message>
   <location line="407" filename="../../plugins/jinglertp/jinglertp.cpp"/>
   <source>Incoming %1 call from %2</source>
   <translation>Incoming %1 call from %2</translation>
  </message>
  <message>
   <location line="408" filename="../../plugins/jinglertp/jinglertp.cpp"/>
   <source>Missed %1 call from %2</source>
   <translation>Missed %1 call from %2</translation>
  </message>
  <message>
   <location line="409" filename="../../plugins/jinglertp/jinglertp.cpp"/>
   <source>video</source>
   <translation>video</translation>
  </message>
  <message>
   <location line="409" filename="../../plugins/jinglertp/jinglertp.cpp"/>
   <source>voice</source>
   <translation>voice</translation>
  </message>
  <message>
   <location line="420" filename="../../plugins/jinglertp/jinglertp.cpp"/>
   <source>Missed call!</source>
   <translation>Missed call!</translation>
  </message>
  <message>
   <location line="420" filename="../../plugins/jinglertp/jinglertp.cpp"/>
   <source>Incoming call!</source>
   <translation>Incoming call!</translation>
  </message>
  <message>
   <location line="456" filename="../../plugins/jinglertp/jinglertp.cpp"/>
   <source>%1 - Chat</source>
   <translation>%1 - Chat</translation>
  </message>
  <message>
   <location line="500" filename="../../plugins/jinglertp/jinglertp.cpp"/>
   <source>Jingle RTP Sessions</source>
   <translation>Jingle RTP Sessions</translation>
  </message>
  <message>
   <location line="501" filename="../../plugins/jinglertp/jinglertp.cpp"/>
   <source>Audio/Video chat via Jingle RTP</source>
   <translation>Audio/Video chat via Jingle RTP</translation>
  </message>
  <message>
   <location line="667" filename="../../plugins/jinglertp/jinglertp.cpp"/>
   <location line="775" filename="../../plugins/jinglertp/jinglertp.cpp"/>
   <source>Video call</source>
   <translation>Video call</translation>
  </message>
  <message>
   <location line="671" filename="../../plugins/jinglertp/jinglertp.cpp"/>
   <location line="764" filename="../../plugins/jinglertp/jinglertp.cpp"/>
   <source>Voice call</source>
   <translation>Voice call</translation>
  </message>
  <message>
   <location line="674" filename="../../plugins/jinglertp/jinglertp.cpp"/>
   <source>Call cancelled</source>
   <translation>Call cancelled</translation>
  </message>
  <message>
   <location line="677" filename="../../plugins/jinglertp/jinglertp.cpp"/>
   <source>Call rejected</source>
   <translation>Call rejected</translation>
  </message>
  <message>
   <location line="680" filename="../../plugins/jinglertp/jinglertp.cpp"/>
   <source>Call finished</source>
   <translation>Call finished</translation>
  </message>
  <message>
   <location line="684" filename="../../plugins/jinglertp/jinglertp.cpp"/>
   <source>Call error</source>
   <translation>Call error</translation>
  </message>
  <message>
   <location line="685" filename="../../plugins/jinglertp/jinglertp.cpp"/>
   <source>Call error (%1)</source>
   <translation>Call error (%1)</translation>
  </message>
  <message>
   <location line="786" filename="../../plugins/jinglertp/jinglertp.cpp"/>
   <source>Hangup</source>
   <translation>Hangup</translation>
  </message>
  <message>
   <location line="798" filename="../../plugins/jinglertp/jinglertp.cpp"/>
   <location line="1034" filename="../../plugins/jinglertp/jinglertp.cpp"/>
   <source>Microphone OFF</source>
   <translation>Microphone OFF</translation>
  </message>
  <message>
   <location line="1013" filename="../../plugins/jinglertp/jinglertp.cpp"/>
   <source>Microphone ON</source>
   <translation>Microphone ON</translation>
  </message>
 </context>
 <context>
  <name>JingleRtpOptions</name>
  <message>
   <location line="14" filename="../../plugins/jinglertp/jinglertpoptions.ui"/>
   <source>Jingle RTP options</source>
   <translation>Jingle RTP options</translation>
  </message>
  <message>
   <location line="46" filename="../../plugins/jinglertp/jinglertpoptions.ui"/>
   <source>Audio</source>
   <translation>Audio</translation>
  </message>
  <message>
   <location line="147" filename="../../plugins/jinglertp/jinglertpoptions.ui"/>
   <location line="221" filename="../../plugins/jinglertp/jinglertpoptions.ui"/>
   <source>Codec</source>
   <translation>Codec</translation>
  </message>
  <message>
   <location line="160" filename="../../plugins/jinglertp/jinglertpoptions.ui"/>
   <source>Notify Interval, ms</source>
   <translation>Notify Interval, ms</translation>
  </message>
  <message>
   <location line="170" filename="../../plugins/jinglertp/jinglertpoptions.ui"/>
   <source>Sample rate, Hz</source>
   <translation>Sample rate, Hz</translation>
  </message>
  <message>
   <location line="191" filename="../../plugins/jinglertp/jinglertpoptions.ui"/>
   <source>Video</source>
   <translation>Video</translation>
  </message>
  <message>
   <location line="241" filename="../../plugins/jinglertp/jinglertpoptions.ui"/>
   <source>Use RTCP</source>
   <translation>Use RTCP</translation>
  </message>
 </context>
</TS>
