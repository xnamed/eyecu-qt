<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
 <context>
  <name>BobUrlHandler</name>
  <message>
   <location line="10" filename="../../plugins/boburlhandler/boburlhandler.cpp"/>
   <source>BOB URL handler</source>
   <translation>BOB URL handler</translation>
  </message>
  <message>
   <location line="11" filename="../../plugins/boburlhandler/boburlhandler.cpp"/>
   <source>Allows URL processor plugin to handle Bits of Binary links (cid: scheme)</source>
   <translation>Allows URL processor plugin to handle Bits of Binary links (cid: scheme)</translation>
  </message>
 </context>
</TS>
