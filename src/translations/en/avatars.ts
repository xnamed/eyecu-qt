<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
 <context>
  <name>AvatarOptionsWidget</name>
  <message>
   <location line="63" filename="../../plugins/avatars/avataroptionswidget.ui"/>
   <source>Small</source>
   <translation>Small</translation>
  </message>
  <message>
   <location line="68" filename="../../plugins/avatars/avataroptionswidget.ui"/>
   <source>Normal</source>
   <translation>Normal</translation>
  </message>
  <message>
   <location line="73" filename="../../plugins/avatars/avataroptionswidget.ui"/>
   <source>Large</source>
   <translation>Large</translation>
  </message>
  <message>
   <location line="87" filename="../../plugins/avatars/avataroptionswidget.ui"/>
   <source>Avatar position</source>
   <translation>Avatar position</translation>
  </message>
  <message>
   <location line="97" filename="../../plugins/avatars/avataroptionswidget.ui"/>
   <source>Avatar size</source>
   <translation>Avatar size</translation>
  </message>
  <message>
   <location line="107" filename="../../plugins/avatars/avataroptionswidget.ui"/>
   <source>Display avatars for offline contacts grayscaled</source>
   <translation>Display avatars for offline contacts grayscaled</translation>
  </message>
  <message>
   <location line="49" filename="../../plugins/avatars/avataroptionswidget.ui"/>
   <source>At Right</source>
   <translation>At Right</translation>
  </message>
  <message>
   <location line="54" filename="../../plugins/avatars/avataroptionswidget.ui"/>
   <source>At Left</source>
   <translation>At Left</translation>
  </message>
  <message>
   <location line="35" filename="../../plugins/avatars/avataroptionswidget.ui"/>
   <source>Display avatars</source>
   <translation>Display avatars</translation>
  </message>
 </context>
 <context>
  <name>AvatarSizeOptionsWidget</name>
  <message>
   <location line="35" filename="../../plugins/avatars/avatarsizeoptionswidget.ui"/>
   <source>Display empty avatars</source>
   <translation type="unfinished">Display empty avatars</translation>
  </message>
  <message>
   <location line="42" filename="../../plugins/avatars/avatarsizeoptionswidget.ui"/>
   <source>Avatar size</source>
   <translation>Avatar size</translation>
  </message>
  <message>
   <location line="63" filename="../../plugins/avatars/avatarsizeoptionswidget.ui"/>
   <location line="79" filename="../../plugins/avatars/avatarsizeoptionswidget.ui"/>
   <location line="95" filename="../../plugins/avatars/avatarsizeoptionswidget.ui"/>
   <source>px</source>
   <translation>px</translation>
  </message>
  <message>
   <location line="111" filename="../../plugins/avatars/avatarsizeoptionswidget.ui"/>
   <source>Small</source>
   <translation>Small</translation>
  </message>
  <message>
   <location line="121" filename="../../plugins/avatars/avatarsizeoptionswidget.ui"/>
   <source>Normal</source>
   <translation>Normal</translation>
  </message>
  <message>
   <location line="131" filename="../../plugins/avatars/avatarsizeoptionswidget.ui"/>
   <source>Large</source>
   <translation>Large</translation>
  </message>
 </context>
 <context>
  <name>Avatars</name>
  <message>
   <location line="151" filename="../../plugins/avatars/avatars.cpp"/>
   <location line="484" filename="../../plugins/avatars/avatars.cpp"/>
   <location line="490" filename="../../plugins/avatars/avatars.cpp"/>
   <location line="496" filename="../../plugins/avatars/avatars.cpp"/>
   <source>Avatars</source>
   <translation>Avatars</translation>
  </message>
  <message>
   <location line="152" filename="../../plugins/avatars/avatars.cpp"/>
   <source>Allows to set and display avatars</source>
   <translation>Allows to set and display avatars</translation>
  </message>
  <message>
   <location line="990" filename="../../plugins/avatars/avatars.cpp"/>
   <source>Avatar</source>
   <translation>Avatar</translation>
  </message>
  <message>
   <location line="994" filename="../../plugins/avatars/avatars.cpp"/>
   <source>Set avatar</source>
   <translation>Set avatar</translation>
  </message>
  <message>
   <location line="1001" filename="../../plugins/avatars/avatars.cpp"/>
   <source>Clear avatar</source>
   <translation>Clear avatar</translation>
  </message>
  <message>
   <location line="1012" filename="../../plugins/avatars/avatars.cpp"/>
   <source>Custom picture</source>
   <translation>Custom picture</translation>
  </message>
  <message>
   <location line="1016" filename="../../plugins/avatars/avatars.cpp"/>
   <source>Set custom picture</source>
   <translation>Set custom picture</translation>
  </message>
  <message>
   <location line="1023" filename="../../plugins/avatars/avatars.cpp"/>
   <source>Clear custom picture</source>
   <translation>Clear custom picture</translation>
  </message>
  <message>
   <location line="1056" filename="../../plugins/avatars/avatars.cpp"/>
   <source>Select avatar image</source>
   <translation>Select avatar image</translation>
  </message>
  <message>
   <location line="1056" filename="../../plugins/avatars/avatars.cpp"/>
   <source>Image Files (*.png *.jpg *.bmp *.gif)</source>
   <translation>Image Files (*.png *.jpg *.bmp *.gif)</translation>
  </message>
 </context>
</TS>
