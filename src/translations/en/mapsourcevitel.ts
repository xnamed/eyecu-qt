<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
 <context>
  <name>MapSourceViTel</name>
  <message>
   <location line="12" filename="../../plugins/mapsourcevitel/mapsourcevitel.cpp"/>
   <source>Vi-Tel map source</source>
   <translation>Vi-Tel map source</translation>
  </message>
  <message>
   <location line="13" filename="../../plugins/mapsourcevitel/mapsourcevitel.cpp"/>
   <source>Allows Map plugin to use Vi-Tel service as map source</source>
   <translation>Allows Map plugin to use Vi-Tel service as map source</translation>
  </message>
 </context>
</TS>
