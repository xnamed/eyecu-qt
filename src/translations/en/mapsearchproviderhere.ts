<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
 <context>
  <name>MapSearchProviderHere</name>
  <message>
   <location line="15" filename="../../plugins/mapsearchproviderhere/mapsearchproviderhere.cpp"/>
   <source>Map Search Provider Here</source>
   <translation>Map Search Provider Here</translation>
  </message>
  <message>
   <location line="16" filename="../../plugins/mapsearchproviderhere/mapsearchproviderhere.cpp"/>
   <source>Allows to use Nokia's Here service as a map search provider</source>
   <translation>Allows to use Nokia's Here service as a map search provider</translation>
  </message>
  <message>
   <location line="55" filename="../../plugins/mapsearchproviderhere/mapsearchproviderhere.cpp"/>
   <source>Here</source>
   <translation>Here</translation>
  </message>
 </context>
</TS>
