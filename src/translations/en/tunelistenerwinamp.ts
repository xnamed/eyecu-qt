<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
 <context>
  <name>TuneListenerWinamp</name>
  <message>
   <location line="41" filename="../../plugins/tunelistenerwinamp/tunelistenerwinamp.cpp"/>
   <source>Tune Listener WINAMP</source>
   <translation>Tune Listener WINAMP</translation>
  </message>
  <message>
   <location line="42" filename="../../plugins/tunelistenerwinamp/tunelistenerwinamp.cpp"/>
   <source>Allow User Tune plugin to obtain currently playing tune information from WINAMP</source>
   <translation>Allow User Tune plugin to obtain currently playing tune information from WINAMP</translation>
  </message>
 </context>
</TS>
