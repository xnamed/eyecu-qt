<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
 <context>
  <name>MapSourceWiki</name>
  <message>
   <location line="13" filename="../../plugins/mapsourcewiki/mapsourcewiki.cpp"/>
   <source>Wikimapia map source</source>
   <translation>Wikimapia map source</translation>
  </message>
  <message>
   <location line="14" filename="../../plugins/mapsourcewiki/mapsourcewiki.cpp"/>
   <source>Allows Map plugin to use Wikimapia as map source</source>
   <translation>Allows Map plugin to use Wikimapia as map source</translation>
  </message>
 </context>
</TS>
