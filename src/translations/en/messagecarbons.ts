<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
 <context>
  <name>MessageCarbons</name>
  <message>
   <location line="33" filename="../../plugins/messagecarbons/messagecarbons.cpp"/>
   <location line="87" filename="../../plugins/messagecarbons/messagecarbons.cpp"/>
   <source>Message Carbons</source>
   <translation>Message Carbons</translation>
  </message>
  <message>
   <location line="34" filename="../../plugins/messagecarbons/messagecarbons.cpp"/>
   <location line="88" filename="../../plugins/messagecarbons/messagecarbons.cpp"/>
   <source>Allows to keep all user IM clients engaged in a conversation</source>
   <translation>Allows to keep all user IM clients engaged in a conversation</translation>
  </message>
 </context>
</TS>
