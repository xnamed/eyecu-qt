<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
 <context>
  <name>MapSourceOsm</name>
  <message>
   <location line="12" filename="../../plugins/mapsourceosm/mapsourceosm.cpp"/>
   <source>OSM map source</source>
   <translation>OSM map source</translation>
  </message>
  <message>
   <location line="13" filename="../../plugins/mapsourceosm/mapsourceosm.cpp"/>
   <source>Allows Map plugin to use Open Street Maps as map source</source>
   <translation>Allows Map plugin to use Open Street Maps as map source</translation>
  </message>
 </context>
</TS>
