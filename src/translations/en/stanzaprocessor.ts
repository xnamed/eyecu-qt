<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
 <context>
  <name>StanzaProcessor</name>
  <message>
   <location line="22" filename="../../plugins/stanzaprocessor/stanzaprocessor.cpp"/>
   <source>Stanza Manager</source>
   <translation>Stanza Manager</translation>
  </message>
  <message>
   <location line="23" filename="../../plugins/stanzaprocessor/stanzaprocessor.cpp"/>
   <source>Allows other modules to send and receive XMPP stanzas</source>
   <translation>Allows other modules to send and receive XMPP stanzas</translation>
  </message>
 </context>
</TS>
