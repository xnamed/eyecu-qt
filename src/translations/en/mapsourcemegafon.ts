<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
 <context>
  <name>MapSourceMegafon</name>
  <message>
   <location line="11" filename="../../plugins/mapsourcemegafon/mapsourcemegafon.cpp"/>
   <source>MegaFon Navigator map source</source>
   <translation>MegaFon Navigator map source</translation>
  </message>
  <message>
   <location line="12" filename="../../plugins/mapsourcemegafon/mapsourcemegafon.cpp"/>
   <source>Allows Map plugin to use MegaFon Navigator as map source</source>
   <translation>Allows Map plugin to use MegaFon Navigator as map source</translation>
  </message>
 </context>
</TS>
