<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
 <context>
  <name>Abbreviations</name>
  <message>
   <location line="36" filename="../../plugins/abbreviations/abbreviations.cpp"/>
   <source>Abbreviations</source>
   <translation>Abbreviations</translation>
  </message>
  <message>
   <location line="37" filename="../../plugins/abbreviations/abbreviations.cpp"/>
   <source>Translates abbreviations frequently used in network communities</source>
   <translation>Translates abbreviations frequently used in network communities</translation>
  </message>
 </context>
</TS>
