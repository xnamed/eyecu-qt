<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
 <context>
  <name>MapSearchProviderOsm</name>
  <message>
   <location line="14" filename="../../plugins/mapsearchproviderosm/mapsearchproviderosm.cpp"/>
   <source>Map Search Provider OSM</source>
   <translation>Map Search Provider OSM</translation>
  </message>
  <message>
   <location line="15" filename="../../plugins/mapsearchproviderosm/mapsearchproviderosm.cpp"/>
   <source>Allows to use OSM as a map search provider</source>
   <translation>Allows to use OSM as a map search provider</translation>
  </message>
  <message>
   <location line="55" filename="../../plugins/mapsearchproviderosm/mapsearchproviderosm.cpp"/>
   <source>OSM</source>
   <translation>OSM</translation>
  </message>
 </context>
</TS>
