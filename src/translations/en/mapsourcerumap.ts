<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
 <context>
  <name>MapSourceRuMap</name>
  <message>
   <location line="12" filename="../../plugins/mapsourcerumap/mapsourcerumap.cpp"/>
   <source>RuMap map source</source>
   <translation>RuMap map source</translation>
  </message>
  <message>
   <location line="13" filename="../../plugins/mapsourcerumap/mapsourcerumap.cpp"/>
   <source>Allows Map plugin to use RuMap (Geocentre-Consulting) as map source</source>
   <translation>Allows Map plugin to use RuMap (Geocentre-Consulting) as map source</translation>
  </message>
 </context>
</TS>
