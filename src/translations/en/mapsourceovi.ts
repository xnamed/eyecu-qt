<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
 <context>
  <name>MapSourceOvi</name>
  <message>
   <location line="12" filename="../../plugins/mapsourceovi/mapsourceovi.cpp"/>
   <source>HERE map source</source>
   <translation>HERE map source</translation>
  </message>
  <message>
   <location line="13" filename="../../plugins/mapsourceovi/mapsourceovi.cpp"/>
   <source>Allows Map plugin to use Nokia's HERE service as map source</source>
   <translation>Allows Map plugin to use Nokia's HERE service as map source</translation>
  </message>
 </context>
</TS>
