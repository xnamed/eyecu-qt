<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
 <context>
  <name>Receipts</name>
  <message>
   <location line="35" filename="../../plugins/receipts/receipts.cpp"/>
   <location line="158" filename="../../plugins/receipts/receipts.cpp"/>
   <source>Message Delivery Receipts</source>
   <translation>Message Delivery Receipts</translation>
  </message>
  <message>
   <location line="36" filename="../../plugins/receipts/receipts.cpp"/>
   <source>Sends, receives and displays message delivery receipts</source>
   <translation>Sends, receives and displays message delivery receipts</translation>
  </message>
  <message>
   <location line="97" filename="../../plugins/receipts/receipts.cpp"/>
   <source>Show delivery notifications</source>
   <translation>Show delivery notifications</translation>
  </message>
  <message>
   <location line="98" filename="../../plugins/receipts/receipts.cpp"/>
   <source>Send delivery notifications</source>
   <translation>Send delivery notifications</translation>
  </message>
  <message>
   <location line="126" filename="../../plugins/receipts/receipts.cpp"/>
   <source>When message delivery notification recieved</source>
   <translation>When message delivery notification recieved</translation>
  </message>
  <message>
   <location line="159" filename="../../plugins/receipts/receipts.cpp"/>
   <source>Sends/receives Message Delivery Receipts</source>
   <translation>Sends/receives Message Delivery Receipts</translation>
  </message>
  <message>
   <location line="326" filename="../../plugins/receipts/receipts.cpp"/>
   <source>Message delivered</source>
   <translation>Message delivered</translation>
  </message>
 </context>
</TS>
