<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>AboutBox</name>
    <message>
        <location filename="../../loader/aboutbox.cpp" line="18"/>
        <source>Version: %1.%2 %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../loader/aboutbox.cpp" line="19"/>
        <source>Revision: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AboutBoxClass</name>
    <message>
        <location filename="../../loader/aboutbox.ui" line="26"/>
        <source>About the program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../loader/aboutbox.ui" line="188"/>
        <source>© 2010-2020 Konstantin Kozlov, Vyatcheslav Tselykh, Sergey Potapov. This software is released under the terms of the GNU General Public License version 3.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PasswordDialog</name>
    <message>
        <source>Enter password:</source>
        <translation type="vanished">Enter password:</translation>
    </message>
    <message>
        <source>Save password</source>
        <translation type="vanished">Save password</translation>
    </message>
</context>
<context>
    <name>PluginManager</name>
    <message>
        <location filename="../../loader/pluginmanager.cpp" line="365"/>
        <source>Saving settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../loader/pluginmanager.cpp" line="408"/>
        <source>Loading plugin: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../loader/pluginmanager.cpp" line="449"/>
        <source>Duplicate plugin uuid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../loader/pluginmanager.cpp" line="456"/>
        <source>Wrong plugin interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../loader/pluginmanager.cpp" line="476"/>
        <source>Dependencies not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../loader/pluginmanager.cpp" line="483"/>
        <source>Conflict with plugin %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../loader/pluginmanager.cpp" line="514"/>
        <source>Initializing plugin connections: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../loader/pluginmanager.cpp" line="526"/>
        <source>Initialization failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../loader/pluginmanager.cpp" line="537"/>
        <source>Initializing plugin objects: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../loader/pluginmanager.cpp" line="547"/>
        <source>Initializing plugin settings: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../loader/pluginmanager.cpp" line="564"/>
        <source>Starting plugin: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../loader/pluginmanager.cpp" line="660"/>
        <source>Application started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../loader/pluginmanager.cpp" line="950"/>
        <source>About Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../loader/pluginmanager.cpp" line="956"/>
        <source>About the program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../loader/pluginmanager.cpp" line="962"/>
        <source>Setup plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../loader/pluginmanager.cpp" line="971"/>
        <source>Global shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../loader/pluginmanager.cpp" line="972"/>
        <source>Application shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchLineEdit</name>
    <message>
        <source>Search options</source>
        <translation type="vanished">Search options</translation>
    </message>
    <message>
        <source>Clear text</source>
        <translation type="vanished">Clear text</translation>
    </message>
</context>
<context>
    <name>SetupPluginsDialog</name>
    <message>
        <location filename="../../loader/setuppluginsdialog.cpp" line="204"/>
        <source>Disabled (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../loader/setuppluginsdialog.cpp" line="207"/>
        <source>With errors (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../../loader/setuppluginsdialog.cpp" line="242"/>
        <source>This plugin depends on %n other plugin(s).</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../loader/setuppluginsdialog.cpp" line="244"/>
        <source>This plugin does not depend on other plugins.</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../../loader/setuppluginsdialog.cpp" line="249"/>
        <source>Other %n plugin(s) depend on this plugin.</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../loader/setuppluginsdialog.cpp" line="249"/>
        <source>Another plugin depends on this plugin.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../loader/setuppluginsdialog.cpp" line="252"/>
        <source>Other plugins don&apos;t depend on this plugin.</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../../loader/setuppluginsdialog.cpp" line="271"/>
        <source>%n dependency(ies) not found.</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../loader/setuppluginsdialog.cpp" line="285"/>
        <source>Restart Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../loader/setuppluginsdialog.cpp" line="285"/>
        <source>Settings saved. Do you want to restart application?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SetupPluginsDialogClass</name>
    <message>
        <location filename="../../loader/setuppluginsdialog.ui" line="14"/>
        <source>Setup Plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../loader/setuppluginsdialog.ui" line="76"/>
        <source>Plugin Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../loader/setuppluginsdialog.cpp" line="89"/>
        <source>Search plugin</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>XmppSaslError</name>
    <message>
        <source>Authorization aborted</source>
        <translation type="vanished">Authorization aborted</translation>
    </message>
    <message>
        <source>Account disabled</source>
        <translation type="vanished">Account disabled</translation>
    </message>
    <message>
        <source>Credentials expired</source>
        <translation type="vanished">Credentials expired</translation>
    </message>
    <message>
        <source>Encryption required</source>
        <translation type="vanished">Encryption required</translation>
    </message>
    <message>
        <source>Incorrect encoding</source>
        <translation type="vanished">Incorrect encoding</translation>
    </message>
    <message>
        <source>Invalid authorization id</source>
        <translation type="vanished">Invalid authorization id</translation>
    </message>
    <message>
        <source>Invalid mechanism</source>
        <translation type="vanished">Invalid mechanism</translation>
    </message>
    <message>
        <source>Malformed request</source>
        <translation type="vanished">Malformed request</translation>
    </message>
    <message>
        <source>Mechanism is too weak</source>
        <translation type="vanished">Mechanism is too weak</translation>
    </message>
    <message>
        <source>Not authorized</source>
        <translation type="vanished">Not authorized</translation>
    </message>
    <message>
        <source>Temporary authentication failure</source>
        <translation type="vanished">Temporary authentication failure</translation>
    </message>
</context>
<context>
    <name>XmppStreamError</name>
    <message>
        <source>Undefined error condition</source>
        <translation type="vanished">Undefined error condition</translation>
    </message>
    <message>
        <source>Bad request format</source>
        <translation type="vanished">Bad request format</translation>
    </message>
    <message>
        <source>Bad namespace prefix</source>
        <translation type="vanished">Bad namespace prefix</translation>
    </message>
    <message>
        <source>Conflict</source>
        <translation type="vanished">Conflict</translation>
    </message>
    <message>
        <source>Connection timeout</source>
        <translation type="vanished">Connection timeout</translation>
    </message>
    <message>
        <source>Host is not serviced</source>
        <translation type="vanished">Host is not serviced</translation>
    </message>
    <message>
        <source>Unknown host</source>
        <translation type="vanished">Unknown host</translation>
    </message>
    <message>
        <source>Improper addressing</source>
        <translation type="vanished">Improper addressing</translation>
    </message>
    <message>
        <source>Internal server error</source>
        <translation type="vanished">Internal server error</translation>
    </message>
    <message>
        <source>Invalid from address</source>
        <translation type="vanished">Invalid from address</translation>
    </message>
    <message>
        <source>Invalid namespace</source>
        <translation type="vanished">Invalid namespace</translation>
    </message>
    <message>
        <source>Invalid XML</source>
        <translation type="vanished">Invalid XML</translation>
    </message>
    <message>
        <source>Not authorized</source>
        <translation type="vanished">Not authorized</translation>
    </message>
    <message>
        <source>XML not well formed</source>
        <translation type="vanished">XML not well formed</translation>
    </message>
    <message>
        <source>Policy violation</source>
        <translation type="vanished">Policy violation</translation>
    </message>
    <message>
        <source>Remote connection failed</source>
        <translation type="vanished">Remote connection failed</translation>
    </message>
    <message>
        <source>Stream need to be reseted</source>
        <translation type="vanished">Stream need to be reseted</translation>
    </message>
    <message>
        <source>Resource constraint</source>
        <translation type="vanished">Resource constraint</translation>
    </message>
    <message>
        <source>Restricted XML</source>
        <translation type="vanished">Restricted XML</translation>
    </message>
    <message>
        <source>See other host</source>
        <translation type="vanished">See other host</translation>
    </message>
    <message>
        <source>System shutdown</source>
        <translation type="vanished">System shutdown</translation>
    </message>
    <message>
        <source>Unsupported encoding</source>
        <translation type="vanished">Unsupported encoding</translation>
    </message>
    <message>
        <source>Unsupported feature</source>
        <translation type="vanished">Unsupported feature</translation>
    </message>
    <message>
        <source>Unsupported stanza type</source>
        <translation type="vanished">Unsupported stanza type</translation>
    </message>
    <message>
        <source>Unsupported version</source>
        <translation type="vanished">Unsupported version</translation>
    </message>
    <message>
        <source>Feature not implemented</source>
        <translation type="vanished">Feature not implemented</translation>
    </message>
    <message>
        <source>Insufficient permissions</source>
        <translation type="vanished">Insufficient permissions</translation>
    </message>
    <message>
        <source>Recipient changed address</source>
        <translation type="vanished">Recipient changed address</translation>
    </message>
    <message>
        <source>Requested item not found</source>
        <translation type="vanished">Requested item not found</translation>
    </message>
    <message>
        <source>Malformed XMPP address</source>
        <translation type="vanished">Malformed XMPP address</translation>
    </message>
    <message>
        <source>Not accepted by the recipient</source>
        <translation type="vanished">Not accepted by the recipient</translation>
    </message>
    <message>
        <source>Not allowed by the recipient</source>
        <translation type="vanished">Not allowed by the recipient</translation>
    </message>
    <message>
        <source>Recipient unavailable</source>
        <translation type="vanished">Recipient unavailable</translation>
    </message>
    <message>
        <source>Redirect to another address</source>
        <translation type="vanished">Redirect to another address</translation>
    </message>
    <message>
        <source>Registration required</source>
        <translation type="vanished">Registration required</translation>
    </message>
    <message>
        <source>Remote server not found</source>
        <translation type="vanished">Remote server not found</translation>
    </message>
    <message>
        <source>Remote server timeout</source>
        <translation type="vanished">Remote server timeout</translation>
    </message>
    <message>
        <source>Service unavailable</source>
        <translation type="vanished">Service unavailable</translation>
    </message>
    <message>
        <source>Subscription required</source>
        <translation type="vanished">Subscription required</translation>
    </message>
    <message>
        <source>Unexpected request</source>
        <translation type="vanished">Unexpected request</translation>
    </message>
    <message>
        <source>Resource limit exceeded</source>
        <translation type="vanished">Resource limit exceeded</translation>
    </message>
    <message>
        <source>Stanza is too big</source>
        <translation type="vanished">Stanza is too big</translation>
    </message>
    <message>
        <source>Too many stanzas</source>
        <translation type="vanished">Too many stanzas</translation>
    </message>
</context>
</TS>
