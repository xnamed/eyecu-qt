<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
 <context>
  <name>PrivateStorage</name>
  <message>
   <location line="30" filename="../../plugins/privatestorage/privatestorage.cpp"/>
   <source>Private Storage</source>
   <translation>Private Storage</translation>
  </message>
  <message>
   <location line="31" filename="../../plugins/privatestorage/privatestorage.cpp"/>
   <source>Allows other modules to store arbitrary data on a server</source>
   <translation>Allows other modules to store arbitrary data on a server</translation>
  </message>
 </context>
</TS>
