<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
 <context>
  <name>RosterManager</name>
  <message>
   <location line="24" filename="../../plugins/roster/rostermanager.cpp"/>
   <source>Roster Manager</source>
   <translation>Roster Manager</translation>
  </message>
  <message>
   <location line="25" filename="../../plugins/roster/rostermanager.cpp"/>
   <source>Allows other modules to get information about contacts in the roster</source>
   <translation>Allows other modules to get information about contacts in the roster</translation>
  </message>
  <message>
   <location line="59" filename="../../plugins/roster/rostermanager.cpp"/>
   <source>Roster request failed</source>
   <translation>Roster request failed</translation>
  </message>
 </context>
</TS>
