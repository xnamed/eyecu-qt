<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
 <context>
  <name>Presence</name>
  <message>
   <location line="425" filename="../../plugins/presence/presence.cpp"/>
   <source>XMPP stream closed unexpectedly</source>
   <translation>XMPP stream closed unexpectedly</translation>
  </message>
 </context>
 <context>
  <name>PresenceManager</name>
  <message>
   <location line="36" filename="../../plugins/presence/presencemanager.cpp"/>
   <source>Presence Manager</source>
   <translation>Presence Manager</translation>
  </message>
  <message>
   <location line="37" filename="../../plugins/presence/presencemanager.cpp"/>
   <source>Allows other modules to obtain information about the status of contacts in the roster</source>
   <translation>Allows other modules to obtain information about the status of contacts in the roster</translation>
  </message>
 </context>
</TS>
