<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
 <context>
  <name>Jingle</name>
  <message>
   <location line="34" filename="../../plugins/jingle/jingle.cpp"/>
   <location line="134" filename="../../plugins/jingle/jingle.cpp"/>
   <source>Jingle</source>
   <translation>Jingle</translation>
  </message>
  <message>
   <location line="35" filename="../../plugins/jingle/jingle.cpp"/>
   <source>Implements XEP-0166: Jingle</source>
   <translation>Implements XEP-0166: Jingle</translation>
  </message>
  <message>
   <location line="135" filename="../../plugins/jingle/jingle.cpp"/>
   <source>Supports XEP-0166: Jingle</source>
   <translation>Supports XEP-0166: Jingle</translation>
  </message>
  <message>
   <location line="477" filename="../../plugins/jingle/jingle.cpp"/>
   <source>Success</source>
   <translation>Success</translation>
  </message>
  <message>
   <location line="479" filename="../../plugins/jingle/jingle.cpp"/>
   <source>Busy</source>
   <translation>Busy</translation>
  </message>
  <message>
   <location line="481" filename="../../plugins/jingle/jingle.cpp"/>
   <source>Cancelled</source>
   <translation>Cancelled</translation>
  </message>
  <message>
   <location line="483" filename="../../plugins/jingle/jingle.cpp"/>
   <source>Alternative session</source>
   <translation>Alternative session</translation>
  </message>
  <message>
   <location line="485" filename="../../plugins/jingle/jingle.cpp"/>
   <source>Connectivity error</source>
   <translation>Connectivity error</translation>
  </message>
  <message>
   <location line="487" filename="../../plugins/jingle/jingle.cpp"/>
   <source>Declined</source>
   <translation>Declined</translation>
  </message>
  <message>
   <location line="489" filename="../../plugins/jingle/jingle.cpp"/>
   <source>Session expired</source>
   <translation>Session expired</translation>
  </message>
  <message>
   <location line="491" filename="../../plugins/jingle/jingle.cpp"/>
   <source>Application failure</source>
   <translation>Application failure</translation>
  </message>
  <message>
   <location line="493" filename="../../plugins/jingle/jingle.cpp"/>
   <source>Transport failure</source>
   <translation>Transport failure</translation>
  </message>
  <message>
   <location line="495" filename="../../plugins/jingle/jingle.cpp"/>
   <source>General error</source>
   <translation>General error</translation>
  </message>
  <message>
   <location line="497" filename="../../plugins/jingle/jingle.cpp"/>
   <source>Session gone</source>
   <translation>Session gone</translation>
  </message>
  <message>
   <location line="499" filename="../../plugins/jingle/jingle.cpp"/>
   <source>Incompatible parameters</source>
   <translation>Incompatible parameters</translation>
  </message>
  <message>
   <location line="501" filename="../../plugins/jingle/jingle.cpp"/>
   <source>Media error</source>
   <translation>Media error</translation>
  </message>
  <message>
   <location line="503" filename="../../plugins/jingle/jingle.cpp"/>
   <source>Security error</source>
   <translation>Security error</translation>
  </message>
  <message>
   <location line="505" filename="../../plugins/jingle/jingle.cpp"/>
   <source>Timeout</source>
   <translation>Timeout</translation>
  </message>
  <message>
   <location line="507" filename="../../plugins/jingle/jingle.cpp"/>
   <source>Application not supported</source>
   <translation>Application not supported</translation>
  </message>
  <message>
   <location line="509" filename="../../plugins/jingle/jingle.cpp"/>
   <source>Transport not supported</source>
   <translation>Transport not supported</translation>
  </message>
 </context>
</TS>
