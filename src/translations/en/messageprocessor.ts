<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
 <context>
  <name>MessageProcessor</name>
  <message>
   <location line="31" filename="../../plugins/messageprocessor/messageprocessor.cpp"/>
   <source>Message Manager</source>
   <translation>Message Manager</translation>
  </message>
  <message>
   <location line="32" filename="../../plugins/messageprocessor/messageprocessor.cpp"/>
   <source>Allows other modules to send and receive messages</source>
   <translation>Allows other modules to send and receive messages</translation>
  </message>
 </context>
</TS>
