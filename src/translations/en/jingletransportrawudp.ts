<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
 <context>
  <name>JingleTransportRawUdp</name>
  <message>
   <location line="16" filename="../../plugins/jingletransportrawudp/jingletransportrawudp.cpp"/>
   <location line="286" filename="../../plugins/jingletransportrawudp/jingletransportrawudp.cpp"/>
   <source>Jingle RAW-UDP Transport</source>
   <translation>Jingle RAW-UDP Transport</translation>
  </message>
  <message>
   <location line="17" filename="../../plugins/jingletransportrawudp/jingletransportrawudp.cpp"/>
   <source>Implements XEP-0177: Jingle RAW-UDP transport method</source>
   <translation>Implements XEP-0177: Jingle RAW-UDP transport method</translation>
  </message>
  <message>
   <location line="287" filename="../../plugins/jingletransportrawudp/jingletransportrawudp.cpp"/>
   <source>Allows using RAW-UDP transport in Jingle sesions</source>
   <translation>Allows using RAW-UDP transport in Jingle sesions</translation>
  </message>
 </context>
</TS>
