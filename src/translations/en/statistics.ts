<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
 <context>
  <name>Statistics</name>
  <message>
   <location line="138" filename="../../plugins/statistics/statistics.cpp"/>
   <source>Statistics</source>
   <translation>Statistics</translation>
  </message>
  <message>
   <location line="139" filename="../../plugins/statistics/statistics.cpp"/>
   <source>Allows to collect application statistics</source>
   <translation>Allows to collect application statistics</translation>
  </message>
  <message>
   <location line="263" filename="../../plugins/statistics/statistics.cpp"/>
   <source>Send anonymous statistics to developer</source>
   <translation>Send anonymous statistics to developer</translation>
  </message>
 </context>
</TS>
