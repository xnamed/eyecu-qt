<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
 <context>
  <name>PositioningMethodIpProviderFreegeoip</name>
  <message>
   <location line="16" filename="../../plugins/positioningmethodipproviderfreegeoip/positioningmethodipproviderfreegeoip.cpp"/>
   <source>Positining Method IP Provider freegeoip.net</source>
   <translation>Positining Method IP Provider freegeoip.net</translation>
  </message>
  <message>
   <location line="17" filename="../../plugins/positioningmethodipproviderfreegeoip/positioningmethodipproviderfreegeoip.cpp"/>
   <source>Allows to use freegeoip.net as an IP positioning provider</source>
   <translation>Allows to use freegeoip.net as an IP positioning provider</translation>
  </message>
  <message>
   <location line="38" filename="../../plugins/positioningmethodipproviderfreegeoip/positioningmethodipproviderfreegeoip.cpp"/>
   <source>freegeoip.net</source>
   <translation>freegeoip.net</translation>
  </message>
 </context>
</TS>
