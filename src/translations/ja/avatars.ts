<?xml version="1.0" ?><!DOCTYPE TS><TS language="ja" version="2.0">
 <context>
  <name>AvatarOptionsWidget</name>
  <message>
   <location filename="../../plugins/avatars/avataroptionswidget.ui" line="63"/>
   <source>Small</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/avatars/avataroptionswidget.ui" line="68"/>
   <source>Normal</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/avatars/avataroptionswidget.ui" line="73"/>
   <source>Large</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/avatars/avataroptionswidget.ui" line="87"/>
   <source>Avatar position</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/avatars/avataroptionswidget.ui" line="97"/>
   <source>Avatar size</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/avatars/avataroptionswidget.ui" line="107"/>
   <source>Display avatars for offline contacts grayscaled</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/avatars/avataroptionswidget.ui" line="49"/>
   <source>At Right</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/avatars/avataroptionswidget.ui" line="54"/>
   <source>At Left</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/avatars/avataroptionswidget.ui" line="35"/>
   <source>Display avatars</source>
   <translation type="unfinished"/>
  </message>
 </context>
 <context>
  <name>AvatarSizeOptionsWidget</name>
  <message>
   <location filename="../../plugins/avatars/avatarsizeoptionswidget.ui" line="35"/>
   <source>Display empty avatars</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/avatars/avatarsizeoptionswidget.ui" line="42"/>
   <source>Avatar size</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/avatars/avatarsizeoptionswidget.ui" line="63"/>
   <location filename="../../plugins/avatars/avatarsizeoptionswidget.ui" line="79"/>
   <location filename="../../plugins/avatars/avatarsizeoptionswidget.ui" line="95"/>
   <source>px</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/avatars/avatarsizeoptionswidget.ui" line="111"/>
   <source>Small</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/avatars/avatarsizeoptionswidget.ui" line="121"/>
   <source>Normal</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/avatars/avatarsizeoptionswidget.ui" line="131"/>
   <source>Large</source>
   <translation type="unfinished"/>
  </message>
 </context>
 <context>
  <name>Avatars</name>
  <message>
   <location filename="../../plugins/avatars/avatars.cpp" line="151"/>
   <location filename="../../plugins/avatars/avatars.cpp" line="484"/>
   <location filename="../../plugins/avatars/avatars.cpp" line="490"/>
   <location filename="../../plugins/avatars/avatars.cpp" line="496"/>
   <source>Avatars</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/avatars/avatars.cpp" line="152"/>
   <source>Allows to set and display avatars</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/avatars/avatars.cpp" line="990"/>
   <source>Avatar</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/avatars/avatars.cpp" line="994"/>
   <source>Set avatar</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/avatars/avatars.cpp" line="1001"/>
   <source>Clear avatar</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/avatars/avatars.cpp" line="1012"/>
   <source>Custom picture</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/avatars/avatars.cpp" line="1016"/>
   <source>Set custom picture</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/avatars/avatars.cpp" line="1023"/>
   <source>Clear custom picture</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/avatars/avatars.cpp" line="1056"/>
   <source>Select avatar image</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/avatars/avatars.cpp" line="1056"/>
   <source>Image Files (*.png *.jpg *.bmp *.gif)</source>
   <translation type="unfinished"/>
  </message>
 </context>
</TS>