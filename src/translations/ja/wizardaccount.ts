<?xml version="1.0" ?><!DOCTYPE TS><TS language="ja" version="2.0">
 <context>
  <name>ConclusionPage</name>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1283"/>
   <source>Welcome to Jabber network!</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1287"/>
   <source>Account name</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1292"/>
   <source>Press &quot;Finish&quot; button to close Wizard.</source>
   <comment>&quot;Finish&quot; should match the text of an appropriate Qt Wizard button</comment>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1296"/>
   <source>Additional account settings...</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1300"/>
   <source>Go online now</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1313"/>
   <source>Done!</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1314"/>
   <source>Connection Wizard cempleted successfuly.</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1316"/>
   <source>Congratulations</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1319"/>
   <source>You successfully connected to Jabber as %1.</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1320"/>
   <source>You successfully registered at Jabber as %1.</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1336"/>
   <source>Failure!</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1337"/>
   <source>Connection Wizard failed to create an account for you</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1338"/>
   <source>Sorry</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1339"/>
   <source>Internal error occured!</source>
   <translation type="unfinished"/>
  </message>
 </context>
 <context>
  <name>ConnectPage</name>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="747"/>
   <source>Connect to Jabber server</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="748"/>
   <source>Trying to logon or register at Jabber server</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="817"/>
   <source>Requesting registration form...</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="817"/>
   <source>Connecting...</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="841"/>
   <source>Failed to check connection :(</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="842"/>
   <source>Internal Error</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="843"/>
   <source>Click &apos;Back&apos; button to change the account credentials or the &apos;Finish&apos; button to add the account as is.</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="910"/>
   <source>Connection failed!</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="911"/>
   <source>Error</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="913"/>
   <source>Please go back and check your credentials.</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="915"/>
   <source>Please go back and check your connection and server name.</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="917"/>
   <source>Please make sure the server supports in-band registration.</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="967"/>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1008"/>
   <source>Password</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="970"/>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1011"/>
   <source>Retype password</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="978"/>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1001"/>
   <source>e-mail</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="997"/>
   <source>User name</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="999"/>
   <source>Enter the text you see</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1038"/>
   <source>Failed to register :(</source>
   <translation type="unfinished"/>
  </message>
 </context>
 <context>
  <name>ConnectionPage</name>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="719"/>
   <source>Connection settings</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="721"/>
   <source>Check your connection settings.</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="722"/>
   <source>If you&apos;re not certain about it, please, contact your system administartor.</source>
   <translation type="unfinished"/>
  </message>
 </context>
 <context>
  <name>ConnectionWizard</name>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="85"/>
   <source>Connection Wizard</source>
   <translation type="unfinished"/>
  </message>
 </context>
 <context>
  <name>CredentialsPage</name>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="585"/>
   <source>Credentials</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="586"/>
   <source>Please, enter your user name, password and resource</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="588"/>
   <source>User name and resource</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="601"/>
   <source>Password</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <source>Re-type password</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="662"/>
   <source>Account exists</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="662"/>
   <source>Account with specified Server and User Name exists already! Please choose different Server or User Name.</source>
   <translation type="unfinished"/>
  </message>
 </context>
 <context>
  <name>IntroPage</name>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="127"/>
   <source>Connect to Jabber</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="128"/>
   <source>This wizard will help you to create a Jabber account</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="133"/>
   <source>Are you already registered at Jabber network?</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="135"/>
   <source>&amp;Yes</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="135"/>
   <source>I have an account on a Jabber server</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="136"/>
   <source>&amp;No</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="136"/>
   <source>I want to register on a Jabber server</source>
   <translation type="unfinished"/>
  </message>
 </context>
 <context>
  <name>NetworkPage</name>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="161"/>
   <source>Other XMPP</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="161"/>
   <source>An independent XMPP server (Jabber)</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="162"/>
   <source>Google Talk</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="162"/>
   <source>A social network and chat service from Google</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <source>Yandex Online</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <source>A popular Russian portal (internet serach, e-mail, news, chat and so on)</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="164"/>
   <source>Odnoklassniki</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="164"/>
   <source>A popular Russian social network, owned by Mail.Ru Group</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="166"/>
   <source>LiveJournal</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="166"/>
   <source>A popular blogging service</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="168"/>
   <source>QIP</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="168"/>
   <source>A popular Russian portal (internet serach, e-mail, news, chat and so on), owned by OOO &quot;Media Mir&quot;, mostly known by its multiprotocol IM client</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="172"/>
   <source>Network selection</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="173"/>
   <source>Please, select a network, you have registered in</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="175"/>
   <source>There are some social networks, portals or blogging services, which have their own XMPP servers. If you have an account in some of those networks, you may use it in eyeCU.
Please, note, that those XMPP servers usually have implemented their own specific features, which cannot be used with standard XMPP clients, like eyeCU. At the same time, they implenment only limited set of standard XMPP extensions, which could be used with standard clients.So, it's hardly recommended to use such accounts only as additional account, to have convenient access to your social networks and services. To have full-featured XMPP experienece, it's recommended to have an account on independent XMPP (Jabber) server. To connect to such server, please, select &quot;Other XMPP&quot;.
If you don&apos;t have a Jabber account yet, please, go Back and select &quot;No&quot; to register in Jabber.</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="197"/>
   <source>Description</source>
   <translation type="unfinished"/>
  </message>
 </context>
 <context>
  <name>RegisterSubmitPage</name>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1093"/>
   <source>Connect to Jabber server</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1094"/>
   <source>Trying to logon or register at Jabber server</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1132"/>
   <source>Registering...</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1144"/>
   <source>Failed to register :(</source>
   <translation type="unfinished"/>
  </message>
 </context>
 <context>
  <name>ServerPage</name>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="245"/>
   <source>Server selection</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="246"/>
   <source>Specify a server you want to use.</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="248"/>
   <source>Please select a server from the list</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="257"/>
   <source>Or enter manually</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="316"/>
   <source>Server</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="316"/>
   <source>Registration</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="316"/>
   <source>PEP</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="316"/>
   <source>Message Archive</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="317"/>
   <source>Message Carbons</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="317"/>
   <source>User Search</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="317"/>
   <source>MUC</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="317"/>
   <source>Proxy</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="318"/>
   <source>File Store</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="318"/>
   <source>Transports</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="318"/>
   <source>Country</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="382"/>
   <source>In-band registration available</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="486"/>
   <source>Warning!</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="487"/>
   <source>The Server you selected do not support Personal Events (PEP)!
Some %1 features (like User Location, User Activity, User Mood and User Tune) will not work!
Using this server for your primary account is deprecated.
Press &quot;Ok&quot; to proceed or &quot;Cancel&quot; to select another server.</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="499"/>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="514"/>
   <source>Attention!</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="500"/>
   <source>The Server you selected belongs to %1 network!
It is not recommended to use such servers as your primary account, due to restricted functionality they have. In-band registration is also unavailable.
Please, choose another server. If you have an account in such network, please, go back and select &quot;Yes&quot; on the first page. If you don&apos;t have an account, but want to have it, you&apos;ll need to visit %2 to register first.</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="510"/>
   <source>%1 website</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="514"/>
   <source>The Server you selected do not support in-band registration!
You'll have to register via web!
Press &quot;Ok&quot; to proceed or &quot;Cancel&quot; to select another server.</source>
   <translation type="unfinished"/>
  </message>
 </context>
 <context>
  <name>WebRegistrationInfo</name>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1174"/>
   <source>Web registration</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1177"/>
   <source>The server you selected doesn&apos;t support in-band registration.</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1178"/>
   <source>Follow instructions below to register via web.</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1202"/>
   <source>Open registration website</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1214"/>
   <source>How to register at %1</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1250"/>
   <source>Warning!</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1251"/>
   <source>Cannot open instructions.</source>
   <translation type="unfinished"/>
  </message>
 </context>
 <context>
  <name>WizardAccount</name>
  <message>
   <location filename="../../plugins/wizardaccount/wizardaccount.cpp" line="14"/>
   <source>Connection Wizard</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardaccount.cpp" line="15"/>
   <source>A Wizard, which helps unexperienced user to connect to Jabber network</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardaccount.cpp" line="55"/>
   <source>Create an account?</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardaccount.cpp" line="56"/>
   <source>It seems, you don&apos;t have a Jabber account yet. Do you want to start a Wizard, which will help you to connect to Jabber?</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardaccount.cpp" line="60"/>
   <source>You may start Connection Wizard anytime, by pressing &quot;%1&quot; link on &quot;Accounts&quot; page in the Options dialog.</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardaccount.cpp" line="60"/>
   <source>Add account</source>
   <translation type="unfinished"/>
  </message>
 </context>
</TS>