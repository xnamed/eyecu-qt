<?xml version="1.0" ?><!DOCTYPE TS><TS language="ja" version="2.0">
 <context>
  <name>Jingle</name>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="34"/>
   <location filename="../../plugins/jingle/jingle.cpp" line="134"/>
   <source>Jingle</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="35"/>
   <source>Implements XEP-0166: Jingle</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="135"/>
   <source>Supports XEP-0166: Jingle</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="477"/>
   <source>Success</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="479"/>
   <source>Busy</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="481"/>
   <source>Cancelled</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="483"/>
   <source>Alternative session</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="485"/>
   <source>Connectivity error</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="487"/>
   <source>Declined</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="489"/>
   <source>Session expired</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="491"/>
   <source>Application failure</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="493"/>
   <source>Transport failure</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="495"/>
   <source>General error</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="497"/>
   <source>Session gone</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="499"/>
   <source>Incompatible parameters</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="501"/>
   <source>Media error</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="503"/>
   <source>Security error</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="505"/>
   <source>Timeout</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="507"/>
   <source>Application not supported</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/jingle/jingle.cpp" line="509"/>
   <source>Transport not supported</source>
   <translation type="unfinished"/>
  </message>
 </context>
</TS>