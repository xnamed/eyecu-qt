<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" version="2.0">
 <context>
  <name>Emoji</name>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="51"/>
   <source>Emoji</source>
   <translation>Emoji</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="52"/>
   <source>Allows to use emoji in messages</source>
   <translation>Toestaan om emoji in berichten te gebruiken</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="87"/>
   <source>Smileys &amp; People</source>
   <translation>Smileys &amp; Mensen</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="88"/>
   <source>Animals &amp; Nature</source>
   <translation>Dieren &amp; Natuur</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="89"/>
   <source>Food &amp; Drink</source>
   <translation>Eten &amp; Drinken</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="90"/>
   <source>Travel &amp; Places</source>
   <translation>Reizen &amp; Plaatsen</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="91"/>
   <source>Symbols</source>
   <translation>Symbolen</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="92"/>
   <source>Activities</source>
   <translation>Activiteiten</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="94"/>
   <source>Flags</source>
   <translation>Vlaggen</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="93"/>
   <source>Objects</source>
   <translation>Objecten</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/emoji.cpp" line="166"/>
   <source>Message windows</source>
   <translation>Berichten venster</translation>
  </message>
 </context>
 <context>
  <name>EmojiOptions</name>
  <message>
   <location filename="../../plugins/emoji/emojioptions.cpp" line="14"/>
   <source>Do not use emoji</source>
   <translation>Gebruik geen emoji</translation>
  </message>
 </context>
 <context>
  <name>EmojiOptionsClass</name>
  <message>
   <location filename="../../plugins/emoji/emojioptions.ui" line="29"/>
   <source>Emoji set:</source>
   <translation>Emoji set:</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/emojioptions.ui" line="39"/>
   <source>Menu icon size:</source>
   <translation>Menu icon grootte:</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/emojioptions.ui" line="49"/>
   <source>Chat icon size:</source>
   <translation>Chat icon grootte:</translation>
  </message>
 </context>
 <context>
  <name>IconSizeSpinBox</name>
  <message numerus="yes">
   <location filename="../../plugins/emoji/iconsizespinbox.cpp" line="62"/>
   <source>pixel(s)</source>
   <translation><numerusform>pixel</numerusform><numerusform>pixels</numerusform></translation>
  </message>
 </context>
 <context>
  <name>SelectIconMenu</name>
  <message>
   <location filename="../../plugins/emoji/selecticonmenu.cpp" line="129"/>
   <source>Fitzpatrick type %1</source>
   <comment>https://en.wikipedia.org/wiki/Fitzpatrick_scale</comment>
   <translation>Fitzpatrick type %1</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/selecticonmenu.cpp" line="143"/>
   <source>Skin color</source>
   <translation>Skin kleur</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/selecticonmenu.cpp" line="112"/>
   <source>Default</source>
   <translation>Standaard</translation>
  </message>
  <message>
   <location filename="../../plugins/emoji/selecticonmenu.cpp" line="129"/>
   <source>1 or 2</source>
   <translation>1 of 2</translation>
  </message>
 </context>
</TS>