<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" version="2.0">
 <context>
  <name>MapSearchProviderHere</name>
  <message>
   <location filename="../../plugins/mapsearchproviderhere/mapsearchproviderhere.cpp" line="15"/>
   <source>Map Search Provider Here</source>
   <translation>Zoek kaart provider hier</translation>
  </message>
  <message>
   <location filename="../../plugins/mapsearchproviderhere/mapsearchproviderhere.cpp" line="16"/>
   <source>Allows to use Nokia&apos;s Here service as a map search provider</source>
   <translation>Toestaan om Nokia&apos;s service als kaart zoek provider te gebruiken</translation>
  </message>
  <message>
   <location filename="../../plugins/mapsearchproviderhere/mapsearchproviderhere.cpp" line="55"/>
   <source>Here</source>
   <translation>Hier</translation>
  </message>
 </context>
</TS>