<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" version="2.0">
 <context>
  <name>Activity</name>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="82"/>
   <source>Doing chores</source>
   <translation>Klusjes aan het doen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="82"/>
   <source>Drinking</source>
   <translation>Drinken</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="82"/>
   <source>Eating</source>
   <translation>Eten</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="82"/>
   <source>Exercising</source>
   <translation>Oefenen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="83"/>
   <source>Grooming</source>
   <translation>Verzorgen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="83"/>
   <source>Having appointment</source>
   <translation>Een afspraak hebben</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="83"/>
   <source>Inactive</source>
   <translation>Inactief</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="83"/>
   <source>Relaxing</source>
   <translation>Relaxen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="84"/>
   <source>Talking</source>
   <translation>Praten</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="84"/>
   <source>Traveling</source>
   <translation>Reizen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="84"/>
   <source>Working</source>
   <translation>Werken</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="84"/>
   <source>Buying groceries</source>
   <translation>Boodschappen doen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="85"/>
   <source>Cleaning</source>
   <translation>Opruimen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="85"/>
   <source>Cooking</source>
   <translation>Koken</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="85"/>
   <source>Doing maintenance</source>
   <translation>Onderhoudswerkzaamheden</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="85"/>
   <source>Writing</source>
   <translation>Schrijven</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="86"/>
   <source>Doing the dishes</source>
   <translation>De afwas doen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="86"/>
   <source>Doing the laundry</source>
   <translation>De was doen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="86"/>
   <source>Gardening</source>
   <translation>Tuinieren</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="86"/>
   <source>Running an errand</source>
   <translation>Een boodschap doen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="87"/>
   <source>Walking the dog</source>
   <translation>Hond uitlaten</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="87"/>
   <source>Having a beer</source>
   <translation>Biertje drinken</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="87"/>
   <source>Having coffee</source>
   <translation>Koffie drinken</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="87"/>
   <source>Having tea</source>
   <translation>Thee drinken</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="88"/>
   <source>Having a snack</source>
   <translation>Snack eten</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="88"/>
   <source>Having breakfast</source>
   <translation>Aan het ontbijten</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="88"/>
   <source>Having dinner</source>
   <translation>Aan het dineren</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="88"/>
   <source>Having lunch</source>
   <translation>Aan het lunchen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="89"/>
   <source>Cycling</source>
   <translation>Fietsen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="89"/>
   <source>Dancing</source>
   <translation>Dansen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="89"/>
   <source>Hiking</source>
   <translation>Wandelen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="89"/>
   <source>Jogging</source>
   <translation>Aan het joggen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="90"/>
   <source>Playing sports</source>
   <translation>Aan het sporten</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="90"/>
   <source>Running</source>
   <translation>Hardlopen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="90"/>
   <source>Skiing</source>
   <translation>Skieen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="90"/>
   <source>Swimming</source>
   <translation>Zwemmen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="91"/>
   <source>Working out</source>
   <translation>Aan het uitwerken</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="91"/>
   <source>At the spa</source>
   <translation>In het kuuroord</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="91"/>
   <source>Brushing teeth</source>
   <translation>Tandenpoetsen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="91"/>
   <source>Getting a haircut</source>
   <translation>Naar de kapper</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="92"/>
   <source>Shaving</source>
   <translation>Scheren</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="92"/>
   <source>Taking a bath</source>
   <translation>Een bad nemen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="92"/>
   <source>Taking a shower</source>
   <translation>Een douche nemen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="92"/>
   <source>Day off</source>
   <translation>Een vrije dag</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="93"/>
   <source>Hanging out</source>
   <translation>Ergens uithangen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="93"/>
   <source>Hiding</source>
   <translation>Verbergen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="93"/>
   <source>On vacation</source>
   <translation>Op vakantie</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="93"/>
   <source>Praying</source>
   <translation>Bidden</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="94"/>
   <source>Scheduled holiday</source>
   <translation>Geplande vakantie</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="94"/>
   <source>Sleeping</source>
   <translation>Slapen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="94"/>
   <source>Thinking</source>
   <translation>Slapen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="94"/>
   <source>Fishing</source>
   <translation>Vissen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="95"/>
   <source>Gaming</source>
   <translation>Gamen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="95"/>
   <source>Going out</source>
   <translation>Uitgaan</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="95"/>
   <source>Partying</source>
   <translation>Feesten</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="95"/>
   <source>Reading</source>
   <translation>Lezen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="96"/>
   <source>Rehearsing</source>
   <translation>Repeteren</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="96"/>
   <source>Shopping</source>
   <translation>Winkelen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="96"/>
   <source>Smoking</source>
   <translation>Roken</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="96"/>
   <source>Socializing</source>
   <translation>Gezelligheid</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="97"/>
   <source>Sunbathing</source>
   <translation>Zonnebaden</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="97"/>
   <source>Watching TV</source>
   <translation>Tv kijken</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="97"/>
   <source>Watching a movie</source>
   <translation>Een film kijken</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="97"/>
   <source>In real life</source>
   <translation>Het echte leven</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="98"/>
   <source>On the phone</source>
   <translation>Aan de telefoon</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="98"/>
   <source>On video phone</source>
   <translation>Video telefoon</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="98"/>
   <source>Commuting</source>
   <translation>woon-werkverkeer</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="98"/>
   <source>Travelling by bicycle</source>
   <translation>Reizen per fiets</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="99"/>
   <source>Driving</source>
   <translation>Auto rijden</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="99"/>
   <source>In a car</source>
   <translation>In een auto</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="99"/>
   <source>On a bus</source>
   <translation>In een bus</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="99"/>
   <source>On a plane</source>
   <translation>In een vliegtuig</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="100"/>
   <source>On a train</source>
   <translation>In de trein</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="100"/>
   <source>On a trip</source>
   <translation>Op reis</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="100"/>
   <source>Walking</source>
   <translation>Wandelen</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="100"/>
   <source>Coding</source>
   <translation>Compileren</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="101"/>
   <source>In a meeting</source>
   <translation>In een vergadering</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="101"/>
   <source>Studying</source>
   <translation>Studeren</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="101"/>
   <source>No activity</source>
   <translation>Geen activiteit</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="110"/>
   <location filename="../../plugins/activity/activity.cpp" line="399"/>
   <source>User Activity</source>
   <translation>Gebruikers activiteit</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="111"/>
   <source>Implements XEP-0108: User Activity</source>
   <translation>Uitvoor: XEP-0108:Gebruikers activiteit</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="213"/>
   <location filename="../../plugins/activity/activity.cpp" line="214"/>
   <source>Set activity</source>
   <translation>Stel activiteit in</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="213"/>
   <source>Ctrl+F6</source>
   <comment>Set activity (for all accounts)</comment>
   <translation>Ctrl+F6</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="214"/>
   <source>F6</source>
   <comment>Set activity (for an account)</comment>
   <translation>F6</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="250"/>
   <source>When user activity changed</source>
   <translation>Wanneer gebruikers activiteit veranderd</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="257"/>
   <location filename="../../plugins/activity/activity.cpp" line="549"/>
   <location filename="../../plugins/activity/activity.cpp" line="839"/>
   <source>Activity</source>
   <translation>Activiteit</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="400"/>
   <source>Supports publishing of current user activity</source>
   <translation>Ondersteunt het publiceren van de huidige activiteit van de gebruiker</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="404"/>
   <source>User Activity Notification</source>
   <translation>Gebruikers Activiteit melding</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="405"/>
   <source>Receives notification of current user activity</source>
   <translation>Ontvangt een melding van huidige gebruikers activiteit</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="502"/>
   <source>Show user activity icons</source>
   <translation>Toon activiteit pictogram van gebruiker</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="506"/>
   <source>Display user activity icon</source>
   <translation>Toon activiteit pictogram van gebruiker</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="507"/>
   <source>Activity events in chat</source>
   <translation>Activiteits gebeurtenissen in de chat</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="576"/>
   <source>%1: %2</source>
   <translation>%1: %2</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="717"/>
   <source>%1 changed activity to %2</source>
   <translation>%1 veranderd activiteit naar %2</translation>
  </message>
  <message>
   <location filename="../../plugins/activity/activity.cpp" line="876"/>
   <source>Changed activity to: %1</source>
   <translation>Veranderd activiteit tot: %1</translation>
  </message>
 </context>
 <context>
  <name>ActivitySelect</name>
  <message>
   <location filename="../../plugins/activity/activityselect.ui" line="26"/>
   <source>Activity select</source>
   <translation>Selecteer je activiteit</translation>
  </message>
 </context>
</TS>