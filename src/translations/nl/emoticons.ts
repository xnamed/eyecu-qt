<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" version="2.0">
 <context>
  <name>Emoticons</name>
  <message>
   <location filename="../../plugins/emoticons/emoticons.cpp" line="48"/>
   <source>Emoticons</source>
   <translation>Smilies</translation>
  </message>
  <message>
   <location filename="../../plugins/emoticons/emoticons.cpp" line="49"/>
   <source>Allows to use your smiley images in messages</source>
   <translation>Toestaan om Smiley afbeeldingen te gebruiken in berichten</translation>
  </message>
  <message>
   <location filename="../../plugins/emoticons/emoticons.cpp" line="136"/>
   <source>Message windows</source>
   <translation>Berichtenvenster</translation>
  </message>
  <message>
   <location filename="../../plugins/emoticons/emoticons.cpp" line="137"/>
   <source>Insert smiley image into message editor</source>
   <translation>Smiley afbeeldingen laden in berichten bewerker</translation>
  </message>
  <message>
   <location filename="../../plugins/emoticons/emoticons.cpp" line="148"/>
   <source>Do not convert text smileys to images</source>
   <translation>Tekst smileys niet omzetten naar afbeeldingen</translation>
  </message>
  <message>
   <location filename="../../plugins/emoticons/emoticons.cpp" line="163"/>
   <source>Smileys:</source>
   <translation>Smileys:</translation>
  </message>
 </context>
 <context>
  <name>EmoticonsOptionsClass</name>
  <message>
   <location filename="../../plugins/emoticons/emoticonsoptions.ui" line="29"/>
   <source>Emoticon sets:</source>
   <translation>Groepen Smileys:</translation>
  </message>
  <message>
   <location filename="../../plugins/emoticons/emoticonsoptions.ui" line="36"/>
   <source>Here you can select emoticon sets and mark those of them to be used</source>
   <translation>Hier kunt je emoticon sets selecteren en markeer die je gaat gebruiken</translation>
  </message>
  <message>
   <location filename="../../plugins/emoticons/emoticonsoptions.ui" line="58"/>
   <source>Priority Up</source>
   <translation>Prioriteit omhoog</translation>
  </message>
  <message>
   <location filename="../../plugins/emoticons/emoticonsoptions.ui" line="61"/>
   <source>Click this button to rise priority of selected emoticon set</source>
   <translation>Klik op deze knop om de prioriteit van de geselecteerde emoticon te stellen</translation>
  </message>
  <message>
   <location filename="../../plugins/emoticons/emoticonsoptions.ui" line="71"/>
   <source>Priority Down</source>
   <translation>Prioriteit laag</translation>
  </message>
  <message>
   <location filename="../../plugins/emoticons/emoticonsoptions.ui" line="74"/>
   <source>Click this button to lower prioroty of selected emoticon set</source>
   <translation>Klik op deze knop om de prioriteit te verlagen van de geselecteerde emoticon set</translation>
  </message>
 </context>
</TS>