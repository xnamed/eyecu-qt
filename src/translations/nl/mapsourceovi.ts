<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" version="2.0">
 <context>
  <name>MapSourceOvi</name>
  <message>
   <location filename="../../plugins/mapsourceovi/mapsourceovi.cpp" line="12"/>
   <source>HERE map source</source>
   <translation>HERE Kaart source</translation>
  </message>
  <message>
   <location filename="../../plugins/mapsourceovi/mapsourceovi.cpp" line="13"/>
   <source>Allows Map plugin to use Nokia&apos;s HERE service as map source</source>
   <translation>Kaart plugin toestaan om Nokia&apos;s HERE service te gebruiken als kaart source</translation>
  </message>
 </context>
</TS>