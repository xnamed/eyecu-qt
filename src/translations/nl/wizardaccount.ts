<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" version="2.0">
 <context>
  <name>ConclusionPage</name>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1283"/>
   <source>Welcome to Jabber network!</source>
   <translation>Welkom bij Jabber netwerk!</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1287"/>
   <source>Account name</source>
   <translation>Account naam</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1292"/>
   <source>Press &quot;Finish&quot; button to close Wizard.</source>
   <comment>&quot;Finish&quot; should match the text of an appropriate Qt Wizard button</comment>
   <translation>Druk op de &quot;Finish&quot; knop om de wizard te sluiten.</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1296"/>
   <source>Additional account settings...</source>
   <translation>Extra accountinstellingen...</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1300"/>
   <source>Go online now</source>
   <translation>Nu online gaan</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1313"/>
   <source>Done!</source>
   <translation>Klaar!</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1314"/>
   <source>Connection Wizard cempleted successfuly.</source>
   <translation>Verbindings Wizard succesvol voltooid.</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1316"/>
   <source>Congratulations</source>
   <translation>Proficiat</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1319"/>
   <source>You successfully connected to Jabber as %1.</source>
   <translation>Succesvol verbonden bij Jabber als %1.</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1320"/>
   <source>You successfully registered at Jabber as %1.</source>
   <translation>Je bent succesvol geregistreerd bij Jabber als %1.</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1336"/>
   <source>Failure!</source>
   <translation>Misluikt!</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1337"/>
   <source>Connection Wizard failed to create an account for you</source>
   <translation>Verbindings Wizard kon geen account voor je aanmaken</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1338"/>
   <source>Sorry</source>
   <translation>Sorry</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1339"/>
   <source>Internal error occured!</source>
   <translation>Interne fout opgetreden!</translation>
  </message>
 </context>
 <context>
  <name>ConnectPage</name>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="747"/>
   <source>Connect to Jabber server</source>
   <translation>Verbonden met Jabber server</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="748"/>
   <source>Trying to logon or register at Jabber server</source>
   <translation>Proberen om in te loggen of te registreren op Jabber server</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="817"/>
   <source>Requesting registration form...</source>
   <translation>Vraagt inschrijfformulier...</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="817"/>
   <source>Connecting...</source>
   <translation>Verbinding...</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="841"/>
   <source>Failed to check connection :(</source>
   <translation>Mislukt om verbinding te checken :(</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="842"/>
   <source>Internal Error</source>
   <translation>Interne fout</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="843"/>
   <source>Click &apos;Back&apos; button to change the account credentials or the &apos;Finish&apos; button to add the account as is.</source>
   <translation>Klik op &apos;Terug&apos; knop om account referenties te veranderen of op de &apos;Finish&apos; knop om de account toe te voegen zoals het is.</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="910"/>
   <source>Connection failed!</source>
   <translation>Verbinding mislukt!</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="911"/>
   <source>Error</source>
   <translation>Fout</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="913"/>
   <source>Please go back and check your credentials.</source>
   <translation>Ga terug en controleer je referenties.</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="915"/>
   <source>Please go back and check your connection and server name.</source>
   <translation>Ga terug and check je verbinding en server naam.</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="917"/>
   <source>Please make sure the server supports in-band registration.</source>
   <translation>Zorg ervoor dat de server in-band registratie ondersteunt.</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="967"/>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1008"/>
   <source>Password</source>
   <translation>Paswoord</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="970"/>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1011"/>
   <source>Retype password</source>
   <translation>Geef nogmaals paswoord in</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="978"/>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1001"/>
   <source>e-mail</source>
   <translation>e-mail</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="997"/>
   <source>User name</source>
   <translation>Gebruikersnaam</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="999"/>
   <source>Enter the text you see</source>
   <translation>Vul tekst in die je ziet</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1038"/>
   <source>Failed to register :(</source>
   <translation>Mislukt te registreren :(</translation>
  </message>
 </context>
 <context>
  <name>ConnectionPage</name>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="719"/>
   <source>Connection settings</source>
   <translation>Verbindings instellingen</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="721"/>
   <source>Check your connection settings.</source>
   <translation>Check uw verbinding instellingen.</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="722"/>
   <source>If you&apos;re not certain about it, please, contact your system administartor.</source>
   <translation>Als je er niet zeker van bent, neem dan contact op met uw systeembeheerder.</translation>
  </message>
 </context>
 <context>
  <name>ConnectionWizard</name>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="85"/>
   <source>Connection Wizard</source>
   <translation>Verbindings Wizard</translation>
  </message>
 </context>
 <context>
  <name>CredentialsPage</name>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="585"/>
   <source>Credentials</source>
   <translation>Referenties</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="586"/>
   <source>Please, enter your user name, password and resource</source>
   <translation>Voer uw gebruikersnaam, wachtwoord en uw bronnen</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="588"/>
   <source>User name and resource</source>
   <translation>Gebruikersnaam en uw bronnen</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="601"/>
   <source>Password</source>
   <translation>Paswoord</translation>
  </message>
  <message>
   <source>Re-type password</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="662"/>
   <source>Account exists</source>
   <translation>Account bestaat al</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="662"/>
   <source>Account with specified Server and User Name exists already! Please choose different Server or User Name.</source>
   <translation>Account bij opgegeven Server en de Gebruikersnaam bestaat al! Kies andere Server of Gebruikersnaam.</translation>
  </message>
 </context>
 <context>
  <name>IntroPage</name>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="127"/>
   <source>Connect to Jabber</source>
   <translation>Verbonden met Jabber</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="128"/>
   <source>This wizard will help you to create a Jabber account</source>
   <translation>Deze Wizard helpt u om een Jabber account aan te maken</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="133"/>
   <source>Are you already registered at Jabber network?</source>
   <translation>Ben je al geregistreerd op een Jabber netwerk?</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="135"/>
   <source>&amp;Yes</source>
   <translation>&amp;Ja</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="135"/>
   <source>I have an account on a Jabber server</source>
   <translation>Ik heb al een account op een Jabber server</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="136"/>
   <source>&amp;No</source>
   <translation>&amp;Nee</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="136"/>
   <source>I want to register on a Jabber server</source>
   <translation>Ik wil registreren op een Jabber account</translation>
  </message>
 </context>
 <context>
  <name>NetworkPage</name>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="161"/>
   <source>Other XMPP</source>
   <translation>Andere XMPP</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="161"/>
   <source>An independent XMPP server (Jabber)</source>
   <translation>Een onafhankelijke XMPP server (Jabber)</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="162"/>
   <source>Google Talk</source>
   <translation>Google Talk</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="162"/>
   <source>A social network and chat service from Google</source>
   <translation>Een sociaal netwerk en chatsdienst van Google</translation>
  </message>
  <message>
   <source>Yandex Online</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <source>A popular Russian portal (internet serach, e-mail, news, chat and so on)</source>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="164"/>
   <source>Odnoklassniki</source>
   <translation>Odnoklassniki</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="164"/>
   <source>A popular Russian social network, owned by Mail.Ru Group</source>
   <translation>Een populaire Russische sociaal netwerk, eigendom van Mail.Ru Group</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="166"/>
   <source>LiveJournal</source>
   <translation>LiveJournaal</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="166"/>
   <source>A popular blogging service</source>
   <translation>Een populaire blogdienst</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="168"/>
   <source>QIP</source>
   <translation>QIP</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="168"/>
   <source>A popular Russian portal (internet serach, e-mail, news, chat and so on), owned by OOO &quot;Media Mir&quot;, mostly known by its multiprotocol IM client</source>
   <translation>Een populaire Russische portaaal (internet zoeken, e-mail, nieuws, chat, enzovoort), eigendom van OOO &quot;Media Mir&quot;, vooral bekend door zijn multiprotocol IM-client</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="172"/>
   <source>Network selection</source>
   <translation>Netwerk selectie</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="173"/>
   <source>Please, select a network, you have registered in</source>
   <translation>Alstublieft, selecteer een netwerk, welke je hebt geregistreerd</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="175"/>
   <source>There are some social networks, portals or blogging services, which have their own XMPP servers. If you have an account in some of those networks, you may use it in eyeCU.
Please, note, that those XMPP servers usually have implemented their own specific features, which cannot be used with standard XMPP clients, like eyeCU. At the same time, they implenment only limited set of standard XMPP extensions, which could be used with standard clients.So, it's hardly recommended to use such accounts only as additional account, to have convenient access to your social networks and services. To have full-featured XMPP experienece, it's recommended to have an account on independent XMPP (Jabber) server. To connect to such server, please, select &quot;Other XMPP&quot;.
If you don&apos;t have a Jabber account yet, please, go Back and select &quot;No&quot; to register in Jabber.</source>
   <translation>Er zijn een aantal sociale netwerken, portals of bloggen diensten, die hun eigen XMPP servers hebben. Als je een account hebt in een aantal van die netwerken, kunt u deze gebruiken in eyeCU.
Let op, dat die XMPP servers meestal hun eigen specifieke kenmerken hebben, die niet kunnen worden gebruikt met een standaard XMPP cliënt, zoals eyeCU. Tegelijkertijd, implenmenteren  ze slechts een beperkt aantal standaard XMPP uitbreidingen die alleen kunnen worden gebruikt met standaard clients. Daarom, is het niet aanbevolen dergelijke accounts alleen te gebruiken voor een extra account om gemakkelijke toegang aan sociale en -diensten. Om de volledige functionaliteit van XMPP ervaringen te hebben, is het aan te raden om een account te hebben op een onafhankelijke XMPP (Jabber) server. Om verbinding te maken met een dergelijke server, alsjeblieft, selecteer &quot;Andere XMPP&quot;
Als je nog geen Jabber account hebt, ga terug en selecteer &quot;Nee&quot; om te registreren bij Jabber.</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="197"/>
   <source>Description</source>
   <translation>Omschrijving</translation>
  </message>
 </context>
 <context>
  <name>RegisterSubmitPage</name>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1093"/>
   <source>Connect to Jabber server</source>
   <translation>Verbonden met Jabber server</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1094"/>
   <source>Trying to logon or register at Jabber server</source>
   <translation>Proberen om in te loggen of te registreren op Jabber server</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1132"/>
   <source>Registering...</source>
   <translation>Registreren...</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1144"/>
   <source>Failed to register :(</source>
   <translation>Mislukt te registreren :(</translation>
  </message>
 </context>
 <context>
  <name>ServerPage</name>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="245"/>
   <source>Server selection</source>
   <translation>Server selectie</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="246"/>
   <source>Specify a server you want to use.</source>
   <translation>Kies een server welke je wilt gebruiken.</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="248"/>
   <source>Please select a server from the list</source>
   <translation>Selecteer een server van de lijst</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="257"/>
   <source>Or enter manually</source>
   <translation>Of handmatig invoeren</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="316"/>
   <source>Server</source>
   <translation>Server</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="316"/>
   <source>Registration</source>
   <translation>Registratie</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="316"/>
   <source>PEP</source>
   <translation>PEP</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="316"/>
   <source>Message Archive</source>
   <translation>Bericht archief</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="317"/>
   <source>Message Carbons</source>
   <translation>Bericht Carbons</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="317"/>
   <source>User Search</source>
   <translation>Gebruiker zoekt</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="317"/>
   <source>MUC</source>
   <translation>MUC</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="317"/>
   <source>Proxy</source>
   <translation>Proxy</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="318"/>
   <source>File Store</source>
   <translation>Bestand bewaren</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="318"/>
   <source>Transports</source>
   <translation>Transporten</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="318"/>
   <source>Country</source>
   <translation>Land</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="382"/>
   <source>In-band registration available</source>
   <translation>In-band registratie beschikbaar</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="486"/>
   <source>Warning!</source>
   <translation>Waarschuwing!</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="487"/>
   <source>The Server you selected do not support Personal Events (PEP)!
Some %1 features (like User Location, User Activity, User Mood and User Tune) will not work!
Using this server for your primary account is deprecated.
Press &quot;Ok&quot; to proceed or &quot;Cancel&quot; to select another server.</source>
   <translation>De geselecteerde Server heeft geen ondersteuning voor persoonlijke gebeurtenissen (PEP)!
Sommige %1 functies (zoals Gebruikerslocatie, Gebruikers activiteit, Gebruikers stemming en Gebruikers melodie) zal niet werken!
Gebruik van deze server voor eerste account wordt afgeraden.
Druk op &quot;OK&quot; om door te gaan of &quot;Annuleer&quot; om andere server te kiezen.
</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="499"/>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="514"/>
   <source>Attention!</source>
   <translation>Attentie!</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="500"/>
   <source>The Server you selected belongs to %1 network!
It is not recommended to use such servers as your primary account, due to restricted functionality they have. In-band registration is also unavailable.
Please, choose another server. If you have an account in such network, please, go back and select &quot;Yes&quot; on the first page. If you don&apos;t have an account, but want to have it, you&apos;ll need to visit %2 to register first.</source>
   <translation>Jou geselecteerde Server behoort tot het %1 netwerk!
Het is niet aan te raden om dergelijke servers te gebruiken voor uw primaire account, gevolg is de beperkte functionaliteit die ze hebben. In-band registratie is niet beschikbaar.
Aub, kies een andere server. Als je een account hebt in zo&apos;n netwerk, ga terug en selecteer &quot;Ja&quot; op de eerste pagina. Als u nog geen account hebt, maar wil registreren, moet u een bezoek brengen aan %2 om je te registreren.
</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="510"/>
   <source>%1 website</source>
   <translation>%1 website</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="514"/>
   <source>The Server you selected do not support in-band registration!
You'll have to register via web!
Press &quot;Ok&quot; to proceed or &quot;Cancel&quot; to select another server.</source>
   <translation>De geselecteerde Server heeft geen ondersteuning voor in-band registratie! 
Je moet je inschrijven via het web! 
Druk op &quot;Ok&quot; om verder te gaan of &quot;Annuleren&quot; om een andere server te selecteren.</translation>
  </message>
 </context>
 <context>
  <name>WebRegistrationInfo</name>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1174"/>
   <source>Web registration</source>
   <translation>Web registratie</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1177"/>
   <source>The server you selected doesn&apos;t support in-band registration.</source>
   <translation>De server die je selecteerde ondersteunt geen in-band registratie.</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1178"/>
   <source>Follow instructions below to register via web.</source>
   <translation>Volg onderstaande instructies op om te registreren via web.</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1202"/>
   <source>Open registration website</source>
   <translation>Open registratie van website</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1214"/>
   <source>How to register at %1</source>
   <translation>Hoe te registreren bij %1</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1250"/>
   <source>Warning!</source>
   <translation>Waarschuwing!</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardpages.cpp" line="1251"/>
   <source>Cannot open instructions.</source>
   <translation>Kan instructies niet openen.</translation>
  </message>
 </context>
 <context>
  <name>WizardAccount</name>
  <message>
   <location filename="../../plugins/wizardaccount/wizardaccount.cpp" line="14"/>
   <source>Connection Wizard</source>
   <translation>Verbindings Wizard</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardaccount.cpp" line="15"/>
   <source>A Wizard, which helps unexperienced user to connect to Jabber network</source>
   <translation>Een hulpje, die onervaren gebruikers helpt om verbinding te maken met Jabber netwerk</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardaccount.cpp" line="55"/>
   <source>Create an account?</source>
   <translation>Account aanmaken?</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardaccount.cpp" line="56"/>
   <source>It seems, you don&apos;t have a Jabber account yet. Do you want to start a Wizard, which will help you to connect to Jabber?</source>
   <translation>Het lijkt erop, dat je hebt nog geen Jabber account hebt. Wilt u het hulpje starten die zal u helpen om verbinding te maken met Jabber?</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardaccount.cpp" line="60"/>
   <source>You may start Connection Wizard anytime, by pressing &quot;%1&quot; link on &quot;Accounts&quot; page in the Options dialog.</source>
   <translation>Je kunt verbindings Wizard op elk moment beginnen, door op link &quot;%1&quot; te drukken in &quot;Accounts&quot; in het dialoogvenster Opties.</translation>
  </message>
  <message>
   <location filename="../../plugins/wizardaccount/wizardaccount.cpp" line="60"/>
   <source>Add account</source>
   <translation>Account toevoegen</translation>
  </message>
 </context>
</TS>