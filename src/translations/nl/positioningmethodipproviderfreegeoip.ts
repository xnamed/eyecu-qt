<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" version="2.0">
 <context>
  <name>PositioningMethodIpProviderFreegeoip</name>
  <message>
   <location filename="../../plugins/positioningmethodipproviderfreegeoip/positioningmethodipproviderfreegeoip.cpp" line="16"/>
   <source>Positining Method IP Provider freegeoip.net</source>
   <translation>Plaatsbepaling Methode IP provider freegeoip.net</translation>
  </message>
  <message>
   <location filename="../../plugins/positioningmethodipproviderfreegeoip/positioningmethodipproviderfreegeoip.cpp" line="17"/>
   <source>Allows to use freegeoip.net as an IP positioning provider</source>
   <translation>Toestaan om freegeoip.net te gebruiken als een IP positie provider</translation>
  </message>
  <message>
   <location filename="../../plugins/positioningmethodipproviderfreegeoip/positioningmethodipproviderfreegeoip.cpp" line="38"/>
   <source>freegeoip.net</source>
   <translation>freegeoip.net</translation>
  </message>
 </context>
</TS>