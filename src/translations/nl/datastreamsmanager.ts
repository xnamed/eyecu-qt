<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" sourcelanguage="en" version="2.0">
 <context>
  <name>DataStreamsManger</name>
  <message>
   <source>Data Streams Manager</source>
   <translation>Gegevens stream manager</translation>
  </message>
  <message>
   <source>None of the available streams are acceptable</source>
   <translation>Geen van de beschikbare streams zijn acceptabel</translation>
  </message>
  <message>
   <source>The profile is not understood or invalid</source>
   <translation>Het profiel in niet begrepen of ongeldig</translation>
  </message>
  <message>
   <source>Data Streams Initiation</source>
   <translation>Inleiding Gegevensstreams</translation>
  </message>
  <message>
   <source>Stream with same ID already exists</source>
   <translation>Stream met zelfde ID bestaat al</translation>
  </message>
  <message>
   <source>&lt;Default Profile&gt;</source>
   <translation>&lt;Standaard profiel&gt;</translation>
  </message>
  <message>
   <source>Allows to initiate a custom stream of data between two XMPP entities</source>
   <translation>Toestaan om een aangepaste stream van gegevens te starten tussen twee XMPP diensten</translation>
  </message>
  <message>
   <source>Supports the initiating of the custom stream of data between two XMPP entities</source>
   <translation>Ondersteunt de initiërende van aangepaste stream gegevens tussen twee XMPP diensten</translation>
  </message>
  <message>
   <source>Invalid stream initiation response</source>
   <translation>Ongeldige reactie van opening stream</translation>
  </message>
  <message>
   <source>Data Transfer</source>
   <translation>Gegevensoverdracht</translation>
  </message>
  <message>
   <source>Transfer method %1</source>
   <translation>Overdracht methode %1</translation>
  </message>
  <message>
   <source>Unsupported stream initiation request</source>
   <translation>Niet-ondersteunde stream initiatie verzoek</translation>
  </message>
 </context>
</TS>