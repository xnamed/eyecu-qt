<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" version="2.0">
 <context>
  <name>Audio</name>
  <message>
   <location filename="../../plugins/jinglertp/audio.ui" line="20"/>
   <source>Audio/Video chat</source>
   <translation>Audio/Video chat</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/audio.ui" line="41"/>
   <source>Audio</source>
   <translation>Audio</translation>
  </message>
 </context>
 <context>
  <name>JingleRtp</name>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="82"/>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="184"/>
   <source>Jingle RTP</source>
   <translation>Jingle RTP</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="83"/>
   <source>Implements XEP-0167: Jingle RTP Sessions</source>
   <translation>Implementeert:XEP-0167: Jingle RTP sessies</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="162"/>
   <source>When incoming voice or video call received</source>
   <translation>Een inkomende spraak- of video-oproep ontvangen</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="170"/>
   <source>When incoming voice or video call missed</source>
   <translation>Een inkomende spraak- of video-oproep gemist</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="407"/>
   <source>Incoming %1 call from %2</source>
   <translation>Inkomend %1 gesprek van %2</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="408"/>
   <source>Missed %1 call from %2</source>
   <translation>Gemiste %1 gesprek van %2</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="409"/>
   <source>video</source>
   <translation>video</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="409"/>
   <source>voice</source>
   <translation>Stem</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="420"/>
   <source>Missed call!</source>
   <translation>Gemiste oproep!</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="420"/>
   <source>Incoming call!</source>
   <translation>Inkomend gesprek!</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="456"/>
   <source>%1 - Chat</source>
   <translation>%1 - Chat</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="500"/>
   <source>Jingle RTP Sessions</source>
   <translation>Jingle RTP sessie</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="501"/>
   <source>Audio/Video chat via Jingle RTP</source>
   <translation>Audio/Video chat met Jingle RTP</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="667"/>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="775"/>
   <source>Video call</source>
   <translation>Video oproep</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="671"/>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="764"/>
   <source>Voice call</source>
   <translation>Spraakoproep</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="674"/>
   <source>Call cancelled</source>
   <translation>Oproep geannuleerd</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="677"/>
   <source>Call rejected</source>
   <translation>Oproep afgewezen</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="680"/>
   <source>Call finished</source>
   <translation>Oproep voltooid</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="684"/>
   <source>Call error</source>
   <translation>Bel fout</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="685"/>
   <source>Call error (%1)</source>
   <translation>Bel fout (%1)</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="786"/>
   <source>Hangup</source>
   <translation>Ophangen</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="798"/>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="1034"/>
   <source>Microphone OFF</source>
   <translation>Microfoon UIT</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertp.cpp" line="1013"/>
   <source>Microphone ON</source>
   <translation>Microfoon aan</translation>
  </message>
 </context>
 <context>
  <name>JingleRtpOptions</name>
  <message>
   <location filename="../../plugins/jinglertp/jinglertpoptions.ui" line="14"/>
   <source>Jingle RTP options</source>
   <translation>Jingle RTP opties</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertpoptions.ui" line="46"/>
   <source>Audio</source>
   <translation>Audio</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertpoptions.ui" line="147"/>
   <location filename="../../plugins/jinglertp/jinglertpoptions.ui" line="221"/>
   <source>Codec</source>
   <translation>Codec</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertpoptions.ui" line="160"/>
   <source>Notify Interval, ms</source>
   <translation>Meld interval, ms</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertpoptions.ui" line="170"/>
   <source>Sample rate, Hz</source>
   <translation>Sample rate, Hz</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertpoptions.ui" line="191"/>
   <source>Video</source>
   <translation>Video</translation>
  </message>
  <message>
   <location filename="../../plugins/jinglertp/jinglertpoptions.ui" line="241"/>
   <source>Use RTCP</source>
   <translation>Gebruik RTCP</translation>
  </message>
 </context>
</TS>