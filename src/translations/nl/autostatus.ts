<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" version="2.0">
 <context>
  <name>AutoRulesOptionsDialog</name>
  <message>
   <location filename="../../plugins/autostatus/autorulesoptionsdialog.cpp" line="181"/>
   <source>Auto Status Rules</source>
   <translation>Auto status regels</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autorulesoptionsdialog.cpp" line="196"/>
   <source>Time</source>
   <translation>Tijd</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autorulesoptionsdialog.cpp" line="196"/>
   <source>Status</source>
   <translation>Status</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autorulesoptionsdialog.cpp" line="196"/>
   <source>Text</source>
   <translation>Tekst</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autorulesoptionsdialog.cpp" line="196"/>
   <source>Priority</source>
   <translation>Prioriteit</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autorulesoptionsdialog.cpp" line="207"/>
   <source>Add</source>
   <translation>Toevoegen</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autorulesoptionsdialog.cpp" line="208"/>
   <source>Delete</source>
   <translation>Verwijder</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autorulesoptionsdialog.cpp" line="285"/>
   <source>Auto status</source>
   <translation>Auto status </translation>
  </message>
 </context>
 <context>
  <name>AutoStatus</name>
  <message>
   <location filename="../../plugins/autostatus/autostatus.cpp" line="30"/>
   <source>Auto Status</source>
   <translation>Auto status </translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatus.cpp" line="31"/>
   <source>Allows to change the status in accordance with the time of inactivity</source>
   <translation>Toestaan om de status in overeenstemming met de tijd van inactiviteit te wijzigen</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatus.cpp" line="103"/>
   <source>Automatic change of status</source>
   <translation>Verander automatisch de status</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatus.cpp" line="229"/>
   <location filename="../../plugins/autostatus/autostatus.cpp" line="249"/>
   <source>Auto status</source>
   <translation>Auto status </translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatus.cpp" line="304"/>
   <source>Auto status due to inactivity for more than #(m) minutes</source>
   <translation>Auto-status vanwege inactiviteit van meer dan # (m) minuten</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatus.cpp" line="312"/>
   <source>Disconnected due to inactivity for more than #(m) minutes</source>
   <translation>Verbinding verbroken vanwege inactiviteit van meer dan # (m) minuten</translation>
  </message>
 </context>
 <context>
  <name>AutoStatusOptionsWidget</name>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="19"/>
   <source>Show all rules for the automatic change of status...</source>
   <translation>Toom alle regels voor de automatische verandering van de status...</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="79"/>
   <source>Auto status due to inactivity for more than #(m) minutes</source>
   <translation>Auto-status vanwege inactiviteit van meer dan # (m) minuten</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="101"/>
   <source>Disconnected due to inactivity for more than #(m) minutes</source>
   <translation>Verbinding verbroken vanwege inactiviteit van meer dan # (m) minuten</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="123"/>
   <source>Auto Status</source>
   <translation>Auto status </translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="124"/>
   <source>You can insert date and time into auto status text:</source>
   <translation>Je kunt de datum en de tijd in de auto-status tekst voegen:</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="125"/>
   <source>   %(&lt;format&gt;) - current date and time</source>
   <translation>%(&lt;formaat&gt;) - huidige datum en tijd</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="126"/>
   <source>   $(&lt;format&gt;) - date and time you are idle form</source>
   <translation>$(&lt;formaat&gt;) - datum en tijd je inactief bent</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="127"/>
   <source>   #(&lt;format&gt;) - time you are idle for</source>
   <translation>#(&lt;formaat&gt;) - tijd dat je inactief bent</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="128"/>
   <source>Date Format:</source>
   <translation>Datum Formaat:</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="129"/>
   <source>   d - the day as number without a leading zero (1 to 31)</source>
   <translation>d - de dag als nummer zonder een voorafgaande nul (1 tot 31)</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="130"/>
   <source>   dd - the day as number with a leading zero (01 to 31)</source>
   <translation>dd - de dag als nummer met een voorafgaande nul (01 tot 31)</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="131"/>
   <source>   ddd - the abbreviated localized day name (e.g. &apos;Mon&apos; to &apos;Sun&apos;)</source>
   <translation>ddd - de verkorte lokale dag naam (e.g. &apos;Maan tot Zon&apos;)</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="132"/>
   <source>   dddd - the long localized day name (e.g. &apos;Monday&apos; to &apos;Sunday&apos;)</source>
   <translation>dddd - de lange lokale dag naam (e.g. &apos;Maandag tot Zondag&apos;)</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="133"/>
   <source>   M - the month as number without a leading zero (1-12)</source>
   <translation>M - De maand als nummer zonder een voorafgaande nul (1-12)</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="134"/>
   <source>   MM - the month as number with a leading zero (01-12)</source>
   <translation>MM - de maand als nummer met een voorafgaande nul (01-12)</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="135"/>
   <source>   MMM - the abbreviated localized month name (e.g. &apos;Jan&apos; to &apos;Dec&apos;)</source>
   <translation>MMM - de verkorte lokale maand naam (e.g. &apos;Jan tot Dec&apos;)</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="136"/>
   <source>   MMMM - the long localized month name (e.g. &apos;January&apos; to &apos;December&apos;)</source>
   <translation>MMMM - de lange lokale maand naam (e.g. &apos;Januari tot &apos;December&apos;)</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="137"/>
   <source>   yy - the year as two digit number (00-99)</source>
   <translation>yy - het jaar als twee cijfers (00-99)</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="138"/>
   <source>   yyyy - the year as four digit number</source>
   <translation>yyyy - het jaar als vier cijfers </translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="139"/>
   <source>Time Format:</source>
   <translation>Tijd formaat:</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="140"/>
   <source>   h - the hour without a leading zero (0 to 23 or 1 to 12 if AM/PM display)</source>
   <translation>h -  het uur zonder een voorafgaande nul (0 tot 23 of 1 tot 12 als AM/PM weergave)</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="141"/>
   <source>   hh - the hour with a leading zero (00 to 23 or 01 to 12 if AM/PM display)</source>
   <translation>hh - het uur met een voorafgaande nul (00 tot 23 of 01 tot 12 als AM/PM weergave)</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="142"/>
   <source>   H - the hour without a leading zero (0 to 23, even with AM/PM display)</source>
   <translation>H - het uur zonder een voorafgaande nul (0 tot 23, zelfs met AM/PM weergave)</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="143"/>
   <source>   HH - the hour with a leading zero (00 to 23, even with AM/PM display)</source>
   <translation>HH - het uur met een voorafgaande nul (00 tot 23, zelfs met AM/PM weergave)</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="144"/>
   <source>   m - the minute without a leading zero (0 to 59)</source>
   <translation>m - de minuut zonder een voorafgaande nul (0 tot 59)</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="145"/>
   <source>   mm - the minute with a leading zero (00 to 59)</source>
   <translation>mm - de minuut met een voorafgaande nul (00 tot 59)</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="146"/>
   <source>   s - the second without a leading zero (0 to 59)</source>
   <translation>s - een seconde zonder een voorafgaande nul (0 tot 59)</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="147"/>
   <source>   ss - the second with a leading zero (00 to 59)</source>
   <translation>ss - een seconde met een voorafgaande nul (00 tot 59)</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="148"/>
   <source>   z - the milliseconds without leading zeroes (0 to 999)</source>
   <translation>z - een milliseconde zonder voorafgaande nullen (0 tot 999)</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="149"/>
   <source>   zzz - the milliseconds with leading zeroes (000 to 999)</source>
   <translation>zz - milliseconden met voorafgaande nullen (000 tot 999)</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="150"/>
   <source>   AP or A - interpret as an AM/PM time. AP must be either &apos;AM&apos; or &apos;PM&apos;</source>
   <translation>AP of A - interpreteren als een AM / PM tijd. AP moet zijn &apos;AM&apos; of &apos;PM&apos;</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="151"/>
   <source>   ap or a - interpret as an AM/PM time. ap must be either &apos;am&apos; or &apos;pm&apos;</source>
   <translation>ap of a - interpreteren als een AM / PM tijd. ap moet zijn &apos;am&apos; of &apos;pm&apos;</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="152"/>
   <source>Example:</source>
   <translation>Voorbeeld:</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.cpp" line="153"/>
   <source>   Status is set to &apos;away&apos; at %(hh:mm:ss), because of idle from $(hh:mm:ss) for #(mm) minutes and #(ss) seconds</source>
   <translation>Status staat op &apos;weg&apos; om %(hh:mm:ss), wegens inactief $(hh:mm:ss) voor #(mm) minuten en #(ss) seconden</translation>
  </message>
 </context>
 <context>
  <name>AutoStatusOptionsWidgetClass</name>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.ui" line="22"/>
   <source>Change status to</source>
   <translation>Verander status naar</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.ui" line="32"/>
   <source>after</source>
   <translation>na</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.ui" line="39"/>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.ui" line="116"/>
   <source> minutes</source>
   <translation>Minuten</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.ui" line="52"/>
   <source>of inactivity with message:</source>
   <translation>zonder activiteit met bericht:</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.ui" line="109"/>
   <source>Automatically disconnect after</source>
   <translation>Automatisch disconnected na</translation>
  </message>
  <message>
   <location filename="../../plugins/autostatus/autostatusoptionswidget.ui" line="129"/>
   <source>of inactivity</source>
   <translation>zonder activiteit</translation>
  </message>
 </context>
</TS>