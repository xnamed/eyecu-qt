<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" version="2.0">
 <context>
  <name>ClientIcons</name>
  <message>
   <location filename="../../plugins/clienticons/clienticons.cpp" line="48"/>
   <source>Client Icons</source>
   <translation>Cliënt pictogrammen</translation>
  </message>
  <message>
   <location filename="../../plugins/clienticons/clienticons.cpp" line="49"/>
   <source>Displays a client icon in the roster</source>
   <translation>Toon een cliënt pictogram in het rooster</translation>
  </message>
  <message>
   <location filename="../../plugins/clienticons/clienticons.cpp" line="185"/>
   <source>Show software version</source>
   <translation>Toon software versie</translation>
  </message>
  <message>
   <location filename="../../plugins/clienticons/clienticons.cpp" line="185"/>
   <source>F3</source>
   <comment>Show software version (chat)</comment>
   <translation>F3</translation>
  </message>
  <message>
   <location filename="../../plugins/clienticons/clienticons.cpp" line="205"/>
   <location filename="../../plugins/clienticons/clienticons.cpp" line="210"/>
   <source>Show client icons</source>
   <translation>Toon cliënt pictogrammen</translation>
  </message>
  <message>
   <location filename="../../plugins/clienticons/clienticons.cpp" line="213"/>
   <source>Display client icon</source>
   <translation>Toon cliënt pictogram </translation>
  </message>
  <message>
   <location filename="../../plugins/clienticons/clienticons.cpp" line="315"/>
   <location filename="../../plugins/clienticons/clienticons.cpp" line="429"/>
   <location filename="../../plugins/clienticons/clienticons.cpp" line="588"/>
   <source>Client:</source>
   <translation>Cliënt:</translation>
  </message>
 </context>
</TS>