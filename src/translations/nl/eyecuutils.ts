<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" version="2.0">
 <context>
  <name>PasswordDialog</name>
  <message>
   <location filename="../../utils/passworddialog.cpp" line="12"/>
   <source>Enter password:</source>
   <translation>Paswoord invoeren:</translation>
  </message>
  <message>
   <location filename="../../utils/passworddialog.cpp" line="18"/>
   <source>Save password</source>
   <translation>Paswoord opslaan</translation>
  </message>
 </context>
 <context>
  <name>SearchLineEdit</name>
  <message>
   <location filename="../../utils/searchlineedit.cpp" line="15"/>
   <source>Search options</source>
   <translation>Zoek opties</translation>
  </message>
  <message>
   <location filename="../../utils/searchlineedit.cpp" line="26"/>
   <source>Clear text</source>
   <translation>Verwijder tekst</translation>
  </message>
 </context>
 <context>
  <name>XmppSaslError</name>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="419"/>
   <source>Authorization aborted</source>
   <translation>Toestemming afgebroken</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="420"/>
   <source>Account disabled</source>
   <translation>Account uitgeschakeld</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="421"/>
   <source>Credentials expired</source>
   <translation>Referenties verlopen</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="422"/>
   <source>Encryption required</source>
   <translation>Encryptie verplicht</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="423"/>
   <source>Incorrect encoding</source>
   <translation>Verkeerde codering</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="424"/>
   <source>Invalid authorization id</source>
   <translation>Ongeldige machtiging id</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="425"/>
   <source>Invalid mechanism</source>
   <translation>Ongeldige mechanisme</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="426"/>
   <source>Malformed request</source>
   <translation>Ongeldige aanvraag</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="427"/>
   <source>Mechanism is too weak</source>
   <translation>Mechanisme is te zwak</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="428"/>
   <source>Not authorized</source>
   <translation>Niet bevoegd</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="429"/>
   <source>Temporary authentication failure</source>
   <translation>Tijdelijke verificatie storing</translation>
  </message>
 </context>
 <context>
  <name>XmppStreamError</name>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="331"/>
   <location filename="../../utils/xmpperror.cpp" line="581"/>
   <source>Undefined error condition</source>
   <translation>Ongedefinieerde fout</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="332"/>
   <location filename="../../utils/xmpperror.cpp" line="582"/>
   <source>Bad request format</source>
   <translation>Slechte verzoek formaat</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="333"/>
   <source>Bad namespace prefix</source>
   <translation>Slechte naamruimte prefix</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="334"/>
   <location filename="../../utils/xmpperror.cpp" line="583"/>
   <source>Conflict</source>
   <translation>Conflict</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="335"/>
   <source>Connection timeout</source>
   <translation>Timeout van verbinding</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="336"/>
   <source>Host is not serviced</source>
   <translation>Host is niet bereikbaar</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="337"/>
   <source>Unknown host</source>
   <translation>Onbekende host</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="338"/>
   <source>Improper addressing</source>
   <translation>Onjuiste adressering</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="339"/>
   <location filename="../../utils/xmpperror.cpp" line="587"/>
   <source>Internal server error</source>
   <translation>Interne server fout</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="340"/>
   <source>Invalid from address</source>
   <translation>Ongeldig adres</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="341"/>
   <source>Invalid namespace</source>
   <translation>Ongeldige naamruimte</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="342"/>
   <source>Invalid XML</source>
   <translation>Ongeldige XML</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="343"/>
   <location filename="../../utils/xmpperror.cpp" line="592"/>
   <source>Not authorized</source>
   <translation>Niet bevoegd</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="344"/>
   <source>XML not well formed</source>
   <translation>XML niet goed gevormd</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="345"/>
   <location filename="../../utils/xmpperror.cpp" line="593"/>
   <source>Policy violation</source>
   <translation>Police overtreding</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="346"/>
   <source>Remote connection failed</source>
   <translation>Remore verbinding mislukt</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="347"/>
   <source>Stream need to be reseted</source>
   <translation>Spoo moet gereset worden</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="348"/>
   <location filename="../../utils/xmpperror.cpp" line="599"/>
   <source>Resource constraint</source>
   <translation>Resource beperking</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="349"/>
   <source>Restricted XML</source>
   <translation>Beperkt XML</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="350"/>
   <source>See other host</source>
   <translation>Zie andere host</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="351"/>
   <source>System shutdown</source>
   <translation>Systeem sluit</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="352"/>
   <source>Unsupported encoding</source>
   <translation>Encoding niet ondersteund</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="353"/>
   <source>Unsupported feature</source>
   <translation>Functie niet ondersteunt</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="354"/>
   <source>Unsupported stanza type</source>
   <translation>Stanza type niet ondersteunt</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="355"/>
   <source>Unsupported version</source>
   <translation>Versie niet ondersteunt</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="584"/>
   <source>Feature not implemented</source>
   <translation>Functie niet geïmplementeerd</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="585"/>
   <source>Insufficient permissions</source>
   <translation>Onvoldoende rechten</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="586"/>
   <source>Recipient changed address</source>
   <translation>Ontvanger heeft adres veranderd</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="588"/>
   <source>Requested item not found</source>
   <translation>Aangevraagde item niet gevonden</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="589"/>
   <source>Malformed XMPP address</source>
   <translation>Ongeldige XMPP adres</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="590"/>
   <source>Not accepted by the recipient</source>
   <translation>Niet geaccepteerd door ontvanger</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="591"/>
   <source>Not allowed by the recipient</source>
   <translation>Niet toegestaan door ontvanger</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="594"/>
   <source>Recipient unavailable</source>
   <translation>Ontvanger niet beschikbaar</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="595"/>
   <source>Redirect to another address</source>
   <translation>Doorsturen naar een ander adres</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="596"/>
   <source>Registration required</source>
   <translation>Registratie verplicht</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="597"/>
   <source>Remote server not found</source>
   <translation>Remote server niet gevonden</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="598"/>
   <source>Remote server timeout</source>
   <translation>Remote sever timeout</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="600"/>
   <source>Service unavailable</source>
   <translation>dienst onbereikbaar</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="601"/>
   <source>Subscription required</source>
   <translation>Abonnement vereist</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="602"/>
   <source>Unexpected request</source>
   <translation>Onverwachtte aanvraag</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="604"/>
   <source>Resource limit exceeded</source>
   <translation>Resource limiet overschreden</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="605"/>
   <source>Stanza is too big</source>
   <translation>Stanza is te groot</translation>
  </message>
  <message>
   <location filename="../../utils/xmpperror.cpp" line="606"/>
   <source>Too many stanzas</source>
   <translation>Te veel stanzas</translation>
  </message>
 </context>
</TS>