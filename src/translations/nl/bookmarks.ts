<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" sourcelanguage="en" version="2.0">
 <context>
  <name>Bookmarks</name>
  <message>
   <source>Bookmarks</source>
   <translation>Bladwijzers</translation>
  </message>
  <message>
   <source>Allows to create bookmarks at the jabber conference and web pages</source>
   <translation>Toestaan om bladwijzers te maken bij een Jabber conferentie en webpagina&apos;s</translation>
  </message>
  <message>
   <source>Edit Bookmarks</source>
   <translation>Bewerk Bladwijzers</translation>
  </message>
  <message>
   <source>Add to Bookmarks</source>
   <translation>Voeg toe aan bladwijzers</translation>
  </message>
  <message>
   <source>Remove from Bookmarks</source>
   <translation>Verwijder van bladwijzers</translation>
  </message>
  <message>
   <source>Edit Bookmark</source>
   <translation>Bewerk bladwijzer</translation>
  </message>
  <message>
   <source>Rename Bookmark</source>
   <translation>Hernoem bladwijzer</translation>
  </message>
  <message>
   <source>Enter bookmark name:</source>
   <translation>Voer bladwijzer naam in:</translation>
  </message>
  <message>
   <source>Join to Conference at Startup</source>
   <translation>De conferentie binnengaan bij het opstarten</translation>
  </message>
  <message>
   <source>Conferences</source>
   <translation>Vergaderingen</translation>
  </message>
  <message>
   <source>Disable auto join to conferences on this computer</source>
   <translation>Schakel auto-join voor vergaderingen uit op deze computer</translation>
  </message>
  <message>
   <source>Show windows of auto joined conferences at startup</source>
   <translation>Toon vensters van auto-join vergaderingen met opstarten</translation>
  </message>
 </context>
 <context>
  <name>EditBookmarkDialog</name>
  <message>
   <source>Field &apos;Name&apos; should not be empty</source>
   <translation>Veld &apos;Naam&apos; mag niet leeg zijn</translation>
  </message>
  <message>
   <source>In URL bookmark field &apos;URL&apos; should not be empty</source>
   <translation>In URL bladwijzer mag het veld &apos;URL&apos; niet leeg zijn</translation>
  </message>
  <message>
   <source>In conference bookmark field &apos;Room&apos; should not be empty</source>
   <translation>In conferentie bladwijzer mag het veld &apos;Kamer&apos;niet leeg zijn</translation>
  </message>
  <message>
   <source>Error</source>
   <translation>Fout</translation>
  </message>
 </context>
 <context>
  <name>EditBookmarkDialogClass</name>
  <message>
   <source>Edit bookmark</source>
   <translation>Bewerk bladwijzer</translation>
  </message>
  <message>
   <source>Password:</source>
   <translation>Paswoord:</translation>
  </message>
  <message>
   <source>Auto join</source>
   <translation>Automatisch binnengaan</translation>
  </message>
  <message>
   <source>Conference:</source>
   <translation>Conferentie:</translation>
  </message>
  <message>
   <source>Conference as &apos;name@service.server.com&apos;</source>
   <translation>Conferentie als </translation>
  </message>
  <message>
   <source>Nickname:</source>
   <translation>Bijnaam:</translation>
  </message>
  <message>
   <source>Your nickname in conference</source>
   <translation>Je bijnaam in conferentie</translation>
  </message>
  <message>
   <source>Conference password if required</source>
   <translation>Conferentie paswoord is vereist</translation>
  </message>
  <message>
   <source>Name:</source>
   <translation>Naam:</translation>
  </message>
  <message>
   <source>Bookmark to Conference</source>
   <translation>Bladwijzer naar Conferentie</translation>
  </message>
  <message>
   <source>Bookmark to Link</source>
   <translation>Bladwijzer naar link</translation>
  </message>
  <message>
   <source>Link:</source>
   <translation>Koppeling:</translation>
  </message>
  <message>
   <source>Any valid link</source>
   <translation>Elke geldige koppeling</translation>
  </message>
  <message>
   <source>Bookmark name</source>
   <translation>Bladwijzer naam</translation>
  </message>
 </context>
 <context>
  <name>EditBookmarksDialog</name>
  <message>
   <source>Cant save bookmarks to server</source>
   <translation>Kan bladwijzer niet op server opslaan</translation>
  </message>
  <message>
   <source>Edit bookmarks - %1</source>
   <translation>Bewerk Bladwijzers - %1</translation>
  </message>
  <message>
   <source>Error</source>
   <translation>Fout</translation>
  </message>
 </context>
 <context>
  <name>EditBookmarksDialogClass</name>
  <message>
   <source>Edit bookmarks</source>
   <translation>Bewerk bladwijzers</translation>
  </message>
  <message>
   <source>Name</source>
   <translation>Naam</translation>
  </message>
  <message>
   <source>JID/URL</source>
   <translation>JID/URL</translation>
  </message>
  <message>
   <source>Nick</source>
   <translation>Bijnaam</translation>
  </message>
  <message>
   <source>Add</source>
   <translation>Toevoegen</translation>
  </message>
  <message>
   <source>Edit</source>
   <translation>Bewerk</translation>
  </message>
  <message>
   <source>Delete</source>
   <translation>Verwijder</translation>
  </message>
  <message>
   <source>Up</source>
   <translation>Omhoog</translation>
  </message>
  <message>
   <source>Down</source>
   <translation>Omlaag</translation>
  </message>
 </context>
</TS>