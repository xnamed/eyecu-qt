<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" version="2.0">
 <context>
  <name>JingleTransportRawUdp</name>
  <message>
   <location filename="../../plugins/jingletransportrawudp/jingletransportrawudp.cpp" line="16"/>
   <location filename="../../plugins/jingletransportrawudp/jingletransportrawudp.cpp" line="286"/>
   <source>Jingle RAW-UDP Transport</source>
   <translation>Jingle RAW-UDP Transport</translation>
  </message>
  <message>
   <location filename="../../plugins/jingletransportrawudp/jingletransportrawudp.cpp" line="17"/>
   <source>Implements XEP-0177: Jingle RAW-UDP transport method</source>
   <translation>Implementeert XEP-0177: Jingle RAW transport methode</translation>
  </message>
  <message>
   <location filename="../../plugins/jingletransportrawudp/jingletransportrawudp.cpp" line="287"/>
   <source>Allows using RAW-UDP transport in Jingle sesions</source>
   <translation>Maakt gebruik van RAW-UDP transport in Jingle sessies</translation>
  </message>
 </context>
</TS>