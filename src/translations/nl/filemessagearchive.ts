<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" version="2.0">
 <context>
  <name>FileArchiveOptionsClass</name>
  <message>
   <location filename="../../plugins/filemessagearchive/filearchiveoptionswidget.ui" line="20"/>
   <source>Synchronize archive database at startup</source>
   <translation>Synchroniseer archiefdatabank bij het opstarten</translation>
  </message>
  <message>
   <location filename="../../plugins/filemessagearchive/filearchiveoptionswidget.ui" line="29"/>
   <source>Move history to:</source>
   <translation>Verplaats geschiedenis naar:</translation>
  </message>
 </context>
 <context>
  <name>FileArchiveOptionsWidget</name>
  <message>
   <location filename="../../plugins/filemessagearchive/filearchiveoptionswidget.cpp" line="49"/>
   <source>Select the location for the file archive</source>
   <translation>Selecteer de locatie voor archief bestand</translation>
  </message>
 </context>
 <context>
  <name>FileMessageArchive</name>
  <message>
   <location filename="../../plugins/filemessagearchive/filemessagearchive.cpp" line="63"/>
   <source>File Message Archive</source>
   <translation>Bestand bericht archief</translation>
  </message>
  <message>
   <location filename="../../plugins/filemessagearchive/filemessagearchive.cpp" line="64"/>
   <source>Allows to save the history of conversations in to local files</source>
   <translation>Toestaan om de geschiedenis van de gesprekken op te slaan in de lokale bestanden</translation>
  </message>
  <message>
   <location filename="../../plugins/filemessagearchive/filemessagearchive.cpp" line="114"/>
   <source>Failed to create database</source>
   <translation>Mislukt database te maken</translation>
  </message>
  <message>
   <location filename="../../plugins/filemessagearchive/filemessagearchive.cpp" line="115"/>
   <source>Failed to open database</source>
   <translation>Kan database niet openen</translation>
  </message>
  <message>
   <location filename="../../plugins/filemessagearchive/filemessagearchive.cpp" line="116"/>
   <source>Database format is not compatible</source>
   <translation>Database-indeling is niet compatibel</translation>
  </message>
  <message>
   <location filename="../../plugins/filemessagearchive/filemessagearchive.cpp" line="117"/>
   <source>Failed to to execute SQL query</source>
   <translation>Mislukt om een SQL vraag uit te voeren</translation>
  </message>
  <message>
   <location filename="../../plugins/filemessagearchive/filemessagearchive.cpp" line="150"/>
   <source>Local File Archive</source>
   <translation>Lokaal bestandsarchief</translation>
  </message>
  <message>
   <location filename="../../plugins/filemessagearchive/filemessagearchive.cpp" line="155"/>
   <source>History of conversations is stored in local files</source>
   <translation>Geschiedenis van de gesprekken wordt opgeslagen in lokale bestanden</translation>
  </message>
 </context>
</TS>