<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" version="2.0">
 <context>
  <name>BirthdayReminder</name>
  <message>
   <location filename="../../plugins/birthdayreminder/birthdayreminder.cpp" line="50"/>
   <source>Birthday Reminder</source>
   <translation>Verjaardags herinnering</translation>
  </message>
  <message>
   <location filename="../../plugins/birthdayreminder/birthdayreminder.cpp" line="51"/>
   <source>Reminds about birthdays of your friends</source>
   <translation>Herinnering over verjaardagen van je vrienden</translation>
  </message>
  <message>
   <location filename="../../plugins/birthdayreminder/birthdayreminder.cpp" line="144"/>
   <source>When reminding of upcoming birthdays</source>
   <translation>Herinnering over aanstaande verjaardagen</translation>
  </message>
  <message>
   <location filename="../../plugins/birthdayreminder/birthdayreminder.cpp" line="284"/>
   <source>Birthday remind</source>
   <translation>Verjaardags herinnering</translation>
  </message>
  <message>
   <location filename="../../plugins/birthdayreminder/birthdayreminder.cpp" line="290"/>
   <location filename="../../plugins/birthdayreminder/birthdayreminder.cpp" line="354"/>
   <source>Birthday today!</source>
   <translation>Verjaardag vandaag!</translation>
  </message>
  <message numerus="yes">
   <location filename="../../plugins/birthdayreminder/birthdayreminder.cpp" line="290"/>
   <source>Birthday in %n day(s),
 %1</source>
   <translation><numerusform>Verjaardag over %n dagen,
%1</numerusform><numerusform>Verjaardag over %n dagen,
%1</numerusform></translation>
  </message>
  <message numerus="yes">
   <location filename="../../plugins/birthdayreminder/birthdayreminder.cpp" line="351"/>
   <source>%1 turns %n</source>
   <translation><numerusform>%1 beurt %n</numerusform><numerusform>%1 beurten %n</numerusform></translation>
  </message>
  <message numerus="yes">
   <location filename="../../plugins/birthdayreminder/birthdayreminder.cpp" line="354"/>
   <source>Birthday in %n day(s)!</source>
   <translation><numerusform>Verjaardag in %n dag!</numerusform><numerusform>Verjaardag in %n dagen!</numerusform></translation>
  </message>
 </context>
</TS>