<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" version="2.0">
 <context>
  <name>FileStream</name>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestream.cpp" line="284"/>
   <source>Waiting for a response to send a file request</source>
   <translation>Wacht op een antwoord om een bestand te verzenden</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestream.cpp" line="533"/>
   <source>Connecting</source>
   <translation>Verbinding</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestream.cpp" line="544"/>
   <source>Data transmission</source>
   <translation>Gegevens overdracht</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestream.cpp" line="568"/>
   <source>Data transmission finished</source>
   <translation>Gegevens overdracht voltooid</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestream.cpp" line="596"/>
   <source>Disconnecting</source>
   <translation>Verbinding verbreken</translation>
  </message>
 </context>
 <context>
  <name>FileStreamsManager</name>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamsmanager.cpp" line="45"/>
   <source>File Streams Manager</source>
   <translation>Streams bestandsbeheer</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamsmanager.cpp" line="46"/>
   <source>Allows to initiate a thread for transferring files between two XMPP entities</source>
   <translation>Toestaan om het starten van een thread voor het overbrengen van bestanden tussen twee XMPP entiteiten</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamsmanager.cpp" line="89"/>
   <source>Show file transfers</source>
   <translation>Toon bestandsoverdrachten</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamsmanager.cpp" line="89"/>
   <source>Ctrl+T</source>
   <comment>Show file transfers</comment>
   <translation>Ctrl+T</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamsmanager.cpp" line="91"/>
   <source>File input/output error</source>
   <translation>Bestandsfout invoer/uitvoer</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamsmanager.cpp" line="92"/>
   <source>File size unexpectedly changed</source>
   <translation>Bestandsgrootte onverwacht gewijzigd</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamsmanager.cpp" line="93"/>
   <source>Connection timed out</source>
   <translation>Time-out verbinding</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamsmanager.cpp" line="94"/>
   <source>Data transmission terminated by remote user</source>
   <translation>Gegevensoverdracht beëindigd door externe gebruiker</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamsmanager.cpp" line="104"/>
   <source>File Transfers</source>
   <translation>Bestandsoverdrachten</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamsmanager.cpp" line="144"/>
   <source>File transfer</source>
   <translation>Bestandsoverdracht</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamsmanager.cpp" line="146"/>
   <source>Create separate folder for each sender</source>
   <translation>Maak een aparte map voor elke afzender</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamsmanager.cpp" line="156"/>
   <source>Default transfer method:</source>
   <translation>Standaard overdrachts methode:</translation>
  </message>
 </context>
 <context>
  <name>FileStreamsOptionsWidget</name>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamsoptionswidget.cpp" line="33"/>
   <source>Select default directory</source>
   <translation>Selecteer standaard map</translation>
  </message>
 </context>
 <context>
  <name>FileStreamsOptionsWidgetClass</name>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamsoptionswidget.ui" line="20"/>
   <source>Default directory:</source>
   <translation>Standaard map:</translation>
  </message>
 </context>
 <context>
  <name>FileStreamsWindow</name>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamswindow.cpp" line="38"/>
   <source>File Transfers</source>
   <translation>Bestandsoverdrachten</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamswindow.cpp" line="76"/>
   <source>File Name</source>
   <translation>Bestandsnaam</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamswindow.cpp" line="76"/>
   <source>State</source>
   <translation>Staat</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamswindow.cpp" line="76"/>
   <source>Size</source>
   <translation>Grootte</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamswindow.cpp" line="76"/>
   <source>Progress</source>
   <translation>Vooruitgang</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamswindow.cpp" line="76"/>
   <source>Speed</source>
   <translation>Snelheid</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamswindow.cpp" line="137"/>
   <source>Create</source>
   <translation>Maak</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamswindow.cpp" line="140"/>
   <source>Negotiate</source>
   <translation>Onderhandelen</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamswindow.cpp" line="143"/>
   <source>Connect</source>
   <translation>Verbind</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamswindow.cpp" line="146"/>
   <source>Transfer</source>
   <translation>Overdracht</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamswindow.cpp" line="149"/>
   <source>Disconnect</source>
   <translation>Verbinding verbreken</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamswindow.cpp" line="152"/>
   <source>Finished</source>
   <translation>Voltooid</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamswindow.cpp" line="155"/>
   <source>Aborted</source>
   <translation>Afgebroken</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamswindow.cpp" line="158"/>
   <source>Unknown</source>
   <translation>Onbekend</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamswindow.cpp" line="169"/>
   <location filename="../../plugins/filestreamsmanager/filestreamswindow.cpp" line="332"/>
   <location filename="../../plugins/filestreamsmanager/filestreamswindow.cpp" line="333"/>
   <source>/sec</source>
   <translation>/sec</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamswindow.cpp" line="229"/>
   <source>B</source>
   <comment>Byte</comment>
   <translation>B</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamswindow.cpp" line="235"/>
   <source>KB</source>
   <comment>Kilobyte</comment>
   <translation>KB</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamswindow.cpp" line="240"/>
   <source>MB</source>
   <comment>Megabyte</comment>
   <translation>MB</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamswindow.cpp" line="245"/>
   <source>GB</source>
   <comment>Gigabyte</comment>
   <translation>GB</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamswindow.cpp" line="331"/>
   <source>Active: %1/%2</source>
   <translation>Actieve: %1/%2</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamswindow.cpp" line="332"/>
   <source>Downloads: %1 at %2</source>
   <translation>Downloads: %1 bij %2</translation>
  </message>
  <message>
   <location filename="../../plugins/filestreamsmanager/filestreamswindow.cpp" line="333"/>
   <source>Uploads: %1 at %2</source>
   <translation>Uploads: %1 bij %2</translation>
  </message>
 </context>
</TS>