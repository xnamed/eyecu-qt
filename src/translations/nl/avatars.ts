<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" version="2.0">
 <context>
  <name>AvatarOptionsWidget</name>
  <message>
   <location filename="../../plugins/avatars/avataroptionswidget.ui" line="63"/>
   <source>Small</source>
   <translation>Smal</translation>
  </message>
  <message>
   <location filename="../../plugins/avatars/avataroptionswidget.ui" line="68"/>
   <source>Normal</source>
   <translation>Normaal</translation>
  </message>
  <message>
   <location filename="../../plugins/avatars/avataroptionswidget.ui" line="73"/>
   <source>Large</source>
   <translation>Groot</translation>
  </message>
  <message>
   <location filename="../../plugins/avatars/avataroptionswidget.ui" line="87"/>
   <source>Avatar position</source>
   <translation>Avatar positie</translation>
  </message>
  <message>
   <location filename="../../plugins/avatars/avataroptionswidget.ui" line="97"/>
   <source>Avatar size</source>
   <translation>Avatar grootte</translation>
  </message>
  <message>
   <location filename="../../plugins/avatars/avataroptionswidget.ui" line="107"/>
   <source>Display avatars for offline contacts grayscaled</source>
   <translation>Toon avatars voor offline contacten grijs getint</translation>
  </message>
  <message>
   <location filename="../../plugins/avatars/avataroptionswidget.ui" line="49"/>
   <source>At Right</source>
   <translation>Rechts</translation>
  </message>
  <message>
   <location filename="../../plugins/avatars/avataroptionswidget.ui" line="54"/>
   <source>At Left</source>
   <translation>Links</translation>
  </message>
  <message>
   <location filename="../../plugins/avatars/avataroptionswidget.ui" line="35"/>
   <source>Display avatars</source>
   <translation>Toon avatars</translation>
  </message>
 </context>
 <context>
  <name>AvatarSizeOptionsWidget</name>
  <message>
   <location filename="../../plugins/avatars/avatarsizeoptionswidget.ui" line="35"/>
   <source>Display empty avatars</source>
   <translation>Toon lege avatars</translation>
  </message>
  <message>
   <location filename="../../plugins/avatars/avatarsizeoptionswidget.ui" line="42"/>
   <source>Avatar size</source>
   <translation>Avatar grootte</translation>
  </message>
  <message>
   <location filename="../../plugins/avatars/avatarsizeoptionswidget.ui" line="63"/>
   <location filename="../../plugins/avatars/avatarsizeoptionswidget.ui" line="79"/>
   <location filename="../../plugins/avatars/avatarsizeoptionswidget.ui" line="95"/>
   <source>px</source>
   <translation>px</translation>
  </message>
  <message>
   <location filename="../../plugins/avatars/avatarsizeoptionswidget.ui" line="111"/>
   <source>Small</source>
   <translation>Smal</translation>
  </message>
  <message>
   <location filename="../../plugins/avatars/avatarsizeoptionswidget.ui" line="121"/>
   <source>Normal</source>
   <translation>Normaal</translation>
  </message>
  <message>
   <location filename="../../plugins/avatars/avatarsizeoptionswidget.ui" line="131"/>
   <source>Large</source>
   <translation>Groot</translation>
  </message>
 </context>
 <context>
  <name>Avatars</name>
  <message>
   <location filename="../../plugins/avatars/avatars.cpp" line="151"/>
   <location filename="../../plugins/avatars/avatars.cpp" line="484"/>
   <location filename="../../plugins/avatars/avatars.cpp" line="490"/>
   <location filename="../../plugins/avatars/avatars.cpp" line="496"/>
   <source>Avatars</source>
   <translation>Avatars</translation>
  </message>
  <message>
   <location filename="../../plugins/avatars/avatars.cpp" line="152"/>
   <source>Allows to set and display avatars</source>
   <translation>Het toestaan om avatars te tonen en in te stellen</translation>
  </message>
  <message>
   <location filename="../../plugins/avatars/avatars.cpp" line="990"/>
   <source>Avatar</source>
   <translation>Avatar</translation>
  </message>
  <message>
   <location filename="../../plugins/avatars/avatars.cpp" line="994"/>
   <source>Set avatar</source>
   <translation>Stel avatar in</translation>
  </message>
  <message>
   <location filename="../../plugins/avatars/avatars.cpp" line="1001"/>
   <source>Clear avatar</source>
   <translation>Wis avatar</translation>
  </message>
  <message>
   <location filename="../../plugins/avatars/avatars.cpp" line="1012"/>
   <source>Custom picture</source>
   <translation>Eigen Foto</translation>
  </message>
  <message>
   <location filename="../../plugins/avatars/avatars.cpp" line="1016"/>
   <source>Set custom picture</source>
   <translation>Stel eigen foto in</translation>
  </message>
  <message>
   <location filename="../../plugins/avatars/avatars.cpp" line="1023"/>
   <source>Clear custom picture</source>
   <translation>Wis eigen foto</translation>
  </message>
  <message>
   <location filename="../../plugins/avatars/avatars.cpp" line="1056"/>
   <source>Select avatar image</source>
   <translation>Selecteer avatar afbeelding</translation>
  </message>
  <message>
   <location filename="../../plugins/avatars/avatars.cpp" line="1056"/>
   <source>Image Files (*.png *.jpg *.bmp *.gif)</source>
   <translation>Afbeelding bestanden (*.png *.jpg *.bmp *.gif)</translation>
  </message>
 </context>
</TS>