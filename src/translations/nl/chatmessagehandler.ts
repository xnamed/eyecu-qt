<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" version="2.0">
 <context>
  <name>ChatMessageHandler</name>
  <message>
   <location filename="../../plugins/chatmessagehandler/chatmessagehandler.cpp" line="68"/>
   <source>Chat Messages</source>
   <translation>Chat berichten</translation>
  </message>
  <message>
   <location filename="../../plugins/chatmessagehandler/chatmessagehandler.cpp" line="69"/>
   <source>Allows to exchange chat messages</source>
   <translation>Toestaan om chatberichten uit te wisselen</translation>
  </message>
  <message>
   <location filename="../../plugins/chatmessagehandler/chatmessagehandler.cpp" line="218"/>
   <location filename="../../plugins/chatmessagehandler/chatmessagehandler.cpp" line="1124"/>
   <source>Open chat dialog</source>
   <translation>Open chat dialoog</translation>
  </message>
  <message>
   <location filename="../../plugins/chatmessagehandler/chatmessagehandler.cpp" line="218"/>
   <source>Return</source>
   <comment>Open chat dialog</comment>
   <translation>Terugkeren</translation>
  </message>
  <message>
   <location filename="../../plugins/chatmessagehandler/chatmessagehandler.cpp" line="225"/>
   <source>When receiving new chat message</source>
   <translation>Bij het ontvangen van nieuw chatbericht</translation>
  </message>
  <message>
   <location filename="../../plugins/chatmessagehandler/chatmessagehandler.cpp" line="346"/>
   <source>Message from %1</source>
   <translation>Bericht van %1</translation>
  </message>
  <message>
   <location filename="../../plugins/chatmessagehandler/chatmessagehandler.cpp" line="353"/>
   <source>Message received</source>
   <translation>Bericht ontvangen</translation>
  </message>
  <message>
   <location filename="../../plugins/chatmessagehandler/chatmessagehandler.cpp" line="372"/>
   <source>Message text hidden</source>
   <translation>Tekstbericht verborgen </translation>
  </message>
  <message>
   <location filename="../../plugins/chatmessagehandler/chatmessagehandler.cpp" line="465"/>
   <source>Contact resource</source>
   <translation>Resource Contactpersoon</translation>
  </message>
  <message>
   <location filename="../../plugins/chatmessagehandler/chatmessagehandler.cpp" line="486"/>
   <source>Clear Chat Window</source>
   <translation>Chat venster leegmaken</translation>
  </message>
  <message>
   <location filename="../../plugins/chatmessagehandler/chatmessagehandler.cpp" line="545"/>
   <source>&lt;Absent&gt;</source>
   <translation>&lt;Afwezig&gt;</translation>
  </message>
  <message>
   <location filename="../../plugins/chatmessagehandler/chatmessagehandler.cpp" line="552"/>
   <source>%1 - Chat</source>
   <translation>%1 - Chat</translation>
  </message>
  <message>
   <location filename="../../plugins/chatmessagehandler/chatmessagehandler.cpp" line="642"/>
   <source>Loading history...</source>
   <translation>Geschiedenis laden...</translation>
  </message>
  <message>
   <location filename="../../plugins/chatmessagehandler/chatmessagehandler.cpp" line="1037"/>
   <source>%1 changed status to [%2] %3</source>
   <translation>%1 veranderd status naar [%2] %3</translation>
  </message>
  <message>
   <location filename="../../plugins/chatmessagehandler/chatmessagehandler.cpp" line="1102"/>
   <source>Failed to load history: %1</source>
   <translation>Mislukt om geschiedenis te laden: %1</translation>
  </message>
 </context>
</TS>