<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" version="2.0">
 <context>
  <name>BobUrlHandler</name>
  <message>
   <location filename="../../plugins/boburlhandler/boburlhandler.cpp" line="10"/>
   <source>BOB URL handler</source>
   <translation>BOB URL geleider</translation>
  </message>
  <message>
   <location filename="../../plugins/boburlhandler/boburlhandler.cpp" line="11"/>
   <source>Allows URL processor plugin to handle Bits of Binary links (cid: scheme)</source>
   <translation>Laat URL processor plugin toe het hanteren van stukjes binaire links (cid: schema)</translation>
  </message>
 </context>
</TS>