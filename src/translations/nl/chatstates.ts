<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" sourcelanguage="en" version="2.0">
 <context>
  <name>ChatStates</name>
  <message>
   <source>Chat State Notifications</source>
   <translation>Meldingen van de chat</translation>
  </message>
  <message>
   <source>Allow Chat State Notifications</source>
   <translation>Toestaan chat meldingen</translation>
  </message>
  <message>
   <source>Disallow Chat State Notifications</source>
   <translation>Niet toestaan van chat meldingen</translation>
  </message>
  <message>
   <source>Allows to share information about the user&apos;s activity in the chat</source>
   <translation>Toestaan om informatie te delen over gebruikers activiteit in de chat</translation>
  </message>
  <message>
   <source>Supports the exchanging of the information about the user&apos;s activity in the chat</source>
   <translation>Ondersteunt de uitwisseling van informatie over gebruikers activiteit in de chat</translation>
  </message>
  <message>
   <source>When contact is typing the message for you</source>
   <translation>Wanneer de contactpersoon een bericht voor je tikt</translation>
  </message>
  <message>
   <source>Typing a message...</source>
   <translation>Tikt een bericht....</translation>
  </message>
  <message>
   <source>Send notifications of your chat activity</source>
   <translation>Stuur aanmelding van je chat-activiteit</translation>
  </message>
 </context>
 <context>
  <name>StateWidget</name>
  <message>
   <source>Default</source>
   <translation>Standaard</translation>
  </message>
  <message>
   <source>Active</source>
   <translation>Actief</translation>
  </message>
  <message>
   <source>Composing</source>
   <translation>Aan het typen</translation>
  </message>
  <message>
   <source>Paused</source>
   <translation>Pauze</translation>
  </message>
  <message>
   <source>Inactive %1</source>
   <translation>Inactief %1</translation>
  </message>
  <message>
   <source>Gone %1</source>
   <translation>Weg %1</translation>
  </message>
  <message>
   <source>User activity in chat</source>
   <translation>Gebruikers activiteit in chat</translation>
  </message>
  <message>
   <source>and %1 other</source>
   <translation>en %1 andere</translation>
  </message>
  <message>
   <source>Always send my chat activity</source>
   <translation>Verstuur altijd mijn chat activiteit</translation>
  </message>
  <message>
   <source>Never send my chat activity</source>
   <translation>Verstuur nooit mijn chat activiteit</translation>
  </message>
  <message>
   <source>Participants activity in conference</source>
   <translation>Deelnemers activiteit aan conferentie</translation>
  </message>
 </context>
</TS>