<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" sourcelanguage="en" version="2.0">
 <context>
  <name>Account</name>
  <message>
   <source>Enter password for account &lt;b&gt;%1&lt;/b&gt;</source>
   <translation>Paswoord invoeren voor account &lt;b&gt;%1&lt;/b&gt;</translation>
  </message>
  <message>
   <source>Account Password</source>
   <translation>Account paswoord</translation>
  </message>
 </context>
 <context>
  <name>AccountItemWidget</name>
  <message>
   <source>Settings...</source>
   <translation>Instellingen...</translation>
  </message>
 </context>
 <context>
  <name>AccountManager</name>
  <message>
   <source>Accounts</source>
   <translation>Accounts</translation>
  </message>
  <message>
   <source>Modify account</source>
   <translation>Wijzig account</translation>
  </message>
  <message>
   <source>Account Manager</source>
   <translation>Account manager</translation>
  </message>
  <message>
   <source>Allows to create and manage Jabber accounts</source>
   <translation>Toestaan om jabber accounts aan te maken en te beheren</translation>
  </message>
  <message>
   <source>Common account settings</source>
   <translation>Algemene accountinstellingen</translation>
  </message>
  <message>
   <source>Account</source>
   <translation>Account</translation>
  </message>
  <message>
   <source>Name:</source>
   <translation>Naam:</translation>
  </message>
  <message>
   <source>Password:</source>
   <translation>Paswoord:</translation>
  </message>
  <message>
   <source>Resource:</source>
   <translation>Resource:</translation>
  </message>
  <message>
   <source>Additional settings</source>
   <translation>Extra instellingen</translation>
  </message>
  <message>
   <source>Require secure connection to server</source>
   <translation>Vereist beveiligde verbinding met de server</translation>
  </message>
  <message>
   <source>Parameters</source>
   <translation>Parameters</translation>
  </message>
  <message>
   <source>Additional</source>
   <translation>Extra</translation>
  </message>
  <message>
   <source>Home</source>
   <translation>Home</translation>
  </message>
  <message>
   <source>Work</source>
   <translation>Werk</translation>
  </message>
  <message>
   <source>Notebook</source>
   <translation>Notitieboekje</translation>
  </message>
  <message>
   <source>(default)</source>
   <translation>(standaard)</translation>
  </message>
  <message>
   <source>&lt;Empty&gt;</source>
   <translation>&lt;Leeg&gt;</translation>
  </message>
  <message>
   <source>Default resource:</source>
   <translation>Standaard resource:</translation>
  </message>
  <message>
   <source>New account</source>
   <translation>Nieuw account</translation>
  </message>
  <message>
   <source>Jabber ID:</source>
   <translation>Jabber ID:</translation>
  </message>
 </context>
 <context>
  <name>AccountsOptionsClass</name>
  <message>
   <source>Add or register a new account</source>
   <translation>Registreer of voeg een nieuw account toe</translation>
  </message>
 </context>
 <context>
  <name>AccountsOptionsWidget</name>
  <message>
   <source>Add Account...</source>
   <translation>Account toevoegen...</translation>
  </message>
  <message>
   <source>Show inactive accounts</source>
   <translation>Toon inactieve accounts</translation>
  </message>
  <message>
   <source>Hide inactive accounts</source>
   <translation>Verberg inactieve accounts</translation>
  </message>
  <message>
   <source>Remove Account</source>
   <translation>Verwijder account</translation>
  </message>
  <message>
   <source>You are assured that wish to remove an account &lt;b&gt;%1&lt;/b&gt;?&lt;br&gt;All settings will be lost.</source>
   <translation>Ben je er zeker van de je een account wilt verwijderen &lt;b&gt;%1&lt;/b&gt;?&lt;br&gt; Alle instellingen gaan verloren.</translation>
  </message>
  <message>
   <source>Use wizard...</source>
   <translation>Gebruik Wizard...</translation>
  </message>
 </context>
</TS>