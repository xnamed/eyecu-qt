<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" version="2.0">
 <context>
  <name>Receipts</name>
  <message>
   <location filename="../../plugins/receipts/receipts.cpp" line="35"/>
   <location filename="../../plugins/receipts/receipts.cpp" line="158"/>
   <source>Message Delivery Receipts</source>
   <translation>Ontvangt Bericht bezorging</translation>
  </message>
  <message>
   <location filename="../../plugins/receipts/receipts.cpp" line="36"/>
   <source>Sends, receives and displays message delivery receipts</source>
   <translation>Verzend, Ontvangt  en toont ontvangstbevestigingen</translation>
  </message>
  <message>
   <location filename="../../plugins/receipts/receipts.cpp" line="97"/>
   <source>Show delivery notifications</source>
   <translation>Toon Bezorg bevestiging</translation>
  </message>
  <message>
   <location filename="../../plugins/receipts/receipts.cpp" line="98"/>
   <source>Send delivery notifications</source>
   <translation>Stuur Bezorg bevestiging</translation>
  </message>
  <message>
   <location filename="../../plugins/receipts/receipts.cpp" line="126"/>
   <source>When message delivery notification recieved</source>
   <translation>Wanneer bericht van bezorg bevestiging aankomt</translation>
  </message>
  <message>
   <location filename="../../plugins/receipts/receipts.cpp" line="159"/>
   <source>Sends/receives Message Delivery Receipts</source>
   <translation>Stuurt/Ontvangt Bericht bezorging</translation>
  </message>
  <message>
   <location filename="../../plugins/receipts/receipts.cpp" line="326"/>
   <source>Message delivered</source>
   <translation>Bericht afgeleverd</translation>
  </message>
 </context>
</TS>