<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" version="2.0">
 <context>
  <name>ConsolePlugin</name>
  <message>
   <location filename="../../plugins/console/consoleplugin.cpp" line="26"/>
   <source>Console</source>
   <translation>Console</translation>
  </message>
  <message>
   <location filename="../../plugins/console/consoleplugin.cpp" line="27"/>
   <source>Allows to view XML stream between the client and server</source>
   <translation>Toestaan om de XML stream te bekijken tussen de Client en Server</translation>
  </message>
  <message>
   <location filename="../../plugins/console/consoleplugin.cpp" line="58"/>
   <source>XML Console</source>
   <translation>XML Console</translation>
  </message>
  <message>
   <location filename="../../plugins/console/consoleplugin.cpp" line="68"/>
   <source>Default Context</source>
   <translation>Standaardcontext</translation>
  </message>
 </context>
 <context>
  <name>ConsoleWidget</name>
  <message>
   <location filename="../../plugins/console/consolewidget.cpp" line="29"/>
   <source>&lt;All Streams&gt;</source>
   <translation>&lt;Alle streams&gt;</translation>
  </message>
  <message>
   <location filename="../../plugins/console/consolewidget.cpp" line="70"/>
   <source>Search</source>
   <translation>Zoek</translation>
  </message>
  <message>
   <location filename="../../plugins/console/consolewidget.cpp" line="140"/>
   <source>XML Console - %1</source>
   <translation>XML Console - %1</translation>
  </message>
  <message>
   <location filename="../../plugins/console/consolewidget.cpp" line="252"/>
   <source>Start sending user stanza...</source>
   <translation>Start versturen gebruiker stanza...</translation>
  </message>
  <message>
   <location filename="../../plugins/console/consolewidget.cpp" line="256"/>
   <source>User stanza sent.</source>
   <translation>Gebruiker stanza verstuurd.</translation>
  </message>
  <message>
   <location filename="../../plugins/console/consolewidget.cpp" line="260"/>
   <source>Stanza is not well formed.</source>
   <translation>Stanza in niet goed gevormd.</translation>
  </message>
  <message>
   <location filename="../../plugins/console/consolewidget.cpp" line="265"/>
   <source>XML is not well formed.</source>
   <translation>XML in niet goed gevormd.</translation>
  </message>
  <message>
   <location filename="../../plugins/console/consolewidget.cpp" line="271"/>
   <source>New Context</source>
   <translation>Nieuw context</translation>
  </message>
  <message>
   <location filename="../../plugins/console/consolewidget.cpp" line="271"/>
   <source>Enter context name</source>
   <translation>Voer context naam in</translation>
  </message>
 </context>
 <context>
  <name>ConsoleWidgetClass</name>
  <message>
   <location filename="../../plugins/console/consolewidget.ui" line="14"/>
   <source>XML Console</source>
   <translation>XML Console</translation>
  </message>
  <message>
   <location filename="../../plugins/console/consolewidget.ui" line="33"/>
   <source>Filter</source>
   <translation>Filter</translation>
  </message>
  <message>
   <location filename="../../plugins/console/consolewidget.ui" line="42"/>
   <source>Stream:</source>
   <translation>Stream:</translation>
  </message>
  <message>
   <location filename="../../plugins/console/consolewidget.ui" line="62"/>
   <source>Condition:</source>
   <translation>Conditie:</translation>
  </message>
  <message>
   <location filename="../../plugins/console/consolewidget.ui" line="96"/>
   <location filename="../../plugins/console/consolewidget.ui" line="159"/>
   <source>Add</source>
   <translation>Toevoegen</translation>
  </message>
  <message>
   <location filename="../../plugins/console/consolewidget.ui" line="109"/>
   <location filename="../../plugins/console/consolewidget.ui" line="172"/>
   <source>Remove</source>
   <translation>Verwijder</translation>
  </message>
  <message>
   <location filename="../../plugins/console/consolewidget.ui" line="122"/>
   <location filename="../../plugins/console/consolewidget.ui" line="322"/>
   <source>Clear</source>
   <translation>Wis</translation>
  </message>
  <message>
   <location filename="../../plugins/console/consolewidget.ui" line="138"/>
   <source>Available context:</source>
   <translation>Beschikbare context:</translation>
  </message>
  <message>
   <location filename="../../plugins/console/consolewidget.ui" line="188"/>
   <source>Console</source>
   <translation>Console</translation>
  </message>
  <message>
   <location filename="../../plugins/console/consolewidget.ui" line="298"/>
   <source>Word wrap</source>
   <translation>Regelafbreking</translation>
  </message>
  <message>
   <location filename="../../plugins/console/consolewidget.ui" line="305"/>
   <source>Highlight XML</source>
   <translation>Markeer XML</translation>
  </message>
  <message>
   <location filename="../../plugins/console/consolewidget.ui" line="315"/>
   <source>Send</source>
   <translation>Verstuur</translation>
  </message>
 </context>
</TS>