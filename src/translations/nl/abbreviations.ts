<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" version="2.0">
 <context>
  <name>Abbreviations</name>
  <message>
   <location filename="../../plugins/abbreviations/abbreviations.cpp" line="36"/>
   <source>Abbreviations</source>
   <translation>Afkortingen</translation>
  </message>
  <message>
   <location filename="../../plugins/abbreviations/abbreviations.cpp" line="37"/>
   <source>Translates abbreviations frequently used in network communities</source>
   <translation>Vertaal vaak gebruikte afkortingen in netwerk van gemeenschappen</translation>
  </message>
 </context>
</TS>