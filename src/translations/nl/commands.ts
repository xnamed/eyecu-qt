<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" version="2.0">
 <context>
  <name>CommandDialog</name>
  <message>
   <location filename="../../plugins/commands/commanddialog.cpp" line="29"/>
   <source>&lt;Back</source>
   <translation>&lt;Terug</translation>
  </message>
  <message>
   <location filename="../../plugins/commands/commanddialog.cpp" line="30"/>
   <source>Next&gt;</source>
   <translation>Volgende&gt;</translation>
  </message>
  <message>
   <location filename="../../plugins/commands/commanddialog.cpp" line="31"/>
   <source>Complete</source>
   <translation>Compleet</translation>
  </message>
  <message>
   <location filename="../../plugins/commands/commanddialog.cpp" line="95"/>
   <source>Command execution completed.</source>
   <translation>Uitgevoerde opdracht voltooid.</translation>
  </message>
  <message>
   <location filename="../../plugins/commands/commanddialog.cpp" line="97"/>
   <source>Command execution canceled.</source>
   <translation>Uitgevoerde opdracht geannuleerd.</translation>
  </message>
  <message>
   <location filename="../../plugins/commands/commanddialog.cpp" line="137"/>
   <source>Requested operation failed: %1</source>
   <translation>Gevraagde bewerking is mislukt: %1</translation>
  </message>
  <message>
   <location filename="../../plugins/commands/commanddialog.cpp" line="152"/>
   <source>Executing command &apos;%1&apos; at %2</source>
   <translation>Voor opdracht uit &apos;%1&apos; om %2</translation>
  </message>
  <message>
   <location filename="../../plugins/commands/commanddialog.cpp" line="190"/>
   <source>Waiting for host response ...</source>
   <translation>Wacht op antwoord van host ...</translation>
  </message>
  <message>
   <location filename="../../plugins/commands/commanddialog.cpp" line="195"/>
   <source>Error: Can&apos;t send request to host.</source>
   <translation>Fout: kan aanvraag niet versturen naar host.</translation>
  </message>
 </context>
 <context>
  <name>Commands</name>
  <message>
   <location filename="../../plugins/commands/commands.cpp" line="44"/>
   <location filename="../../plugins/commands/commands.cpp" line="543"/>
   <source>Ad-Hoc Commands</source>
   <translation>Ad Hoc opdrachten</translation>
  </message>
  <message>
   <location filename="../../plugins/commands/commands.cpp" line="45"/>
   <source>Allows to perform special commands provided by various services</source>
   <translation>Maakt het mogelijk om speciale opdrachten door verschillende diensten te laten uitvoeren</translation>
  </message>
  <message>
   <location filename="../../plugins/commands/commands.cpp" line="114"/>
   <source>Can not understand the specified action</source>
   <translation>Begrijp opgegeven actie niet</translation>
  </message>
  <message>
   <location filename="../../plugins/commands/commands.cpp" line="115"/>
   <source>Can not accept the specified action</source>
   <translation>Kan opgegeven actie niet accepteren</translation>
  </message>
  <message>
   <location filename="../../plugins/commands/commands.cpp" line="116"/>
   <source>Can not accept the specified language/locale</source>
   <translation>Kan opgegeven landinstelling niet accepteren</translation>
  </message>
  <message>
   <location filename="../../plugins/commands/commands.cpp" line="117"/>
   <source>The data form did not provide one or more required fields</source>
   <translation>Het gegevensformulier zorgde niet voor een of meer vereiste velden</translation>
  </message>
  <message>
   <location filename="../../plugins/commands/commands.cpp" line="118"/>
   <source>Specified session not present</source>
   <translation>Opgegeven sessie niet aanwezig</translation>
  </message>
  <message>
   <location filename="../../plugins/commands/commands.cpp" line="119"/>
   <source>Specified session is no longer active</source>
   <translation>Opgegeven sessie is niet langer actief</translation>
  </message>
  <message>
   <location filename="../../plugins/commands/commands.cpp" line="345"/>
   <source>Execute</source>
   <translation>Uitvoeren</translation>
  </message>
  <message>
   <location filename="../../plugins/commands/commands.cpp" line="360"/>
   <source>Commands</source>
   <translation>Opdrachten</translation>
  </message>
  <message>
   <location filename="../../plugins/commands/commands.cpp" line="378"/>
   <source>Request commands</source>
   <translation>Verzoek opdrachten</translation>
  </message>
  <message>
   <location filename="../../plugins/commands/commands.cpp" line="544"/>
   <source>Supports the running or performing of the special services commands</source>
   <translation>Ondersteunt het functioneren of het uitvoeren van de speciale dienst opdrachten</translation>
  </message>
 </context>
</TS>