<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" version="2.0">
 <context>
  <name>Attention</name>
  <message>
   <location filename="../../plugins/attention/attention.cpp" line="51"/>
   <location filename="../../plugins/attention/attention.cpp" line="156"/>
   <location filename="../../plugins/attention/attention.cpp" line="175"/>
   <location filename="../../plugins/attention/attention.cpp" line="188"/>
   <location filename="../../plugins/attention/attention.cpp" line="365"/>
   <location filename="../../plugins/attention/attention.cpp" line="510"/>
   <source>Attention</source>
   <translation>Attentie</translation>
  </message>
  <message>
   <location filename="../../plugins/attention/attention.cpp" line="52"/>
   <source>Implements XEP-0224: Attention</source>
   <translation>Implementeert XEP-0224: Attentie</translation>
  </message>
  <message>
   <location filename="../../plugins/attention/attention.cpp" line="148"/>
   <source>When contact attempts to attract user&apos;s attention</source>
   <translation>Als contactpersoon de aandacht van de gebruiker probeert te trekken</translation>
  </message>
  <message>
   <source>Shift+Return</source>
   <comment>Attention</comment>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/attention/attention.cpp" line="156"/>
   <source>Alt+Return</source>
   <comment>Attention</comment>
   <translation type="unfinished"/>
  </message>
  <message>
   <location filename="../../plugins/attention/attention.cpp" line="176"/>
   <source>Notification pop-up</source>
   <translation>Melding pop-up</translation>
  </message>
  <message>
   <location filename="../../plugins/attention/attention.cpp" line="177"/>
   <source>Always play sound</source>
   <translation>Speel altijd geluid</translation>
  </message>
  <message>
   <location filename="../../plugins/attention/attention.cpp" line="189"/>
   <source>Implements XEP-0224: Allows to attract user&apos;s attention</source>
   <translation>Implementeert XEP-0224: Toestaan om aandacht van de gebruiker te trekken</translation>
  </message>
  <message>
   <location filename="../../plugins/attention/attention.cpp" line="264"/>
   <source>Attention from %1</source>
   <translation>Attentie van %1</translation>
  </message>
  <message>
   <location filename="../../plugins/attention/attention.cpp" line="274"/>
   <source>ATTENTION!!!</source>
   <translation>ATTENTIE!!!</translation>
  </message>
  <message>
   <location filename="../../plugins/attention/attention.cpp" line="411"/>
   <source>%1 - Chat</source>
   <translation>%1 - Chat</translation>
  </message>
 </context>
 <context>
  <name>AttentionDialog</name>
  <message>
   <location filename="../../plugins/attention/attentiondialog.ui" line="170"/>
   <source>ATTENTION</source>
   <translation>ATTENTIE</translation>
  </message>
  <message>
   <location filename="../../plugins/attention/attentiondialog.ui" line="295"/>
   <source>Ok</source>
   <translation>OK</translation>
  </message>
 </context>
</TS>