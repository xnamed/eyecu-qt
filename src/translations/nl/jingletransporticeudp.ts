<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl" version="2.0">
 <context>
  <name>JingleTransportIceUdp</name>
  <message>
   <location filename="../../plugins/jingletransporticeudp/jingletransporticeudp.cpp" line="13"/>
   <location filename="../../plugins/jingletransporticeudp/jingletransporticeudp.cpp" line="113"/>
   <source>Jingle ICE-UDP Transport</source>
   <translation>Jingle ICE-UDP Transport</translation>
  </message>
  <message>
   <location filename="../../plugins/jingletransporticeudp/jingletransporticeudp.cpp" line="14"/>
   <source>Implements XEP-0176: Jingle ICE-UDP transport Method</source>
   <translation>Implementeert XEP-0176: Jingle ICE UDP transport Methode</translation>
  </message>
  <message>
   <location filename="../../plugins/jingletransporticeudp/jingletransporticeudp.cpp" line="114"/>
   <source>Allows using ICE-UDP transport in Jingle sesions</source>
   <translation>Maakt gebruik van ICE-UDP transport in Jingle sessies</translation>
  </message>
 </context>
</TS>