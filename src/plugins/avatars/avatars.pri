HEADERS = avatars.h \
    avataroptionswidget.h \
    $$PWD/avatarsizeoptionswidget.h

SOURCES = avatars.cpp \
    avataroptionswidget.cpp \
    $$PWD/avatarsizeoptionswidget.cpp

FORMS += \
    avataroptionswidget.ui \
    $$PWD/avatarsizeoptionswidget.ui
