HEADERS = selecticonwidget.h \
          selecticonmenu.h \
          emoji.h \
          emojioptions.h \
    $$PWD/iconsizespinbox.h \
    $$PWD/iconsetdelegate.h

SOURCES = selecticonwidget.cpp \
          selecticonmenu.cpp \
          emoji.cpp \ 
          emojioptions.cpp \
    $$PWD/iconsizespinbox.cpp \
    $$PWD/iconsetdelegate.cpp

FORMS +=  emojioptions.ui
