HEADERS = xhtmlim.h \
    edithtml.h \
    insertimage.h \
    addlink.h \
    resourceretriever.h \
    savequery.h \
    imageopenthread.h \
    settooltip.h \
    $$PWD/xhtmloptions.h

SOURCES = xhtmlim.cpp \
    edithtml.cpp \
    insertimage.cpp \
    addlink.cpp \
    resourceretriever.cpp \
    savequery.cpp \
    imageopenthread.cpp \
    settooltip.cpp \
    $$PWD/xhtmloptions.cpp

FORMS += \
    insertimage.ui \
    addlink.ui \
    settooltip.ui \
    $$PWD/xhtmloptions.ui
